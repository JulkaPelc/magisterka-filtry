Na płycie znajduje się:
	- folder Comprec_repo
	- plik z pracą magisterską ("W11_221380_praca magisterska.pdf")
	- plik README.txt

Folder Comprec_repo zawiera część repozytorium serwisu Comprec (ze względu na wielkość serwisu, tylko najpotrzebniejsze pliki zostały dodane na płytę). Wśród plików znajdują się 11 plików stworzonych całkowicie przeze mnie:
	- 6 filtrów w folderze o ścieżce /Comprec_repo/comprec_repo/comprec_project/
	(pliki: buried_exposed.py, frequent_gap.py, inter_terminus.py, intra_secondary_structure.py, low_entropy.py, terminus.py)
	- 2 pliki pomocnicze w folderze o ścieżce /Comprec_repo/comprec_repo/comprec_project/
	(pliki entropy.py, loadDCAFile.py)
	- plik html do wyświetlenia strony narzędzia /Comprec_repo/comprec_repo/templates/comprec/FPFilterInput.html
	- plik javascript do obsługi ukrywania niektórych pól formularza (/Comprec_repo/comprec_repo/static/js/fpfilters.js
	- folder .zip z przykładowymi plikami (input dla filtrów) o poprawym formatowaniu (/Comprec_repo/comprec_repo/static/comprec/FP_filter/FPFilters_Input.zip
 
Ponadto edytowane było kilka utworzonych wcześniej plików (wszystkie w folderze o ścieżce /Comprec_repo/comprec_repo/comprec_project):
	- forms.py
	- models.py
	- urls.py
	- views.py
