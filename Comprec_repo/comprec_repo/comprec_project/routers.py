class AmyloadRouter(object):
	"""
	A router to control all database operations on models in the amyload application.
	"""
	def db_for_read(self, model, **hints):
		"""
		Attempts to read amyload models go to amyload_db.
		"""
		if model._meta.app_label == 'amyload':
			return 'amyload_db'
		return None

	def db_for_write(self, model, **hints):
		"""
		Attempts to write amyload models go to amyload_db.
		"""
		if model._meta.app_label == 'amyload':
			return 'amyload_db'
		return None

	def allow_relation(self, obj1, obj2, **hints):
		"""
		Allow relations if a model in the amyload app is involved.
		"""
		if obj1._meta.app_label == 'amyload' or \
		   obj2._meta.app_label == 'amyload':
		   return True
		return None

	def allow_migrate(self, db, model, **hints):
		"""
		Make sure the amyload app only appears in the 'amyload_db'
		database.
		"""
		if model._meta.app_label == 'amyload':
			return db == 'amyload_db'
		return None
