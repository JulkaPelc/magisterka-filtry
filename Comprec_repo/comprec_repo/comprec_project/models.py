from django.db import models


class FPFilter(models.Model):
    """
    FP filter model
    author: Julia Pelc
    """
    QueryName = models.CharField(max_length=25)
    DCAfile = models.FileField()
    MSAfile = models.FileField(blank=True, null=True)
    SSfile = models.FileField(blank=True, null=True)
    RSAfile = models.FileField(blank=True, null=True)
    email = models.EmailField()
