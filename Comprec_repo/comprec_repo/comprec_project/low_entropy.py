import os
from django.contrib.staticfiles import finders
from loadDCAFile import load_dca_file
from entropy import count_entropy


def get_results_from_low_entropy(DCAPath, MSAfileID, cutOffValue, query_name):
    try:
        # load DCA file
        DCA = load_dca_file(DCAPath)

        # load MSA file
        file = open(MSAfileID, "r")
        MSA = file.readlines()
        file.close()

        if len(MSA) < 3:
            raise ValueError("Wrong format of the MSA file")

        # Remove headings of MSA sequences (leave only pure sequences od AA)
        MSAnew = []
        for i in range(len(MSA)):
            if MSA[i][0] != ">":
                MSAnew.append(MSA[i])

        cutOffValue = float(cutOffValue)

        # count entropy for each position in MSA
        H = count_entropy(MSAnew)

        # MSAsequenceCount = len(MSAnew)  # number of sequences in MSA
        # sequenceLength = len(MSAnew[0])-1  # length of target sequence

        # positions to remove (with entropy <= cutOffValue)
        positionToFilterOut = []
        for i in range(len(H)):
            if H[i] <= cutOffValue:
                positionToFilterOut.append(i+1)

        #  filter out contacts (from DCA) between two positions if on one of them entropy <= cutOffValue
        DCAremoved = []
        i = 0
        while (i - len(DCAremoved)) <= 200:
            posA = int(DCA[i][0])
            posB = int(DCA[i][1])
            if posA in positionToFilterOut or posB in positionToFilterOut:
                DCAremoved.append(DCA[i])
            i = i+1

        # create file with new DCA
        save_dir = finders.find("comprec/FP_filter/tmp/")
        file_name = os.path.join(save_dir, query_name + '_low-entropy-fpfilter_' + str(cutOffValue) + '.results')
        file_to_save = open(file_name, 'w')
        for line in DCAremoved:
            file_to_save.write(str(line[0]) + ',' + str(line[1]) + ',' + str(line[2]) + '\n')
        file_to_save.close()

        return file_name

    except ValueError:
        save_dir = finders.find("comprec/FP_filter/tmp/")
        file_name = os.path.join(save_dir, query_name + '_low-entropy-fpfilter_ERROR.results')
        file_to_save = open(file_name, 'w')
        file_to_save.write("An error occurred. Wrong format of the MSA file.")
        file_to_save.close()
        return file_name

