import os
from django.contrib.staticfiles import finders
from loadDCAFile import load_dca_file


def get_results_from_inter_terminus(DCAPath, MSAfileID, cutOffValue, query_name):
    try:
        # load DCA file
        DCA = load_dca_file(DCAPath)

        # load MSA file
        file = open(MSAfileID, "r")
        MSA = file.readlines()
        file.close()

        if len(MSA) < 3:
            raise ValueError("Wrong format of the MSA file")

        # Remove headings of MSA sequences (leave only pure sequences od AA)
        MSAnew = []
        for i in range(len(MSA)):
            if MSA[i][0] != ">":
                MSAnew.append(MSA[i])

        cutOffValue = float(cutOffValue)

        sequenceLength = len(MSA[1])-1  # length of target sequence

        DCAremoved = []
        i = 0
        while (i - len(DCAremoved)) <= 200:
            posA = float(DCA[i][0])
            posB = float(DCA[i][1])
            separationNorm = abs(posA - posB) / sequenceLength
            if separationNorm > cutOffValue:
                DCAremoved.append(DCA[i])
            i = i+1

        # create file with new DCA (with filtered out pairs with normalized separation > cutOffValue)
        save_dir = finders.find("comprec/FP_filter/tmp/")
        file_name = os.path.join(save_dir, query_name + '_inter-terminus-fpfilter_' + str(cutOffValue) + '.results')
        file_to_save = open(file_name, 'w')
        for line in DCAremoved:
            file_to_save.write(str(line[0]) + ',' + str(line[1]) + ',' + str(line[2]) + '\n')
        file_to_save.close()

        return file_name

    except ValueError:
        save_dir = finders.find("comprec/FP_filter/tmp/")
        file_name = os.path.join(save_dir, query_name + '_inter-terminus-fpfilter_ERROR.results')
        file_to_save = open(file_name, 'w')
        file_to_save.write("An error occurred. Wrong format of the MSA file.")
        file_to_save.close()
        return file_name
