from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from comprec_project import views
admin.autodiscover()

if(settings.MAINTENANCE):
	urlpatterns = patterns('',
		url(r'^admin/', include(admin.site.urls)),
		url(r'^.*$', views.maintenance),
	)
else:
	urlpatterns = patterns('',
		url(r'^amyload/', include('comprec_project.amyload.urls')),
		url(r'^admin/', include(admin.site.urls)),
		url(r'^c2sInput/$', views.c2sInput),
		url(r'^c2sRun/$', views.c2sRun),
		#url(r'^c2sInitiate/$', views.c2sInitiate),
		url(r'^fishInput/$', views.fishInput),
		url(r'^fishAbout/$', views.fishAbout),
		url(r'^fishRun/$', views.fishRun),
		url(r'^amyloadAbout/$', views.amyloadAbout),
		url(r'^fpFilterInput/$', views.fpFilterInput),
		url(r'^fpFilterRun/$', views.fpFilterRun),
		url(r'^downloadFishPlot/$', views.downloadFishPlot),
		url(r'^downloadFishData/$', views.downloadFishData),
		url(r'^downloadC2S/$', views.downloadC2S),
		url(r'^downloadC2SInput/$', views.downloadC2SInput),
		url(r'^downloadFpFilterInput/$', views.downloadFpFilterInput),
		url(r'^downloadPCO/$', views.downloadPCO),
		url(r'^c2s/$', views.c2s),
		url(r'^pco/$', views.pco),
		url(r'^dcaVSmisfolds/$', views.dcaVSmisfolds),
		url(r'^downloadDCAvsMisfolds/(?P<fileName>.+)/$', views.downloadDCAvsMisfolds),
		url(r'^dcaQ/$', views.dcaQ),
		url(r'^downloadDCAQ/(?P<fileName>.+)/$', views.downloadDCAQ), ##
		url(r'^$', views.index),
	)

if settings.DEBUG:
	urlpatterns += patterns(
		'django.views.static',
		(r'media/(?P<path>.*)',
		'serve',
		{'document_root': settings.MEDIA_ROOT}), )
