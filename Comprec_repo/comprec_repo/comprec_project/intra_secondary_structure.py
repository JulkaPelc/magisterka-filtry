import os
from django.contrib.staticfiles import finders
from loadDCAFile import load_dca_file


def get_results_from_intra_secondary_structure(DCAPath, SSfileID, cutOffValue, query_name):
    try:
        # load DCA file
        DCA = load_dca_file(DCAPath)

        # load Secondary Structure (SS) file
        file = open(SSfileID, "r")
        SS = file.readlines()
        file.close()

        if len(SS) != 1:
            raise ValueError("Wrong format of the SS file")

        cutOffValue = float(cutOffValue)

        sequenceLength = len(SS[0]) - 1  # length of target sequence

        SSaffiliation = []
        recentLetter = 'C'
        SScount = 0
        for i in range(sequenceLength):
            if SS[0][i] == 'C':
                SSaffiliation.append(0)
                recentLetter = 'C'
            else:
                if SS[0][i] == recentLetter:
                    SSaffiliation.append(SScount)
                else:
                    SScount = SScount + 1
                    SSaffiliation.append(SScount)
                recentLetter = SS[0][i]

        DCAremoved = []
        i = 0
        while (i - len(DCAremoved)) <= 200:
            posA = int(DCA[i][0])
            posB = int(DCA[i][1])
            SSposA = SSaffiliation[posA-1]
            SSposB = SSaffiliation[posB-1]
            separation = abs(posA - posB)
            if SSposA == SSposB:
                if SSposA != 0:
                    if separation >= cutOffValue:
                        DCAremoved.append(DCA[i])
            i = i+1

        # create file with new DCA (with filtered out pairs from the same SS with separation > cutOffValue)
        save_dir = finders.find("comprec/FP_filter/tmp/")
        file_name = os.path.join(save_dir, query_name + '_intra-secondary-structure-fpfilter_' + str(cutOffValue) + '.results')
        file_to_save = open(file_name, 'w')
        for line in DCAremoved:
            file_to_save.write(str(line[0]) + ',' + str(line[1]) + ',' + str(line[2]) + '\n')
        file_to_save.close()

        return file_name

    except ValueError:
        save_dir = finders.find("comprec/FP_filter/tmp/")
        file_name = os.path.join(save_dir, query_name + '_intra-secondary-structure-fpfilter_ERROR.results')
        file_to_save = open(file_name, 'w')
        file_to_save.write("An error occurred. Wrong format of the SS file.")
        file_to_save.close()
        return file_name
