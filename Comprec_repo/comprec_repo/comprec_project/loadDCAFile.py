def take_dca_value(element):
    return element[2]


def load_dca_file(dca_file_id):
    dca = []
    with open(dca_file_id, "rt") as f:
        for line in f:
            dca.append(line.split(','))

    for i in range(len(dca)):
        dca[i][0] = int(dca[i][0])
        dca[i][1] = int(dca[i][1])
        dca[i][2] = float(dca[i][2])

    dcanew = []
    for i in range(len(dca)):
        pos_a = float(dca[i][0])
        pos_b = float(dca[i][1])
        separation = abs(pos_a - pos_b)
        if separation >= 5:
            dcanew.append(dca[i])

    dcanew = sorted(dcanew, key=take_dca_value, reverse=True)

    return dcanew
