import os
from django.contrib.staticfiles import finders
import numpy as np
from loadDCAFile import load_dca_file


def get_results_from_frequent_gap(DCAPath, MSAfileID, cutOffValue, query_name):
    try:
        # load DCA file
        DCA = load_dca_file(DCAPath)

        # load MSA file
        file = open(MSAfileID, "r")
        MSA = file.readlines()
        file.close()

        if len(MSA) < 3:
            raise ValueError("Wrong format of the MSA file")

        # Remove headings of MSA sequences (leave only pure sequences od AA)
        MSAnew = []
        for i in range(len(MSA)):
            if MSA[i][0] != ">":
                MSAnew.append(MSA[i])

        cutOffValue = int(cutOffValue)

        MSAsequenceCount = len(MSAnew)  # number of sequences in MSA
        sequenceLength = len(MSAnew[0])-1  # length of target sequence

        # count gaps on each position
        gapCount = np.zeros((1, sequenceLength), dtype=np.int)
        for i in range(sequenceLength):
            for j in range(MSAsequenceCount):
                if MSAnew[j][i] == '-':
                    gapCount[0][i] = gapCount[0][i] + 1

        # number of gaps on each position to percentage
        gapPercentage = gapCount * 100 / MSAsequenceCount
        gapPercentage = gapPercentage.tolist()

        # positions to remove (with gapPercentage >= 95%)
        positionToFilterOut = []
        for i in range(len(gapPercentage[0])):
            if gapPercentage[0][i] >= cutOffValue:
                positionToFilterOut.append(i+1)

        #  filter out from DCA contacts between two positions where on one of them gapPercentage >= 95%
        DCAremoved = []
        i = 0
        while (i - len(DCAremoved)) <= 200:
            posA = int(DCA[i][0])
            posB = int(DCA[i][1])
            if posA in positionToFilterOut or posB in positionToFilterOut:
                DCAremoved.append(DCA[i])
            i = i+1

        # create file with new DCA (with filtered out contacts with gap count >=95%)
        save_dir = finders.find("comprec/FP_filter/tmp/")
        file_name = os.path.join(save_dir, query_name + '_frequent-gap-fpfilter_' + str(cutOffValue) + '.results')
        file_to_save = open(file_name, 'w')
        for line in DCAremoved:
            file_to_save.write(str(line[0]) + ',' + str(line[1]) + ',' + str(line[2]) + '\n')
        file_to_save.close()

        return file_name

    except ValueError:
        save_dir = finders.find("comprec/FP_filter/tmp/")
        file_name = os.path.join(save_dir, query_name + '_frequent-gap-fpfilter_ERROR.results')
        file_to_save = open(file_name, 'w')
        file_to_save.write("An error occurred. Wrong format of the MSA file.")
        file_to_save.close()
        return file_name

