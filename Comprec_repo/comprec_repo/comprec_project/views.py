from django.http import HttpResponseRedirect, HttpResponse
from django.core.servers.basehttp import FileWrapper
from django.contrib.staticfiles import finders
from comprec_project.amyload.models import Fragment
from django.core.mail import send_mail, EmailMessage
from comprec_project.forms import *
from django.shortcuts import render
from threading import Thread
from low_entropy import get_results_from_low_entropy
from frequent_gap import get_results_from_frequent_gap
from intra_secondary_structure import get_results_from_intra_secondary_structure
from inter_terminus import get_results_from_inter_terminus
from terminus import get_results_from_terminus
from buried_exposed import get_results_from_buried_exposed
import os, subprocess
import mimetypes
import tempfile
import shutil
import math
import uuid



def downloadDCAvsMisfolds(request, fileName=""):
	"""
	Set downloadable file with data from article about obsoletes results.
	
	@return: HttpResponse
	"""
	download_name = "comprec/" + fileName
	return downloadFile(download_name)

def dcaVSmisfolds(request):
	"""
	Redirects to the page with article about obsoletes results.
	@return: page render
	"""
	return render(request, 'comprec/dcaVSmisfolds.html', {'title': "DCA vs. Misfolds"})

def downloadDCAQ(request, fileName=""):
        """
	Set downloadable file with data from article about dcaQ results.

        @return: HttpResponse
        """
	download_name ="comprec/" + fileName
        return downloadFile(download_name)

def dcaQ(request):
        """
	Redirects to the page with article about dcaQ results.
        @return: page render
        """
	return render(request, 'comprec/dcaQ.html', {'title': "Forecasting of DCA prediction accuracy"})

def maintenance(request):
	"""
	Redirects to the maintenance page.
	@return: page render
	"""
	return render(request, 'maintenance.html')

def index(request):
	"""
	Redirects to the home page.
	@return: page render
	"""
	return render(request, 'comprec/home.html', {'title': "Home"})

def c2sInput(request):
	"""
	Redirects to the C2S form page.
	@return: page render
	"""
	c2s_form = C2SForm()
	return render(request, 'comprec/c2sInput.html', {'title': "C2S input", 'c2s_form': c2s_form})


def fishInput(request):
	"""
	Redirects to the FiSH form page.
	@return: page render
	"""
	fish_form = FishForm()
	return render(request, 'comprec/fishInput.html', {'title': "FISH input", 'fish_form': fish_form})

def fishAbout(request):
	"""
	Redirects to the FiSH about page.
	@return: page render
	"""
	return render(request, 'comprec/fishAbout.html', {'title': "FISH about"})

def amyloadAbout(request):
	"""
	Redirects to the AmyLoad short about page.
	@return: page render
	"""
	howMany = len(Fragment.objects.filter(reviewed=1).all())
	return render(request, 'comprec/amyloadAbout.html', {'title': "AmyLoad about", 'howMany': howMany})

def downloadFishPlot(request):
	"""
	Set downloadable plot data file at FiSH about page to 'fish_plot.txt' file.
	
	@return: HttpResponse
	"""
	download_name ="comprec/fish_plot.txt"
	return downloadFile(download_name)

def downloadFishData(request):
	"""
	Set downloadable FiSH data file at FiSH about page to 'fish_data.txt' file.
	
	@return: HttpResponse
	"""
	download_name ="comprec/fish_data.txt"
	return downloadFile(download_name)

def downloadC2S(request):
	"""
	Set downloadable C2S download file at C2S about page to 'C2S_src_v1_3.tar.gz' file.
	
	@return: HttpResponse
	"""
	download_name ="comprec/C2S_src_v1_3.tar.gz"
	return downloadFile(download_name)

def downloadC2SInput(request):
        """
	Set downloadable C2S input files at C2SInput page to 'C2S_input.zip' file.

        @return: HttpResponse
        """
	download_name ="comprec/C2S_Input.zip"
        return downloadFile(download_name)

def downloadPCO(request):
	"""
	Set downloadable PCO download file at PCO about page to 'PCO_v16_1.owl' file.
	
	@return: HttpResponse
	"""
	download_name ="comprec/PCO_v16_1.owl"
	return downloadFile(download_name)

def downloadFile(download_name):
	"""
	Get proper static file to download on Help page.
	
	@param download_name: name of the file to download
	@type download_name: string
	@return: HttpResponse
	"""
	
	filename = finders.find(download_name)
	wrapper = FileWrapper(open(filename))
	content_type = mimetypes.guess_type(filename)[0]
	response = HttpResponse(wrapper,content_type=content_type)
	response['Content-Length'] = os.path.getsize(filename)    
	response['Content-Disposition'] = "attachment; filename=%s"%download_name
	return response

def c2s(request):
	"""
	Redirects to the C2S about page.
	@return: page render
	"""
	return render(request, 'comprec/C2S.html', {'title': "C2S - Contacts-to-Structure"})

def pco(request):
	"""
	Redirects to the PCO about page.
	@return: page render
	"""
	return render(request, 'comprec/PCO.html', {'title': "PCO - Protein Contacts Ontology"})

def fishRun(request):
	"""
	Run FiSH.
	@return: page render
	"""
	if request.method == 'POST':
		#get data from fish form:
		fish_form = FishForm(data=request.POST)
		#check if fish form is valid:
		if(fish_form.is_valid()):
			sequence = fish_form.data['fishSequence'].strip().upper()
			threshold = fish_form.data['threshold'].strip()
			prediction = fish_form.data['prediction'].strip()
			randCode = uuid.uuid4()
			if(len(sequence) < 10):
				lower = int(math.ceil((len(sequence)/3)))
				upper = int(math.ceil((len(sequence)/3)*2))
			else:
				lower = 5
				upper = len(sequence)-5
			outputName = sequence[1:lower] + "-" + str(randCode) + "-" + sequence[upper:]
			sourceDir = finders.find("comprec/FISH_src/")
			outputDir = finders.find("comprec/FISH_tmp/")
			os.chdir(sourceDir)
			command = ["octave","fish.m",sequence,threshold,prediction,outputDir + "/" + outputName]
			process = subprocess.Popen(command).wait() #wait until it is done; the same as "subprocess.call(command)"
			#stdoutdata, stderrdata = process.communicate()
			#print process.returncode
			content = []
			with open(outputDir + "/" + outputName) as fp:
				for line in fp:
					content.append(line.split())
			#content = open(outputDir + "/" + outputName, 'r').read() #<pre>{{content}}</pre>
			os.remove(outputDir + "/" + outputName)
			return render(request, 'comprec/fishRes.html', {'title': "FISH result", 'content': content, 'sequence': sequence, 'threshold':threshold, 'prediction':prediction})
	else:
		fish_form = FishForm()
	return render(request, 'comprec/fishInput.html', {'title': "FISH input", 'fish_form': fish_form})

def c2sRun(request):
	"""
	Run C2S.
	@return: page render
	"""
	info = ""
	if request.method == 'POST':
		#get data from C2S form:
		c2s_form = C2SForm(request.POST, request.FILES)
		#check if C2S form is valid:
		if(c2s_form.is_valid()):
			saveDir = finders.find("comprec/C2S/cmap2struct_pipeline/tmp/")
			if(saveDir):
				contactDict = save_file(request.FILES["contactMap"], saveDir)
				fastaDict = save_file(request.FILES["fastaFile"], saveDir)
				threshold = request.POST['threshold']
				runNmb = request.POST['runNmb']
				email = request.POST['email']
				#run parallel process for C2S:
				th = Thread(name='c2sRun', target=backgroundC2SRun, args=(contactDict['filePath'], fastaDict['filePath'], threshold, runNmb, email, contactDict['filename'], request.FILES["contactMap"]._get_name()))
				th.setDaemon(True)
				th.start()
				info = "E-mail with the results will be sent to " + email + "."
	else:
		c2s_form = C2SForm()
	return render(request, 'comprec/c2sInput.html', {'title': "C2S input", 'c2s_form': c2s_form, 'info': info})

def c2sInitiate(request):
	"""
	Initiate C2S. Able after url uncomment and c2sInput template modification.
	@return: page render
	"""
	sourceDir = finders.find("comprec/C2S/")
	os.chdir(sourceDir)
	command = ["tar","-zxvf","cmap2struct_pipeline.tar.gz"]
	process = subprocess.Popen(command).wait()
	
	sourceDir = finders.find("comprec/C2S/cmap2struct_pipeline/")
	os.chdir(sourceDir)
	logfile = open("logInit.txt", "w")
	command = ["python","init_pipeline.py"]
	process = subprocess.Popen(command, stdout=logfile, stderr=logfile).wait()
	logfile.flush()
	logfile.close()
	
	info = ""
	c2s_form = C2SForm()
	return render(request, 'comprec/c2sInput.html', {'title': "C2S input", 'c2s_form': c2s_form, 'info': info})

def save_file(file, path):
	filename = file._get_name()
	filename = filename.split('.')
	randCode = uuid.uuid4()
	randomStr = str(randCode)[0:7]
	randomStr = randomStr.replace(".", "");
	filename = randomStr + "." + filename[-1]
	filePath = str(path) + "/" + str(filename)
	fd = open(filePath, 'wb')
	for chunk in file.chunks():
		fd.write(chunk)
	fd.close()
	return {'filename': filename, 'filePath': filePath}

def backgroundC2SRun(contactPath, fastaPath, threshold, runNmb, email, outputName, cmapName):
	outputName = outputName.split('.')
	dots = len(outputName)
	if(dots <= 2):
		sourceDir = finders.find("comprec/C2S/cmap2struct_pipeline/")
		os.chdir(sourceDir)
		command = ["python","c2s/generate_models.py", "-cmap", contactPath, "-threshold", threshold, "-seq", fastaPath, "num_run", runNmb]
		
		#logfile = open("logCalc.txt", "w")
		
		#process = subprocess.Popen(command, stdout=logfile, stderr=logfile).wait() #wait until it is done; the same as "subprocess.call(command)"
		process = subprocess.Popen(command).wait() #wait until it is done; the same as "subprocess.call$

		os.remove(contactPath)
		os.remove(fastaPath)
		
		#logfile.flush()
		#logfile.close()
		
		outputDir1 = finders.find("comprec/C2S/cmap2struct_pipeline/models/" + outputName[0] + "_final_results.zip")
		outputDir2 = finders.find("comprec/C2S/cmap2struct_pipeline/models/" + outputName[0] + "/")
		f = open(outputDir1)
		mail = EmailMessage("Results of C2S for " + cmapName, "Please, note that your zip file was named with your original cmap name, but the generated models inside are called using temporary name created by our system.\n\n" + "Thank you for using C2S.", 'C2S', [email])
		mail.attach(cmapName.split('.')[0] + "_final_results.zip", f.read())
		mail.send()
		os.remove(outputDir1)
		shutil.rmtree(outputDir2)
	else:
		mail = EmailMessage("Results of C2S for " + cmapName.split('.')[0], "Error: Try again.", 'C2S', [email])
		mail.send()


def fpFilterInput(request):
	"""
	Redirects to the FPFilter form page.
	@return: page render
	author: Julia Pelc
	"""
	fpfilter_form = FPFilterForm()
	fpfilter_choice_form = FPFilterChoiceForm()
	return render(
		request,
		'comprec/FPFilterInput.html',
		{'title': "FP Filter input", 'fpfilter_choice_form': fpfilter_choice_form, 'fpfilter_form': fpfilter_form})


def fpFilterRun(request):
	"""
	Run FP Filter.
	@return: page render
	author: Julia Pelc
	"""
	info = ""
	saveDir = finders.find("comprec/FP_filter/tmp/")
	# saveDir = finders.find('C:/MAGISTERKA_REPO/Comprec_repo/comprec_repo/static/tmp/')
	if request.method == 'POST':
		# get data from forms:
		fpfilter_form = FPFilterForm(request.POST, request.FILES)
		fpfilter_choice_form = FPFilterChoiceForm(request.POST)
		# check if forms are valid:
		if fpfilter_form.is_valid() and fpfilter_choice_form.is_valid():
			query_name = request.POST['QueryName']
			cov1 = request.POST['cut_off_1']
			cov2 = request.POST['cut_off_2']
			cov3 = request.POST['cut_off_3']
			cov5 = request.POST['cut_off_5']
			cov6t = request.POST['cut_off_6t']
			cov6rsa = request.POST['cut_off_6rsa']
			cov7a = request.POST['cut_off_7a']
			cov7b = request.POST['cut_off_7b']
			chosen_filters = fpfilter_choice_form.cleaned_data.get('filters')
			DCADict = save_file(request.FILES["DCAfile"], saveDir)

			if 'MSAfile' in request.FILES:
				MSADict = save_file(request.FILES['MSAfile'], saveDir)
				MSAPath = MSADict['filePath']
			else:
				MSAPath = 'empty'

			if 'SSfile' in request.FILES:
				SSDict = save_file(request.FILES['SSfile'], saveDir)
				SSPath = SSDict['filePath']
			else:
				SSPath = 'empty'

			if 'RSAfile' in request.FILES:
				RSADict = save_file(request.FILES['RSAfile'], saveDir)
				RSAPath = RSADict['filePath']
			else:
				RSAPath = 'empty'

			email = request.POST['email']
			th = Thread(name='fpFilterRun', target=backgroundfpFilterRun, args=(
				query_name,
				chosen_filters,
				DCADict['filePath'],
				MSAPath, SSPath, RSAPath,
				cov1, cov2, cov3, cov5, cov6t, cov6rsa, cov7a, cov7b,
				email))
			th.setDaemon(True)
			th.start()
			info = "E-mail with the results for " + query_name + " job will be sent to " + email + ". "
			fpfilter_choice_form = FPFilterChoiceForm()
	else:
		fpfilter_form = FPFilterForm()
		fpfilter_choice_form = FPFilterChoiceForm()
	return render(
		request,
		'comprec/FPFilterInput.html',
		{'title': "FP Filter input",
			'fpfilter_choice_form': fpfilter_choice_form,
			'fpfilter_form': fpfilter_form,
			'info': info})


def backgroundfpFilterRun(queryName, chosenFilters, DCAPath, MSAPath, SSPath, RSAPath, cov1, cov2, cov3, cov5, cov6t, cov6rsa, cov7a, cov7b, email):
	"""
	Run FP Filter tool in the backgrond.
	author: Julia Pelc
	"""
	sourceDir = finders.find("comprec/FP_filter/")
	os.chdir(sourceDir)

	chosen_filters = [s.replace('u', '') for s in chosenFilters]

	for i in range(len(chosen_filters)):
		j = int(chosen_filters[i])
		if j == 0:
			file_path = get_results_from_low_entropy(DCAPath, MSAPath, cov1, queryName)
			# send_email(queryName, email, file_path)
			continue
		if j == 1:
			file_path = get_results_from_frequent_gap(DCAPath, MSAPath, cov2, queryName)
			# send_email(queryName, email, file_path)
			continue
		if j == 2:
			file_path = get_results_from_intra_secondary_structure(DCAPath, SSPath, cov3, queryName)
			# send_email(queryName, email, file_path)
			continue
		if j == 3:
			file_path = get_results_from_inter_terminus(DCAPath, MSAPath, cov5, queryName)
			# send_email(queryName, email, file_path)
			continue
		if j == 4:
			file_path = get_results_from_terminus(DCAPath, RSAPath, cov6t, cov6rsa, queryName)
			# send_email(queryName, email, file_path)
			continue
		if j == 5:
			file_path = get_results_from_buried_exposed(DCAPath, RSAPath, cov7a, cov7b, queryName)
			# send_email(queryName, email, file_path)
			continue

	os.remove(DCAPath)
	if os.path.exists(MSAPath):
		os.remove(MSAPath)
	if os.path.exists(SSPath):
		os.remove(SSPath)
	if os.path.exists(RSAPath):
		os.remove(RSAPath)


def downloadFpFilterInput(request):
	"""
	Set downloadable FP filter input files at FpFilterInput page to 'FP-Filter_input.zip' file.
	@return: HttpResponse
	author: Julia Pelc
	"""
	download_name = "comprec/FP_filter/FPFilters_Input.zip"
	return downloadFile(download_name)


def send_email(queryName, emailAdress, filePath):
	"""
	Sends email with generated file from fp filters.
	author: Julia Pelc
	"""
	info = "Results for your query are in the attached file. Thank you for using FP Filter."
	mail = EmailMessage("Results of FPFilter for " + queryName, info, 'FP Filters', [emailAdress])
	mail.attach_file(filePath)
	mail.send()
