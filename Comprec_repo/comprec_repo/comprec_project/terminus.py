import os
from django.contrib.staticfiles import finders
from loadDCAFile import load_dca_file


def get_results_from_terminus(DCAPath, RSAfileID, tCutOffValue, rsaCutOffValue, query_name):
    try:
        # load DCA file
        DCA = load_dca_file(DCAPath)

        # load Relative Solvent Accessibility (RSA) file
        file = open(RSAfileID, "r")
        RSA = file.read()
        file.close()

        RSA = RSA.split(" ")

        tCutOffValue = int(tCutOffValue)
        rsaCutOffValue = int(rsaCutOffValue)

        sequenceLength = len(RSA)  # length of target sequence

        DCAremoved = []
        n = sequenceLength - tCutOffValue + 1
        i = 0
        while (i - len(DCAremoved)) <= 200:
            posA = int(DCA[i][0])
            posB = int(DCA[i][1])
            RSAposA = int(RSA[posA-1])
            RSAposB = int(RSA[posB-1])
            if RSAposA > rsaCutOffValue and RSAposB > rsaCutOffValue:
                if posA <= tCutOffValue or posB <= tCutOffValue or posA >= n or posB >= n:
                    DCAremoved.append(DCA[i])
            i = i+1

        # create file with new DCA (with filtered out pairs with both RSAs>35
        # when eather of them is on n- or c-terminus)
        save_dir = finders.find("comprec/FP_filter/tmp/")
        file_name = os.path.join(save_dir, query_name + '_terminus-fpfilter_t-' + str(tCutOffValue) + '_rsa-' + str(rsaCutOffValue) + '.results')
        file_to_save = open(file_name, 'w')
        for line in DCAremoved:
            file_to_save.write(str(line[0]) + ',' + str(line[1]) + ',' + str(line[2]) + '\n')
        file_to_save.close()

        return file_name

    except IndexError:
        save_dir = finders.find("comprec/FP_filter/tmp/")
        file_name = os.path.join(save_dir, query_name + '_terminus-fpfilter_ERROR.results')
        file_to_save = open(file_name, 'w')
        file_to_save.write("An error occurred. Wrong format of the RSA file.")
        file_to_save.close()
        return file_name
