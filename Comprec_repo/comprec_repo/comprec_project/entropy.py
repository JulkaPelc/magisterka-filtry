import numpy as np
import math


def count_entropy(MSA):

    sequenceLength = len(MSA[0])-1  # length of target sequence
    H = np.zeros(sequenceLength)  # empty array for entropy at each position
    amino_acid_types = "ARNDCEQGHILKMFPSTWYV-"

    for k in range(sequenceLength):
        amino_acid_frequency = np.zeros(21)
        for j in range(len(MSA)):
            for i in range(len(amino_acid_types)):
                if MSA[j][k] == amino_acid_types[i]:
                    amino_acid_frequency[i] = amino_acid_frequency[i] + 1
                    break
        for t in range(len(amino_acid_frequency)):
            f = amino_acid_frequency[t] / len(MSA)
            if f > 0:
                H[k] = H[k] + (f * math.log(f, math.e)/math.log(21, math.e))
        H[k] = -H[k]

    return H
