from django import forms
from django.forms import ModelForm  # Pelc
from .models import FPFilter  # Pelc

PREDICTION = (('0', 'Standard'), ('1', 'Full'))


class FishForm(forms.Form):
	"""
	FiSH form.
	"""
	threshold = forms.FloatField(initial=0.19, label="Threshold:", widget=forms.NumberInput(attrs={'class':'special', 'style':'width: 100px;', 'min': '0'}))
	prediction = forms.ChoiceField(required=False, choices=PREDICTION, label="Prediction:")
	fishSequence = forms.CharField(required=False, label="Insert sequence in one letter code:", widget=forms.Textarea(attrs={'cols': 140, 'rows': 5, 'style':'resize:none;'}))
	
	def clean_fishSequence(self):
		sequence = self.cleaned_data["fishSequence"]
		if(sequence == ''):
			raise forms.ValidationError('Sequence field is required.')
		elif(not sequence == '' and not sequence.isalpha()):
			raise forms.ValidationError('Sequence must consist letters only.')
		else:
			return sequence


class C2SForm(forms.Form):
	"""-+
	C2S form.
	"""
	contactMap = forms.FileField(required=True, label="Contact map file:")
	fastaFile = forms.FileField(required=True, label="FASTA file:")
	threshold = forms.IntegerField(required=True, label="Contact cutoff:", initial=8, widget=forms.NumberInput(attrs={'class':'special', 'style':'width: 100px;', 'min': '8', 'max': '12'}))
	runNmb = forms.IntegerField(required=True, label="Run number:", initial=1, widget=forms.NumberInput(attrs={'class':'special', 'style':'width: 100px;', 'min': '1', 'max': '10'}))
	email = forms.EmailField(required=True, label="Email:")


class FPFilterForm(ModelForm):
	"""
	FP filter form.
	author: Julia Pelc
	"""
	class Meta:
		model = FPFilter
		fields = ['QueryName', 'DCAfile', 'MSAfile', 'SSfile', 'RSAfile', 'email']
		labels = {
			'QueryName': 'Query name:',
			'DCAfile': 'DCA file:',
			'MSAfile': 'MSA file:',
			'SSfile': 'Secondary Structure  file:',
			'RSAfile': 'Relative Solvent Accessibility file:',
			'email': 'Email:',
		}


class FPFilterChoiceForm(forms.Form):
	"""
	FP filter choice form.
	author: Julia Pelc
	"""
	FILTER_CHOICES = (
		('0', 'Low entropy'),
		('1', 'Frequent gap'),
		('2', 'Inta-secondary structure'),
		('3', 'Inter-terminus'),
		('4', 'Terminus'),
		('5', 'Buried-exposed'))
	filters = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=FILTER_CHOICES)
	cut_off_1 = forms.FloatField(widget=forms.NumberInput(attrs={'id': 'cov-1', 'step': '0.01', 'style': 'width:10ch'}), min_value=0.01, max_value=0.95, initial=0.02)
	cut_off_2 = forms.FloatField(widget=forms.NumberInput(attrs={'id': 'cov-2', 'step': '1', 'style': 'width:10ch'}), min_value=1, max_value=99, initial=95)
	cut_off_3 = forms.FloatField(widget=forms.NumberInput(attrs={'id': 'cov-3', 'step': '1', 'style': 'width:10ch'}), min_value=5, max_value=25, initial=5)
	cut_off_5 = forms.FloatField(widget=forms.NumberInput(attrs={'id': 'cov-5', 'step': '0.01', 'style': 'width:10ch'}), min_value=0.2, max_value=0.99, initial=0.95)
	cut_off_6t = forms.FloatField(widget=forms.NumberInput(attrs={'id': 'cov-6t', 'step': '1', 'style': 'width:10ch'}), min_value=1, max_value=20, initial=15)
	cut_off_6rsa = forms.FloatField(widget=forms.NumberInput(attrs={'id': 'cov-6rsa', 'step': '5', 'style': 'width:10ch'}), min_value=5, max_value=95, initial=35)
	cut_off_7a = forms.FloatField(widget=forms.NumberInput(attrs={'id': 'cov-7a', 'step': '1', 'style': 'width:10ch'}), min_value=-10, max_value=55, initial=20)
	cut_off_7b = forms.FloatField(widget=forms.NumberInput(attrs={'id': 'cov-7b', 'step': '1', 'style': 'width:10ch'}), min_value=0, max_value=140, initial=120)
