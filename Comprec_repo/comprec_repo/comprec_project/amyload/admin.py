from django.contrib import admin
from comprec_project.amyload.models import *

class ProteinAdmin(admin.ModelAdmin):
	"""
	Set display options for protein list in admin panel.
	"""
	list_display = ('id', 'name', 'date', 'reviewed', 'get_username')
	list_display_links = ('name',)
	ordering = ['name']
	
	def get_username(self, obj):
		return obj.user.username
	
	get_username.short_description = 'Username'
	get_username.admin_order_field = 'user__username'

class MethodAdmin(admin.ModelAdmin):
	"""
	Set display options for method list in admin panel.
	"""
	list_display = ('id', 'name', 'date', 'reviewed', 'get_username')
	list_display_links = ('name',)
	ordering = ['name']
	
	def get_username(self, obj):
		return obj.user.username
	
	get_username.short_description = 'Username'
	get_username.admin_order_field = 'user__username'

class CitationAdmin(admin.ModelAdmin):
	"""
	Set display options for citation list in admin panel.
	"""
	list_display = ('id', 'name', 'pmid', 'date', 'reviewed', 'get_username')
	list_display_links = ('name',)
	ordering = ['name']
	
	def get_username(self, obj):
		return obj.user.username
	
	get_username.short_description = 'Username'
	get_username.admin_order_field = 'user__username'

class DiseaseAdmin(admin.ModelAdmin):
	"""
	Set display options for disease list in admin panel.
	"""
	list_display = ('id', 'name', 'date', 'reviewed', 'get_username')
	list_display_links = ('name',)
	ordering = ['name']
	
	def get_username(self, obj):
		return obj.user.username
	
	get_username.short_description = 'Username'
	get_username.admin_order_field = 'user__username'

class SourceCitationsInline(admin.TabularInline):
	model = Source.citations.through

def delete_chosen_sources(modeladmin, request, queryset):
	#delete sources one by one:
	for q in queryset:
		q.delete()
delete_chosen_sources.short_description = "Delete selected one by one"
class SourceAdmin(admin.ModelAdmin):
	"""
	Set display options for source list in admin panel.
	"""
	inlines = [
		SourceCitationsInline,
	]
	exclude = ('citations',)
	list_display = ('id', 'name', 'date', 'reviewed', 'get_username')
	list_display_links = ('name',)
	ordering = ['name']
	actions = [delete_chosen_sources]
	
	def get_username(self, obj):
		return obj.user.username
	
	get_username.short_description = 'Username'
	get_username.admin_order_field = 'user__username'

class FragmentMethodsInline(admin.TabularInline):
	model = Fragment.methods.through
class FragmentSourcesInline(admin.TabularInline):
	model = Fragment.sources.through
class FragmentCitationsInline(admin.TabularInline):
	model = Fragment.citations.through

def magic(modeladmin, request, queryset):
	#do special operations:
	if(len(queryset) == 7):
		print "No magic to do"
		#import os
		#from django.contrib.auth.models import User
		#import datetime
		#f = open(os.path.dirname(__file__) + "/diseases", 'r')
		#userAdmin = User.objects.filter(username="moher").all()[0]
		#for line in f:
		#	line = line.strip()
		#	splitter = line.split(";")
		#	foundDis = Disease.objects.filter(name=splitter[1]).all()
		#	if(foundDis):
		#		fragment = Fragment.objects.filter(amyindex=splitter[0]).all()[0]
		#		fragment.diseases.add(foundDis[0])
		#	else:
		#		disease = Disease.objects.create(name=splitter[1], date=datetime.datetime.now(), reviewed=True, user=userAdmin)
		#		fragment = Fragment.objects.filter(amyindex=splitter[0]).all()[0]
		#		fragment.diseases.add(disease)
magic.short_description = "Do some magic"
def delete_chosen_fragments(modeladmin, request, queryset):
	#delete fragments one by one:
	for q in queryset:
		q.delete()
delete_chosen_fragments.short_description = "Delete selected one by one"
def make_fragments_reviewed(modeladmin, request, queryset):
	#delete fragments one by one:
	for q in queryset:
		q.reviewed = 1;
		q.save()
make_fragments_reviewed.short_description = "Change to reviewed"
class FragmentAdmin(admin.ModelAdmin):
	"""
	Set display options for fragment list in admin panel.
	"""
	inlines = [
		FragmentMethodsInline,
		FragmentSourcesInline,
		FragmentCitationsInline,
	]
	exclude = ('methods', 'sources', 'citations',)
	list_display = ('id', 'amyindex', 'name', 'get_protein', 'sequence', 'reviewed', 'get_username', 'ifaggregate', 'date', 'get_length')
	list_display_links = ('name',)
	ordering = ['name']
	actions = [delete_chosen_fragments, make_fragments_reviewed, magic]
	
	def get_protein(self, obj):
		return obj.protein.name
	
	get_protein.short_description = 'Protein'
	get_protein.admin_order_field = 'protein__name'
	
	def get_username(self, obj):
		return obj.user.username
	
	get_username.short_description = 'Username'
	get_username.admin_order_field = 'user__username'
	
	def get_length(self, obj):
		return str(len(obj.sequence))
	
	get_length.short_description = 'Length'

class SessionAdmin(admin.ModelAdmin):
	"""
	Set display options for session list in admin panel.
	"""
	list_display = ('id', 'name', 'get_username', 'date', 'saved')
	list_display_links = ('name',)
	ordering = ['date']
	
	def get_username(self, obj):
		return obj.user.username
	
	get_username.short_description = 'Username'
	get_username.admin_order_field = 'user__username'

class UserProfileAdmin(admin.ModelAdmin):
	"""
	Set display options for user profile list in admin panel.
	"""
	list_display = ('id', 'get_username', 'get_first_name', 'get_last_name')
	list_display_links = ('get_username',)
	ordering = ['id']
	
	def get_username(self, obj):
		return obj.user.username
	
	def get_first_name(self, obj):
		return obj.user.first_name
	
	def get_last_name(self, obj):
		return obj.user.last_name
	
	get_username.short_description = 'Username'
	get_username.admin_order_field = 'user__username'
	
	get_first_name.short_description = 'First name'
	get_first_name.admin_order_field = 'user__first_name'
	
	get_last_name.short_description = 'Last name'
	get_last_name.admin_order_field = 'user__last_name'

admin.site.register(Protein, ProteinAdmin)
admin.site.register(Method, MethodAdmin)
admin.site.register(Citation, CitationAdmin)
admin.site.register(Disease, DiseaseAdmin)
admin.site.register(Source, SourceAdmin)
admin.site.register(Fragment, FragmentAdmin)
admin.site.register(Session, SessionAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
