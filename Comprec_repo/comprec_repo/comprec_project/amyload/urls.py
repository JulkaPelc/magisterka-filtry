from django.conf.urls import patterns, include, url
from comprec_project.amyload import views

urlpatterns = patterns('',
	url(r'^user/password/reset/$', 'django.contrib.auth.views.password_reset', {'post_reset_redirect' : '/amyload/user/password/reset/done/'}),
	url(r'^user/password/reset/done/$', 'django.contrib.auth.views.password_reset_done'),
	url(r'^user/password/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm', {'post_reset_redirect' : '/amyload/user/password/done/'}),
	url(r'^user/password/done/$', 'django.contrib.auth.views.password_reset_complete'),
	url(r'^$',views.index),
	url(r'^register/$', views.register),
	url(r'^confirm/(?P<activation_key>\w+)/$', views.register_confirm),
	url(r'^database/$', views.database),
	url(r'^databaseFilter/$', views.databaseFilter),
	url(r'^databaseHowMany/$', views.databaseHowMany),
	url(r'^databaseNext/$', views.databaseNext),
	url(r'^databasePrevious/$', views.databasePrevious),
	url(r'^databaseMove/$', views.databaseMove),
	url(r'^databaseRemove/$', views.databaseRemove),
	url(r'^databaseAllFasta/$', views.databaseAllFasta),
	url(r'^databaseHowManyMoved/$', views.databaseHowManyMoved),
	url(r'^databaseNextMoved/$', views.databaseNextMoved),
	url(r'^databasePreviousMoved/$', views.databasePreviousMoved),
	url(r'^databaseAllXml/$', views.databaseAllXml),
	url(r'^databaseAllSsv/$', views.databaseAllSsv),
	url(r'^databaseAllCsv/$', views.databaseAllCsv),
	url(r'^databaseSaveSession/$', views.databaseSaveSession),
	url(r'^insertXml/$', views.insertXml),
	url(r'^aboutYou/$', views.aboutYou),
	url(r'^changeEmail/$', views.changeEmail),
	url(r'^changePassword/$', views.changePassword),
	url(r'^addedFrags/$', views.addedFrags),
	url(r'^addedFilter/$', views.addedFilter),
	url(r'^addedHowMany/$', views.addedHowMany),
	url(r'^addedNext/$', views.addedNext),
	url(r'^addedPrevious/$', views.addedPrevious),
	url(r'^savedSessions/$', views.savedSessions),
	url(r'^savedFilter/$', views.savedFilter),
	url(r'^savedHowMany/$', views.savedHowMany),
	url(r'^savedPrevious/$', views.savedPrevious),
	url(r'^savedNext/$', views.savedNext),
	url(r'^loadSaved/$', views.loadSaved),
	url(r'^removeSaved/$', views.removeSaved),
	url(r'^newSession/$', views.newSession),
	url(r'^databaseChosenFasta/$', views.databaseChosenFasta),
	url(r'^databaseChosenXml/$', views.databaseChosenXml),
	url(r'^databaseChosenCsv/$', views.databaseChosenCsv),
	url(r'^databaseChosenSsv/$', views.databaseChosenSsv),
	url(r'^analysisPage/$', views.analysisPage),
	url(r'^performAnalysis/$', views.performAnalysis),
	url(r'^fragmentInfo/(?P<fragId>\w+)/$', views.fragmentInfo),
	url(r'^addedFragmentInfo/(?P<fragId>\w+)/$', views.addedFragmentInfo),
	url(r'^sendComment/(?P<fragId>\w+)/$', views.sendComment),
	url(r'^login/$', views.user_login),
	url(r'^logout/$', views.user_logout),
	url(r'^insertForm/$', views.insertForm),
	url(r'^help/$', views.help),
	url(r'^downloadXML1/$', views.downloadXML1),
	url(r'^downloadXML2/$', views.downloadXML2),
	url(r'^downloadFasta/$', views.downloadFasta),
)
