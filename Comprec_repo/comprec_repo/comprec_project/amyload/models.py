from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import pre_delete, post_delete, post_save
from django.dispatch.dispatcher import receiver

from django.core.exceptions import ObjectDoesNotExist
from django.db.models.signals import m2m_changed

import datetime

class Protein(models.Model):
	name = models.CharField(max_length=100, unique=True)
	date = models.DateField()
	reviewed = models.BooleanField(default=False)
	user = models.ForeignKey(User, null=True)
	
	def __unicode__(self):
		return self.name
	
	def __str__(self):
		return '%s' % (self.name)

class Method(models.Model):
	name = models.CharField(max_length=100, unique=True)
	date = models.DateField()
	reviewed = models.BooleanField(default=False)
	user = models.ForeignKey(User, null=True)
	
	def __unicode__(self):
		return self.name
	
	def __str__(self):
		return '%s' % (self.name)

class Citation(models.Model):
	name = models.CharField(max_length=100)
	pmid = models.IntegerField(unique=True, null=True, blank=True)
	link = models.CharField(max_length=200, null=True, blank=True)
	date = models.DateField()
	reviewed = models.BooleanField(default=False)
	user = models.ForeignKey(User, null=True)
	
	def __unicode__(self):
		return '%s - PMID: %s' % (self.name, self.pmid)
	
	def __str__(self):
		return '%s' % (self.name)

class Disease(models.Model):
	name = models.CharField(max_length=200, unique=True)
	date = models.DateField()
	reviewed = models.BooleanField(default=False)
	user = models.ForeignKey(User, null=True)
	
	def __unicode__(self):
		return self.name
	
	def __str__(self):
		return '%s' % (self.name)

class Source(models.Model):
	name = models.CharField(max_length=100, unique=True)
	citations = models.ManyToManyField(Citation)
	date = models.DateField()
	reviewed = models.BooleanField(default=False)
	user = models.ForeignKey(User, null=True)
	
	def __unicode__(self):
		return self.name
	
	def __str__(self):
		return '%s' % (self.name)
	
	def __init__(self, *args, **kwargs):
		super(Source, self).__init__(*args, **kwargs)
		#create old_reviewed to see it in save():
		self.old_reviewed = self.reviewed
	
	def save(self, force_insert=False, force_update=False, *args, **kwargs):
		if self.old_reviewed == 0 and self.reviewed == 1:
			#review all its unreviewed (all) citations:
			for c in self.citations.all():
				c.reviewed = 1
				c.save()
		super(Source, self).save(force_insert, force_update, *args, **kwargs)
		self.old_reviewed = self.reviewed

class Fragment(models.Model):
	amyindex = models.IntegerField(unique=True, blank=False, null=False)
	name = models.CharField(max_length=100)
	sequence = models.TextField(max_length=1000)
	ifaggregate = models.BooleanField(default=False)
	protein = models.ForeignKey(Protein)
	solution = models.TextField(max_length=1000, blank=True)
	methods = models.ManyToManyField(Method, blank=True)
	sources = models.ManyToManyField(Source, blank=True)
	citations = models.ManyToManyField(Citation, blank=True)
	diseases = models.ManyToManyField(Disease, blank=True)
	reviewed = models.BooleanField(default=False)
	date = models.DateField()
	user = models.ForeignKey(User, null=True)
	note = models.TextField(max_length=1000, blank=True)
	
	def __str__(self):
		return 'AMY%s -> %s -> %s' % (self.amyindex, self.name, self.sequence)
	
	def __unicode__(self):
		return self.name
	
	def __init__(self, *args, **kwargs):
		super(Fragment, self).__init__(*args, **kwargs)
		#create old_reviewed to see it in save():
		self.old_reviewed = self.reviewed

	def save(self, force_insert=False, force_update=False, *args, **kwargs):
		if self.old_reviewed == 0 and self.reviewed == 1:
			#review all its unreviewed (all) methods:
			for m in self.methods.all():
				m.reviewed = 1
				m.save()
			#review all its unreviewed (all) sources:
			for s in self.sources.all():
				s.reviewed = 1
				s.save()
			#review all its unreviewed (all) citations:
			for c in self.citations.all():
				c.reviewed = 1
				c.save()
			#review all its unreviewed (all) diseases:
			for d in self.diseases.all():
				d.reviewed = 1
				d.save()
			self.protein.reviewed = 1
			self.protein.save()
		super(Fragment, self).save(force_insert, force_update, *args, **kwargs)
		self.old_reviewed = self.reviewed

class Session(models.Model):
	name = models.CharField(max_length=100, null=True)
	user = models.ForeignKey(User)
	date = models.DateField()
	fragments = models.ManyToManyField(Fragment, blank=True)
	saved = models.BooleanField(default=False)
	
	def __str__(self):
		return 'id%s - %s - %s - %s' % (self.id, self.date, self.user.username, self.saved)
	
	def __unicode__(self):
		return str(self.name)

class UserProfile(models.Model):
	user = models.OneToOneField(User)
	activation_key = models.CharField(max_length=40, blank=True)
	key_expires = models.DateField(default=datetime.datetime.now()+datetime.timedelta(2))
	affiliation = models.CharField(max_length=200, blank=True)
	comment = models.CharField(max_length=1000, blank=True)
	
	def __str__(self):
		return self.user.username
	
	class Meta:
		verbose_name_plural=u'User profiles'

@receiver(pre_delete, sender=Fragment)
def Fragment_pre_delete(sender, instance, **kwargs):
	"""
	What to do before deleting a Fragment.
	"""
	#delete all its unreviewed methods:
	for m in instance.methods.all():
		if(m.reviewed == 0 and len(Fragment.objects.filter(methods__id=m.id).all()) == 1):
			m.delete()
	#delete all its unreviewed sources:
	for s in instance.sources.all():
		if(s.reviewed == 0 and len(Fragment.objects.filter(sources__id=s.id).all()) == 1):
			s.delete()
	#delete all its unreviewed citations:
	for c in instance.citations.all():
		if(c.reviewed == 0 and len(Fragment.objects.filter(citations__id=c.id).all()) == 1):
			c.delete()
	#delete all its unreviewed diseases:
	for d in instance.diseases.all():
		if(d.reviewed == 0 and len(Fragment.objects.filter(diseases__id=d.id).all()) == 1):
			d.delete()

@receiver(post_delete, sender=Fragment)
def Fragment_post_delete(sender, instance, **kwargs):
	"""
	What to do after deleting a Fragment.
	"""
	#delete its unreviewed protein:
	if(instance.protein.reviewed == 0 and len(Fragment.objects.filter(protein__id=instance.protein.id).all()) == 0):
		instance.protein.delete()

@receiver(pre_delete, sender=Source)
def Source_pre_delete(sender, instance, **kwargs):
	"""
	What to do before deleting a Source.
	"""
	#delete all its unreviewed citations:
	for c in instance.citations.all():
		if(c.reviewed == 0 and len(Source.objects.filter(citations__id=c.id).all()) == 1):
			c.delete()

@receiver(post_save, sender=Session)
def Session_post_save(sender, instance, **kwargs):
	"""
	What to do after saving a session.
	"""
	#delete all users unsaved sessions (except one - temporary browsing session):
	unsaved = Session.objects.filter(user__id=instance.user.id, saved=0).all()
	length = len(unsaved)
	if(length != 1 and length != 0):
		for u in unsaved[0:length-1]:
			u.delete()
