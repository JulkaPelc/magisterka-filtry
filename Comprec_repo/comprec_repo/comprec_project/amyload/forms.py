from django import forms
from django.core import validators
from comprec_project.amyload.models import *
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
import datetime
import re
from django.forms import extras
from email.utils import parseaddr

AGGREGATES = (('-1', 'Amyloidogenic: Yes/No'), ('1', 'Amyloidogenic: Yes'), ('0', 'Amyloidogenic: No'))
REVIEW = (('-1', 'Reviewed: Yes/No'), ('1', 'Reviewed: Yes'), ('0', 'Reviewed: No'))
AGGREGATES_INSERT = (('1', 'Yes'), ('0', 'No'))
SORT = (('-1', 'Sort by'), ('0', 'Protein Name'), ('1', 'Fragment Name'), ('2', 'Sequence Length'), ('3', 'Fibril Former'),)
ADDSORT = (('-1', 'Sort by'), ('0', 'Protein Name'), ('1', 'Fragment Name'), ('2', 'Sequence Length'), ('3', 'Fibril Former'), ('4', 'Review'), ('5', 'Date'))
SESSIONSORT = (('-1', 'Sort by'), ('0', 'Session Name'), ('1', 'Date'))
ASCDESC = (('0', 'Ascending'), ('1', 'Descending'))
REV = (('-1', 'Review'), ('1', 'Reviewed'), ('0', 'Unreviewed'))
FOLDAMYLOID = (('1', 'Expected number of contacts 8A'), ('2', 'Bone-bone donors'), ('3', 'Hybrid (contacts + donors)'), ('4', 'Bone-bone acceptors'), ('5', 'Triple hybrid (contacts + donors + acceptors)'), )
PREDICTION = (('0', 'Standard'), ('1', 'Full'))

class SortingForm(forms.Form):
	"""
	Sorting form on Database page.
	"""
	proteins = forms.ModelChoiceField(queryset=Protein.objects.filter(reviewed=1).order_by('name'), empty_label="Protein Name", widget=forms.Select(attrs={'style': 'width:300px'}))
	ifaggregate = forms.ChoiceField(choices=AGGREGATES)
	minlength = forms.IntegerField(widget=forms.NumberInput(attrs={'class':'special', 'style':'width: 100px;', 'placeholder': 'Min Length', 'min': '0'}))
	maxlength = forms.IntegerField(widget=forms.NumberInput(attrs={'class':'special', 'style':'width: 100px;', 'placeholder': 'Max Length', 'min': '0'}))
	bywhatsort = forms.ChoiceField(choices=SORT)
	ascordesc = forms.ChoiceField(choices=ASCDESC)
	ifcontainProt = forms.CharField(widget=forms.TextInput(attrs={'class':'special', 'size': '105', 'placeholder': 'Input word and search for protein names which contain it. Browser is case-insensitive.'}))
	ifcontainSeq = forms.CharField(widget=forms.TextInput(attrs={'class':'special', 'size': '105', 'placeholder': 'Input amino acid sequence and search for protein sequences which contain it. Browser is case-insensitive.'}))

class RegistrationForm(UserCreationForm):
	"""
	Registration form on Registration page.
	"""
	first_name = forms.CharField(required=True)
	last_name = forms.CharField(required=True)
	email = forms.EmailField(max_length=User._meta.get_field('username').max_length, required=True, label="Email address")
	affiliation = forms.CharField(max_length=UserProfile._meta.get_field('affiliation').max_length, required=False, label="Affiliation")
	comment = forms.CharField(max_length=UserProfile._meta.get_field('comment').max_length, required=False, label="Why you are going to use this site", widget=forms.Textarea(attrs={'cols': 105, 'rows': 5, 'style':'resize:none;', 'maxlength':UserProfile._meta.get_field('comment').max_length}))
	
	class Meta:
		model = User
		fields = ('first_name', 'last_name', 'username', 'email', 'password1', 'password2')
	
	def clean_email(self):
		email = self.cleaned_data["email"]
		try:
			User._default_manager.get(email=email)
		except User.DoesNotExist:
			return email
		raise forms.ValidationError('This email is already in our database.')
	
	def clean_first_name(self):
		first_name = self.cleaned_data["first_name"]
		if(not first_name):
			raise forms.ValidationError('This field is required.')
		else:
			return first_name
	
	def clean_last_name(self):
		last_name = self.cleaned_data["last_name"]
		if(not last_name):
			raise forms.ValidationError('This field is required.')
		else:
			return last_name
	
	def save(self, commit=True):        
		user = super(RegistrationForm, self).save(commit=False)
		user.email = self.cleaned_data['email']
		if commit:
			user.is_active = False
			user.save()
		return user

def today():
    return datetime.date.today().isoformat()

class InsertForm(forms.Form):
	"""
	Sequence submit form on 'Submit seq' page.
	"""
	name = forms.CharField(max_length=Fragment._meta.get_field('name').max_length, required=False, label="Insert fragment name:")
	sequence = forms.CharField(max_length=Fragment._meta.get_field('sequence').max_length, required=False, label="Insert sequence:*", widget=forms.Textarea(attrs={'cols': 105, 'rows': 5, 'style':'resize:none;', 'maxlength':Fragment._meta.get_field('sequence').max_length}))
	ifaggregate = forms.ChoiceField(choices=AGGREGATES_INSERT, label="Set aggregation:*")
	proteinExist = forms.ModelChoiceField(queryset=Protein.objects.filter(reviewed=1).order_by('name'), required=False, label="Choose source protein:*", widget=forms.Select(attrs={'style': 'width:250px'}))
	proteinName = forms.CharField(max_length=Protein._meta.get_field('name').max_length, required=False, label="or define new:**")
	solution = forms.CharField(max_length=Fragment._meta.get_field('solution').max_length, required=False, label="Insert information about solution:", widget=forms.Textarea(attrs={'cols': 105, 'rows': 5, 'style':'resize:none;', 'maxlength':Fragment._meta.get_field('solution').max_length}))
	methodExist = forms.ModelChoiceField(queryset=Method.objects.filter(reviewed=1).order_by('name'), required=False, label="Choose method name:", widget=forms.Select(attrs={'style': 'width:250px'}))
	methodNames = forms.CharField(required=False, label="or type by yourself:**")
	diseaseExist = forms.ModelChoiceField(queryset=Disease.objects.filter(reviewed=1).order_by('name'), required=False, label="Choose disease name:", widget=forms.Select(attrs={'style': 'width:250px'}))
	diseaseNames = forms.CharField(required=False, label="or type by yourself:**")
	sourceExist = forms.ModelChoiceField(queryset=Source.objects.filter(reviewed=1).order_by('name'), required=False, label="Choose other database which contains it:", widget=forms.Select(attrs={'style': 'width:250px'}))
	sourceName = forms.CharField(max_length=Source._meta.get_field('name').max_length, required=False, label="or define new - database name:**")
	sourceCitName = forms.CharField(max_length=Citation._meta.get_field('name').max_length, required=False, label="database reference name:")
	sourceCitPmid = forms.IntegerField(label="database reference PMID:", required=False,)
	sourceCitUrl = forms.URLField(max_length=Citation._meta.get_field('link').max_length, required=False, label="or database reference url:")
	citationExist = forms.ModelChoiceField(queryset=Citation.objects.filter(reviewed=1).order_by('name'), required=False, label="Choose reference:", widget=forms.Select(attrs={'style': 'width:250px'}))
	citationName = forms.CharField(max_length=Citation._meta.get_field('name').max_length, required=False, label="or define new - name:**")
	citationPmid = forms.IntegerField(label="and PMID:", required=False)
	citationUrl = forms.URLField(max_length=Citation._meta.get_field('link').max_length, required=False, label="or url:")
	note = forms.CharField(max_length=Fragment._meta.get_field('note').max_length, required=False, label="Remarks for curator:", widget=forms.Textarea(attrs={'cols': 105, 'rows': 5, 'style':'resize:none;', 'maxlength':Fragment._meta.get_field('note').max_length}))
	
	class Meta:
		model = Fragment
	
	def clean_name(self):
		name = self.cleaned_data["name"]
		if(not len(name) <= Fragment._meta.get_field('name').max_length):
			raise forms.ValidationError('Name is too long - max ' + str(Fragment._meta.get_field('name').max_length) + '.')
		else:
			return name
	
	def clean_sequence(self):
		sequence = self.cleaned_data["sequence"]
		if(sequence == ''):
			raise forms.ValidationError('Sequence field is required. See *.')
		#elif(not sequence == '' and not sequence.isalpha()):
		#	raise forms.ValidationError('Sequence must consist letters only.')
		elif(not len(sequence) <= Fragment._meta.get_field('sequence').max_length):
			raise forms.ValidationError('Sequence is too long  - max ' + str(Fragment._meta.get_field('sequence').max_length) + '.')
		#elif(Fragment.objects.filter(sequence=sequence)):
		#	raise forms.ValidationError('There is already such a sequence in the database.')
		else:
			return sequence
	
	def clean_proteinName(self):
		cleaned_data = super(InsertForm, self).clean()
		proteinExist = cleaned_data.get("proteinExist")
		proteinName = cleaned_data.get("proteinName")
		if(proteinExist == None and proteinName == ''):
			raise forms.ValidationError('Protein field is required. See *.')
		elif(proteinExist != None and proteinName != ''):
			raise forms.ValidationError('Cannot fill both protein fields. See **.')
		elif(not len(proteinName) <= Protein._meta.get_field('name').max_length):
			raise forms.ValidationError('Protein name is too long  - max ' + str(Protein._meta.get_field('name').max_length) + '.')
		else:
			return proteinName
	
	def clean_solution(self):
		solution = self.cleaned_data["solution"]
		if(not len(solution) <= Fragment._meta.get_field('solution').max_length):
			raise forms.ValidationError('Information about solution is too long - max ' + str(Fragment._meta.get_field('solution').max_length) + '.')
		else:
			return solution
	
	def clean_methodNames(self):
		cleaned_data = super(InsertForm, self).clean()
		methodExist = cleaned_data.get("methodExist")
		methodNames = cleaned_data.get("methodNames")
		if(methodExist != None and methodNames != ''):
			raise forms.ValidationError('Cannot fill both method fields. See **.')
		elif(methodNames != ''):
			metTable = methodNames.strip()
			metTable = metTable.split(';')
			for m in metTable:
				if (not len(m) <= Method._meta.get_field('name').max_length):
					raise forms.ValidationError('One of the methods names is too long - max ' + str(Method._meta.get_field('name').max_length) + '.')
					break;
		else:
			return methodNames
	
	def clean_diseaseNames(self):
		cleaned_data = super(InsertForm, self).clean()
		diseaseExist = cleaned_data.get("diseaseExist")
		diseaseNames = cleaned_data.get("diseaseNames")
		if(diseaseExist != None and diseaseNames != ''):
			raise forms.ValidationError('Cannot fill both disease fields. See **.')
		elif(diseaseNames != ''):
			disTable = diseaseNames.strip()
			disTable = disTable.split(';')
			for d in disTable:
				if (not len(d) <= Disease._meta.get_field('name').max_length):
					raise forms.ValidationError('One of the diseases names is too long - max ' + str(Disease._meta.get_field('name').max_length) + '.')
					break;
		else:
			return diseaseNames
	
	def clean_sourceCitUrl(self):
		cleaned_data = super(InsertForm, self).clean()
		sourceExist = cleaned_data.get("sourceExist")
		sourceName = cleaned_data.get("sourceName")
		sourceCitName = cleaned_data.get("sourceCitName")
		sourceCitPmid = cleaned_data.get("sourceCitPmid")
		sourceCitUrl = cleaned_data.get("sourceCitUrl")
		if(sourceExist != None and (sourceName != '' or sourceCitName != '' or sourceCitPmid != None or sourceCitUrl != '')):
			raise forms.ValidationError('Cannot fill both source fields. See **.')
		elif(sourceExist == None and (sourceName != '' or sourceCitName != '' or sourceCitPmid != None or sourceCitUrl != '') and not (sourceName != '' and sourceCitName != '' and (sourceCitPmid != None or sourceCitUrl != ''))):
			raise forms.ValidationError('If you give your new source, you must give its name, name of reference and one of PMID or link. See **.')
		elif(not len(sourceName) <= Source._meta.get_field('name').max_length):
			raise forms.ValidationError('Source name is too long - max ' + str(Source._meta.get_field('name').max_length) + '.')
		elif(not len(sourceCitName) <= Citation._meta.get_field('name').max_length):
			raise forms.ValidationError('Source\'s citation name is too long - max ' + str(Citation._meta.get_field('name').max_length) + '.')
		elif(not len(sourceCitUrl) <= Citation._meta.get_field('link').max_length):
			raise forms.ValidationError('Source\'s citation link is too long - max ' + str(Citation._meta.get_field('link').max_length) + '.')
		else:
			return sourceCitUrl
	
	def clean_citationUrl(self):
		cleaned_data = super(InsertForm, self).clean()
		citationExist = cleaned_data.get("citationExist")
		citationName = cleaned_data.get("citationName")
		citationPmid = cleaned_data.get("citationPmid")
		citationUrl = cleaned_data.get("citationUrl")
		if(citationExist != None and (citationName != '' or citationPmid != None or citationUrl != '')):
			raise forms.ValidationError('Cannot fill both reference fields. See **.')
		elif(citationExist == None and (citationName != '' or citationPmid != None or citationUrl != '') and not (citationName != '' and (citationPmid != None or citationUrl != ''))):
			raise forms.ValidationError('If you give your new reference, you must give its name and one of PMID or link. See **.')
		elif(not len(citationName) <= Citation._meta.get_field('name').max_length):
			raise forms.ValidationError('Citation name is too long - max ' + str(Citation._meta.get_field('name').max_length) + '.')
		elif(not len(citationUrl) <= Citation._meta.get_field('link').max_length):
			raise forms.ValidationError('Citation link is too long - max ' + str(Citation._meta.get_field('link').max_length) + '.')
		else:
			return citationUrl
	
	def clean_note(self):
		note = self.cleaned_data["note"]
		if(not len(note) <= Fragment._meta.get_field('note').max_length):
			raise forms.ValidationError('Remarks for reviewer are too long  - max ' + str(Fragment._meta.get_field('note').max_length) + '.')
		else:
			return note
	
class UploadFileForm(forms.Form):
	"""
	Form to upload file on 'Submit seq' page.
	"""
	file = forms.FileField()

class SaveSessionForm(forms.Form):
	"""
	Form to save temporary session on Database page.
	"""
	saveName = forms.CharField(max_length=Session._meta.get_field('name').max_length, widget=forms.TextInput(attrs={'placeholder': 'Input session name'}))

class ChangeEmailForm(forms.Form):
	"""
	Form to change email on 'About You' page.
	"""
	newEmail = forms.EmailField(max_length=User._meta.get_field('username').max_length, required=False, widget=forms.EmailInput(attrs={'placeholder': 'Input new email'}))

	def clean_newEmail(self):
		newEmail = self.cleaned_data["newEmail"]
		if(parseaddr(newEmail) == ('','')):
			raise forms.ValidationError('Wrong email format.')
		else:
			return newEmail

class ChangePassForm(forms.Form):
	"""
	Form to change password on 'About You' page.
	"""
	newPass1 = forms.CharField(max_length=User._meta.get_field('password').max_length, required=False, widget=forms.PasswordInput(attrs={'placeholder': 'Input new password'}))
	newPass2 = forms.CharField(max_length=User._meta.get_field('password').max_length, required=False, widget=forms.PasswordInput(attrs={'placeholder': 'Repeat new password'}))
	
	def clean_newPass2(self):
		newPass1 = self.cleaned_data["newPass1"]
		newPass2 = self.cleaned_data["newPass2"]
		if(not newPass1):
			raise forms.ValidationError('Password cannot be empty.')
		elif(newPass1 != newPass2):
			raise forms.ValidationError('Passwords are not equal.')
		else:
			return newPass2

class SortingAddedForm(forms.Form):
	"""
	Sorting form on 'Added fragments' page.
	"""
	proteins = forms.ModelChoiceField(queryset=Protein.objects.none())
	reviewed = forms.ChoiceField(choices=REVIEW)
	ifaggregate = forms.ChoiceField(choices=AGGREGATES)
	minlength = forms.IntegerField(widget=forms.NumberInput(attrs={'class':'special', 'style':'width: 100px;', 'placeholder': 'Min Length', 'min': '0'}))
	maxlength = forms.IntegerField(widget=forms.NumberInput(attrs={'class':'special', 'style':'width: 100px;', 'placeholder': 'Max Length', 'min': '0'}))
	bywhatsort = forms.ChoiceField(choices=ADDSORT)
	ascordesc = forms.ChoiceField(choices=ASCDESC)
	ifcontainProt = forms.CharField(widget=forms.TextInput(attrs={'class':'special', 'size': '105', 'placeholder': 'Input word and search for protein names which contain it. Browser is case-insensitive.'}))
	ifcontainSeq = forms.CharField(widget=forms.TextInput(attrs={'class':'special', 'size': '105', 'placeholder': 'Input amino acid sequence and search for protein sequences which contain it. Browser is case-insensitive.'}))
	
	def __init__(self, user, *args, **kwargs):
		super(SortingAddedForm, self).__init__(*args, **kwargs)
		if user:
			queryset = Protein.objects.filter(fragment__user=user).distinct().order_by('name')
		else:
			queryset = Protein.objects.all().distinct().order_by('name')
		self.fields['proteins'] = forms.ModelChoiceField(queryset=queryset, empty_label="Protein Name", widget=forms.Select(attrs={'style': 'width:200px'}))

class SortingSavedSessionsForm(forms.Form):
	"""
	Sorting form on 'Saved sessions' page.
	"""
	date1 = forms.DateField(widget=extras.SelectDateWidget())
	date2 = forms.DateField(widget=extras.SelectDateWidget())
	bywhatsort = forms.ChoiceField(choices=SESSIONSORT)
	ascordesc = forms.ChoiceField(choices=ASCDESC)
	ifcontainSession = forms.CharField(widget=forms.TextInput(attrs={'class':'special', 'size': '105', 'placeholder': 'Input word and search for session names which contain it. Browser is case-insensitive.'}))
	
	def __init__(self, *args, **kwargs):
		super(SortingSavedSessionsForm, self).__init__(*args, **kwargs)
		self.fields['date1'] = forms.DateField(widget=extras.SelectDateWidget(years=range(datetime.datetime.now().year,2012,-1)))
		self.fields['date2'] = forms.DateField(widget=extras.SelectDateWidget(years=range(datetime.datetime.now().year,2012,-1)))

class AnalysisForm(forms.Form):
	"""
	Form to choose method on Analysis page.
	"""
	file = forms.FileField()
	email = forms.EmailField(widget=forms.EmailInput(attrs={'placeholder': 'Input email'}))
	foldamyloid = forms.BooleanField(initial=False)
	foldamyloid_options = forms.MultipleChoiceField(choices=FOLDAMYLOID, widget=forms.CheckboxSelectMultiple)
	foldamyloid_frame_1 = forms.IntegerField(initial=5, widget=forms.NumberInput(attrs={'class':'special', 'style':'width: 150px;', 'min': '1'}))
	foldamyloid_frame_2 = forms.IntegerField(initial=5, widget=forms.NumberInput(attrs={'class':'special', 'style':'width: 150px;', 'min': '1'}))
	foldamyloid_frame_3 = forms.IntegerField(initial=5, widget=forms.NumberInput(attrs={'class':'special', 'style':'width: 150px;', 'min': '1'}))
	foldamyloid_frame_4 = forms.IntegerField(initial=5, widget=forms.NumberInput(attrs={'class':'special', 'style':'width: 150px;', 'min': '1'}))
	foldamyloid_frame_5 = forms.IntegerField(initial=5, widget=forms.NumberInput(attrs={'class':'special', 'style':'width: 150px;', 'min': '1'}))
	foldamyloid_threshold_1 = forms.FloatField(initial=21.4)
	foldamyloid_threshold_2 = forms.FloatField(initial=0.697)
	foldamyloid_threshold_3 = forms.FloatField(initial=0.09)
	foldamyloid_threshold_4 = forms.FloatField(initial=0.671)
	foldamyloid_threshold_5 = forms.FloatField(initial=0.062)
	aggrescan = forms.BooleanField()
	fish = forms.BooleanField()
	fish_options = forms.MultipleChoiceField(choices=PREDICTION, widget=forms.CheckboxSelectMultiple, label="Prediction")
	fish_threshold_1 = forms.FloatField(initial=0.19, label="Threshold", widget=forms.NumberInput(attrs={'class':'special', 'style':'width: 150px;', 'min': '0'}))
	fish_threshold_2 = forms.FloatField(initial=0.19, label="Threshold", widget=forms.NumberInput(attrs={'class':'special', 'style':'width: 150px;', 'min': '0'}))
	inclusion = forms.BooleanField()
	
	def __init__(self, *args, **kwargs):
		super(AnalysisForm, self).__init__(*args, **kwargs)
		self.fields['file'].widget.attrs['onchange'] = "checkEnableAnalyze();"
		self.fields['email'].widget.attrs['onkeyup'] = "checkEnableAnalyze();"
		self.fields['email'].widget.attrs['onblur'] = "checkEnableAnalyze();"
		self.fields['email'].widget.attrs['onfocus'] = "checkEnableAnalyze();"
		self.fields['foldamyloid'].widget.attrs['onclick'] = "enableTd('foldamyloidTD','id_foldamyloid');checkEnableAnalyze();"
		self.fields['foldamyloid_options'].widget.attrs['onclick'] = "checkEnableAnalyze();"
		self.fields['fish'].widget.attrs['onclick'] = "enableTd('fishTD','id_fish');checkEnableAnalyze();"
		self.fields['fish_options'].widget.attrs['onclick'] = "checkEnableAnalyze();"
		self.fields['aggrescan'].widget.attrs['onclick'] = "checkEnableAnalyze();"
		self.fields['inclusion'].widget.attrs['onclick'] = "checkEnableAnalyze();"

class CommentForm(forms.Form):
	"""
	Form to send comment on 'Fragment info' page.
	"""
	comment = forms.CharField(max_length=1000, required=False, label="Send comments", widget=forms.Textarea(attrs={'cols': 105, 'rows': 5, 'style':'resize:none;'}))
