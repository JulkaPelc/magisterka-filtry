from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles import finders
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.core.servers.basehttp import FileWrapper
from django.core.mail import send_mail
from django.db import transaction
from django.utils import timezone
from django.conf import settings
from django.db.models import Max
from comprec_project.amyload.models import *
from comprec_project.amyload.forms import *
from threading import Thread
from urlparse import urlparse
from lxml import etree
from Bio import SeqIO
import hashlib, datetime, random
import sys, traceback
import os, subprocess
import tempfile
import mimetypes
import uuid
import math
import re

def index(request):
	"""
	Redirects to the home page.
	
	@return: page render
	"""
	return render(request, 'amyload/home.html', {'title': "AmyLoad - Home"})

def setDatabasePageParams(request, fragment_list, fromWhich, toWhich, title, form, saveForm, selected, fromWhichMoved, toWhichMoved, selectedMoved, info="", iferror=""):
	"""
	Pass parameters to the Database page.
	
	@param fragment_list: list of dictionaries representing objects of Fragment model
	@type fragment_list: list of dictionaries
	@param fromWhich: number of first fragment to display in Filtering table
	@type fromWhich: integer
	@param toWhich: number of last fragment to display in Filtering table
	@type toWhich: integer
	@param title: title of the web page
	@type title: string
	@param form: filtering form on the Database page
	@type form: Django form object
	@param selected: number of fragments displayed in Filtering table
	@type selected: integer
	@param saveForm: saving session form on the Database page
	@type saveForm: Django form object 
	@param fromWhichMoved: number of first fragment to display in Session table
	@type fromWhichMoved: integer
	@param toWhichMoved: number of last fragment to display in Session table
	@type toWhichMoved: integer
	@param selectedMoved: number of fragments displayed in Session table
	@type selectedMoved: integer
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	
	#get specific subset of fragments to present in Filtering table:
	results = getDatabaseData(fragment_list, fromWhich, toWhich)
	#get specific subset of fragments to present in Session table:
	resultsMoved = getSessionData(request, fromWhichMoved, toWhichMoved)
	return render(request, 'amyload/database.html', {'fragments': results['data_list'], 'sessionList': resultsMoved['session_list'], 
	'title': title, 'form': form, 'saveForm': saveForm, 'howMany': results['howMany'], 'howManyMoved': resultsMoved['howManyMoved'],
	'selected': selected, 'selectedMoved': selectedMoved, 'fromWhich': fromWhich, 'fromWhichMoved': fromWhichMoved, 'toWhich': toWhich,
	'toWhichMoved': toWhichMoved, 'info': info, 'iferror': iferror})

def databaseFilter(request, info="", iferror=""):
	"""
	Gather data for Filtering table after clicking the 'Filter' button.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in Filtering table:
		fragment_list = afterFilter(data, '')
		#check if the fragment list is empty:
		if (len(fragment_list) == 0):
			fromWhich = 0
		else:
			fromWhich = 1
		#if user is not logged then there is no Session table on the page:
		if (request.user.is_authenticated()):
			return setDatabasePageParams(request, fragment_list, fromWhich, int(data['selected']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), int(data['h_fromWhichMoved']), int(data['h_toWhichMoved']), int(data['selectedMoved']), info, iferror)
		else:
			return setDatabasePageParams(request, fragment_list, fromWhich, int(data['selected']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), '', '', '', info, iferror)
	else:
		return database(request)

def databaseHowMany(request, info="", iferror=""):
	"""
	Gather data for Filtering table after changing number of fragments displayed in Filtering table on the Database page.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in Filtering table:
		fragment_list = afterHowMany(data, '')
		#check if the fragment list is empty:
		if (len(fragment_list) == 0):
			fromWhich = 0
		else:
			fromWhich = 1
		#if user is not logged then there is no Session table on the page:
		if (request.user.is_authenticated()):
			return setDatabasePageParams(request, fragment_list, fromWhich, int(data['selected']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), int(data['h_fromWhichMoved']), int(data['h_toWhichMoved']), int(data['selectedMoved']), info, iferror)
		else:
			return setDatabasePageParams(request, fragment_list, fromWhich, int(data['selected']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), '', '', '', info, iferror)
	else:
		return database(request)

def databaseNext(request, info="", iferror=""):
	"""
	Gather data for Filtering table after clicking the 'Next' link above Filtering table on the Database page.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in Filtering table:
		fragment_list = afterHowMany(data, '')
		#if user is not logged then there is no Session table on the page:
		if (request.user.is_authenticated()):
			return setDatabasePageParams(request, fragment_list, int(data['h_fromWhich']) + int(data['selected']), int(data['h_toWhich']) + int(data['selected']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), int(data['h_fromWhichMoved']), int(data['h_toWhichMoved']), int(data['selectedMoved']), info, iferror)
		else:
			return setDatabasePageParams(request, fragment_list, int(data['h_fromWhich']) + int(data['selected']), int(data['h_toWhich']) + int(data['selected']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), '', '', '', info, iferror)
	else:
		return database(request)

def databasePrevious(request, info="", iferror=""):
	"""
	Gather data for Filtering table after clicking the 'Previous' link above Filtering table on the Database page.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in Filtering table:
		fragment_list = afterHowMany(data, '')
		#if user is not logged then there is no Session table on the page:
		if (request.user.is_authenticated()):
			return setDatabasePageParams(request, fragment_list, int(data['h_fromWhich']) - int(data['selected']), int(data['h_toWhich']) - int(data['selected']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), int(data['h_fromWhichMoved']), int(data['h_toWhichMoved']), int(data['selectedMoved']), info, iferror)
		else:
			return setDatabasePageParams(request, fragment_list, int(data['h_fromWhich']) - int(data['selected']), int(data['h_toWhich']) - int(data['selected']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), '', '', '', info, iferror)
	else:
		return database(request)

@login_required
def databaseMove(request, info="", iferror=""):
	"""
	Move checked data from Filtering table into Session table after clicking the 'Move' button.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in Filtering table:
		fragment_list = afterHowMany(data, '')
		#get all checked fragments:
		chosenFrags = data.getlist('checkbox')
		chosen_list = Fragment.objects.filter(id__in=chosenFrags)
		#get latest session of the user:
		latestSession = Session.objects.filter(user_id=request.user)
		if(latestSession):
			latestSession = latestSession.latest('id')
		else:
			print "Move session error occurred"
			info = "An error occurred. Chceck if everything is ok with your browsing session."
			iferror = "1"
			latestSession = Session.objects.create(user=request.user, date=datetime.date.today().isoformat(), saved=False)
		#add checked fragments into user's latest session:
		latestSession.fragments.add(*chosen_list)
		return setDatabasePageParams(request, fragment_list, int(data['h_fromWhich']), int(data['h_toWhich']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), 1, int(data['selectedMoved']), int(data['selectedMoved']), info, iferror)
	else:
		return database(request)

@login_required
def databaseRemove(request, info="", iferror=""):
	"""
	Remove checked data from Session table after clicking the 'Remove' button.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in Filtering table:
		fragment_list = afterHowMany(data, '')
		#get all checked fragments:
		chosenFrags = data.getlist('checkbox2')
		chosen_list = Fragment.objects.filter(id__in=chosenFrags)
		latestSession = Session.objects.filter(user_id=request.user)
		if(latestSession):
			latestSession = latestSession.latest('id')
		else:
			print "Remove error occurred"
			info = "An error occurred. Chceck if everything is ok with your browsing session."
			iferror = "1"
			latestSession = Session.objects.create(user=request.user, date=datetime.date.today().isoformat(), saved=False)
		#remove checked fragments from user's latest session:
		latestSession.fragments.remove(*chosen_list)
		return setDatabasePageParams(request, fragment_list, int(data['h_fromWhich']), int(data['h_toWhich']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), 1, int(data['selectedMoved']), int(data['selectedMoved']), info, iferror)
	else:
		return database(request)

@login_required
def databaseAllFasta(request, info="", iferror=""):
	"""
	Gather all data from Session table to create FASTA file after clicking the 'Download all in FASTA file' button.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render or HttpResponse
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get all fragments in user's latest session:
		latestSession = Session.objects.filter(user_id=request.user)
		if(latestSession):
			latestSession = latestSession.latest('id')
		else:
			print "FASTA session error occurred"
			info = "An error occurred. Chceck if everything is ok with your browsing session."
			iferror = "1"
			latestSession = Session.objects.create(user=request.user, date=datetime.date.today().isoformat(), saved=False)
		session_list = latestSession.fragments.all()
		#if there are any session then create FASTA file:
		if(session_list):
			return createFasta(session_list, str(latestSession.id), request.user.username)
		else:
			fragment_list = afterHowMany(data, '')
			return setDatabasePageParams(request, fragment_list, int(data['h_fromWhich']), int(data['h_toWhich']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), int(data['h_fromWhichMoved']), int(data['h_toWhichMoved']), int(data['selectedMoved']), info, iferror)
	else:
		return database(request)

def createFasta(session_list, session_id, username):
	"""
	Create FASTA file.
	
	@param session_list: list of dictionaries representing objects of Fragment model
	@type session_list: list of dictionaries
	@param session_id: session number
	@type session_id: integer
	@param username: name of logged user
	@type username: string
	@return: HttpResponse
	"""
	response = HttpResponse(content_type='text/plain')
	response['Content-Disposition'] = 'attachment; filename="%s"' % ("AmyLoad_session_" + session_id + "_" + username + ".fasta")
	for s in session_list:
		#Write FASTA header:
		response.write(">AMY" + str(s.amyindex) + "|" + s.name.replace(" ", "_") + "|" + s.protein.name.replace(" ", "_") + "\r\n")
		#Write FASTA sequence:
		response.write(s.sequence + "\r\n")
	return response

@login_required
def databaseHowManyMoved(request, info="", iferror=""):
	"""
	Gather data for Session table after changing number of fragments displayed in Session table on the Database page.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in Filtering table:
		fragment_list = afterHowMany(data, '')
		return setDatabasePageParams(request, fragment_list, int(data['h_fromWhich']), int(data['h_toWhich']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), 1, int(data['selectedMoved']), int(data['selectedMoved']), info, iferror)
	else:
		return database(request)

@login_required
def databaseNextMoved(request, info="", iferror=""):
	"""
	Gather data for Session table after clicking the 'Next' link above Session table on the Database page.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in Filtering table:
		fragment_list = afterHowMany(data, '')
		return setDatabasePageParams(request, fragment_list, int(data['h_fromWhich']), int(data['h_toWhich']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), int(data['h_fromWhichMoved']) + int(data['selectedMoved']), int(data['h_toWhichMoved']) + int(data['selectedMoved']), int(data['selectedMoved']), info, iferror)
	else:
		return database(request)

@login_required
def databasePreviousMoved(request, info="", iferror=""):
	"""
	Gather data for Session table after clicking the 'Previous' link above Session table on the Database page.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in Filtering table:
		fragment_list = afterHowMany(data, '')
		return setDatabasePageParams(request, fragment_list, int(data['h_fromWhich']), int(data['h_toWhich']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), int(data['h_fromWhichMoved']) - int(data['selectedMoved']), int(data['h_toWhichMoved']) - int(data['selectedMoved']), int(data['selectedMoved']), info, iferror)
	else:
		return database(request)

def database(request, info="", iferror=""):
	"""
	Open Database page.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	#get fragments to present in Filtering table:
	fragment_list = afterNonPost()
	return setDatabasePageParams(request, fragment_list, 1, 10, "AmyLoad - Database", SortingForm(), SaveSessionForm(), 10, 1, 10, 10, info, iferror)

def getSessionData(request, fromWhichMoved, toWhichMoved):
	"""
	Gather specific subset of fragments to present in Session table.
	
	@param fromWhichMoved: number of first fragment to display in Session table
	@type fromWhichMoved: integer
	@param toWhichMoved: number of last fragment to display in Session table
	@type toWhichMoved: integer
	@return: dictionary with: 'session_list' - list of dictionaries representing objects of Fragment model; 'howManyMoved' - number of fragments to show at a time in Session table
	"""
	if request.user.is_authenticated():
		#get fragments from the latest session of logged user:
		latestSession = Session.objects.filter(user_id=request.user)
		if(latestSession):
			latestSession = latestSession.latest('id')
			session_list = latestSession.fragments.all().order_by('protein__name')
			howManyMoved = len(session_list)
			#get subset of fragments:
			if(fromWhichMoved == 0):
				session_list = session_list[1:toWhichMoved]
			else:
				session_list = session_list[fromWhichMoved-1:toWhichMoved]
		else:
			print "Get session data error occurred"
			latestSession = Session.objects.create(user=request.user, date=datetime.date.today().isoformat(), saved=False)
			session_list = ''
			howManyMoved = ''
	else:
		session_list = ''
		howManyMoved = ''
	resultsMoved = {'session_list': session_list, 'howManyMoved': howManyMoved}
	return resultsMoved

def getDatabaseData(data_list, fromWhich, toWhich):
	"""
	Gather specific subset of fragments to present in Filtering table.
	
	@param data_list: list of dictionaries representing objects of chosen model
	@type data_list: list of dictionaries
	@param fromWhich: number of first fragment to display in Session table
	@type fromWhich: integer
	@param toWhich: number of last fragment to display in Session table
	@type toWhich: integer
	@return: dictionary with: 'data_list' - list of dictionaries representing objects of Fragment model; 'howMany' - number of fragments to show at a time in Filter table
	"""
	howManyFrags = len(data_list)
	#get subset of fragments:
	if(fromWhich == 0):
		data_list = data_list[1:toWhich]
	else:
		data_list = data_list[fromWhich-1:toWhich]
	results = {'data_list': data_list, 'howMany': howManyFrags}
	return results

def afterHowMany(data, user):
	"""
	Gather fragments to present in table.
	
	@param data: data gathered after POST from page form
	@type data: dictionary
	@param user: logged user
	@type user: Django user object
	@return: list of dictionaries representing objects of Fragment model
	"""
	#if h_proteins on page was set then server is asking for fragments after clicking 'Filter' button:
	if(data['h_proteins'] == 'None'):
		#if user was set then server is asking for fragments on 'Added fragments' page after nonPOST web enter:
		if(user):
			fragment_list = addedAfterNonPost(user)
		#if user wasn't set then server is asking for fragments on Database page after nonPOST web enter:
		else:
			fragment_list = afterNonPost()
		return fragment_list
	else:
		#filtrate fragments:
		fragment_list = mainFilter(data['h_proteins'], data['h_ifaggregate'], data['h_minlength'], data['h_maxlength'], data['h_bywhatsort'], data['h_ascordesc'], data['h_ifcontainProt'], data['h_ifcontainSeq'], data['reviewed'], user)
		return fragment_list

def afterNonPost():
	"""
	Gather fragments to present in tables on Database page after nonPOST web enter.
	
	@return: list of dictionaries representing objects of Fragment model
	"""
	fragment_list = Fragment.objects.filter(reviewed=1).order_by('protein__name')
	return fragment_list

def addedAfterNonPost(user):
	"""
	Gather fragments to present in table on 'Added fragments' page after nonPOST web enter.
	
	@param user: logged user
	@type user: Django user object
	@return: list of dictionaries representing objects of Fragment model
	"""
	fragment_list = Fragment.objects.filter(user__id=user.id).order_by('protein__name')
	return fragment_list

def afterFilter(data, user):
	"""
	Gather fragments to present in table on 'Added fragments'or Database page after clicking 'Filter' button.
	
	@param data: data gathered after POST from page form
	@type data: dictionary
	@param user: logged user
	@type user: Django user object
	@return: list of dictionaries representing objects of Fragment model
	"""
	fragment_list = mainFilter(data['proteins'], data['ifaggregate'], data['minlength'], data['maxlength'], data['bywhatsort'], data['ascordesc'], data['ifcontainProt'], data['ifcontainSeq'], data['reviewed'], user)
	return fragment_list

def mainFilter(sProtein, sIfAggregate, sMinLength, sMaxLength, sByWhatSort, sAscOrDesc, sIfContainProt, sIfContainSeq, sReviewed, user):
	"""
	Perform main filtration of fragments according to parameters submitted in filtration form on the web page.
	
	@param sProtein: protein name
	@type sProtein: string
	@param sIfAggregate: information about fibril formation 
	@type sIfAggregate: boolean (1 = 'Yes'; 0 = 'No')
	@param sMinLength: minimal sequence length
	@type sMinLength: integer
	@param sMaxLength: maximal sequence length
	@type sMaxLength: integer
	@param sByWhatSort: parameter to sort by
	@type sByWhatSort: integer (0 - protein name, 1 - name, 2 - sequence length, 3 - aggregation, 4 - revision, 5 - addition date)
	@param sAscOrDesc: order of sorting data
	@type sAscOrDesc: boolean (1 - descendingly, 0 - ascendingly)
	@param sIfContainProt: protein name substring
	@type sIfContainProt: string
	@param sIfContainSeq: sequence substring
	@type sIfContainSeq: string
	@param sReviewed: revision state
	@type sReviewed: boolean (1 - reviewed, 0 - unreviewed)
	@param user: logged user
	@type user: Django user object
	@return: list of dictionaries representing objects of Fragment model
	"""
	#initial parameters:
	params = {'sort': '.order_by(\'protein__name\')', 'sfiltr': '.filter(', 'efiltr': ')', 'prot': '', 'aggreg': '', 'reviewed': '', 'length': '', 'ascdesc': '', 'ifcontainProt': '', 'ifcontainSeq': '', 'user': ''}
	#get order of sorting data:
	if (sAscOrDesc == '1'):
		params['sort'] = '.order_by(\'-protein__name\')'
		params['ascdesc'] = '-'
	#if minimal sequence length contains letters:
	if(not sMinLength.isdigit()):
		sMinLength = ''
	#if maximal sequence length contains letters:
	if(not sMaxLength.isdigit()):
		sMaxLength = ''
	#get parameter to sort by:
	if (sByWhatSort == '0'):
		params['sort'] = '.order_by(\'' + params['ascdesc'] + 'protein__name\')'
	elif (sByWhatSort == '1'):
		params['sort'] = '.order_by(\'' + params['ascdesc'] + 'name\')'
	elif (sByWhatSort == '2'):
		params['sort'] = '.extra(select={\'length\':\'Length(sequence)\'}).order_by(\'' + params['ascdesc'] + 'length\')'
	elif (sByWhatSort == '3'):
		params['sort'] = '.order_by(\'' + params['ascdesc'] + 'ifaggregate\')'
	elif (sByWhatSort == '4'):
		params['sort'] = '.order_by(\'' + params['ascdesc'] + 'reviewed\')'
	elif (sByWhatSort == '5'):
		params['sort'] = '.order_by(\'' + params['ascdesc'] + 'date\')'
	#get protein name:
	if (sProtein != ''):
		params['prot'] = 'protein=sProtein,'
	#get information about fibril formation:
	if (sIfAggregate != '-1'):
		params['aggreg'] = 'ifaggregate=sIfAggregate,'
	#eliminate situation when (min length) > (max length):
	if (sMaxLength != '' and sMinLength != '' and int(sMinLength) > int(sMaxLength)):
		sMaxLength = '0'
		sMinLength = '0'
	#get protein name substring (Django bug - make new table in MySQL with lowercased protein names):
	if (sIfContainProt != ''):
		sIfContainProt = eliminRegexDestroyers(sIfContainProt)
		params['ifcontainProt'] = 'protein__name__icontains=\'' + sIfContainProt + '\','
	#get sequence substring:
	if (sIfContainSeq != ''):
		sIfContainSeq = eliminRegexDestroyers(sIfContainSeq)
		params['ifcontainSeq'] = 'sequence__icontains=\'' + sIfContainSeq + '\','
	#get user:
	if(user):
		params['user'] = 'user__id=' + str(user.id) + ','
	#get revision state:
	if(sReviewed == '1' or (sReviewed == '0' and user)):
			params['reviewed'] = 'reviewed=' + sReviewed + ','
	#apply sequence length regulations:
	if(sMaxLength != '' and sMinLength != ''):
		params['length'] = ".extra(where=[\"CHAR_LENGTH(sequence) >= " + sMinLength + " AND CHAR_LENGTH(sequence) <= " + sMaxLength + "\"])"
	elif(sMaxLength == '' and sMinLength != ''):
		params['length'] = ".extra(where=[\"CHAR_LENGTH(sequence) >= " + sMinLength + "\"])"
	elif(sMaxLength != '' and sMinLength == ''):
		params['length'] = ".extra(where=[\"CHAR_LENGTH(sequence) <= " + sMaxLength + "\"])"
	#get filtered fragments:
	final = 'fragment_list = Fragment.objects' + params['sfiltr'] + params['prot'] + params['aggreg'] + params['reviewed'] + params['ifcontainProt'] + params['ifcontainSeq'] + params['user'] + params['efiltr'] + params['length'] + params['sort']
	exec(final)
	return fragment_list

def eliminRegexDestroyers(word):
	"""
	Decodes the input string into utf-8, stripped, and string_escape.
	
	@param word: word to decode
	@type word: string
	"""
	return str(u''.join(word).encode('utf-8').strip()).encode('string_escape')

def fragmentInfo(request, fragId="-1", info="", iferror=""):
	"""
	Shows information about chosen fragment.
	
	@param fragId: fragment database id
	@type fragId: integer
	@param info: information text to display on fragmentInfo page
	@type info: string
	@param iferror: 1 if info is an error
	@type iferror: string
	@return: page render
	"""
	fragment_list = Fragment.objects.filter(id=fragId)
	#if such id exists:
	if(fragment_list):
		fragment_list = fragment_list[0]
		amyId = fragment_list.amyindex
		name = fragment_list.name
		protein = fragment_list.protein.name
		ifaggregate = fragment_list.ifaggregate
		sequence = fragment_list.sequence
		solution = fragment_list.solution
		methods = fragment_list.methods.all()
		sources = fragment_list.sources.all()
		citations = fragment_list.citations.all()
		diseases = fragment_list.diseases.all()
		comment_form = CommentForm()
		
		data = {'amyId': amyId, 'name': name, 'protein': protein, 'ifaggregate': ifaggregate, 'sequence': sequence, 'solution': solution, 
			'methods': methods, 'sources': sources, 'citations': citations, 'diseases': diseases, 'title': "AmyLoad - Fragment Info", 'comment_form': comment_form, 
			"fragId": fragId, "info": info, "iferror": iferror}
		return render(request, 'amyload/fragmentInfo.html', data)
	else:
		return render(request, 'amyload/home.html', {'title': "AmyLoad - Home"})

def register(request):
	"""
	Gather data from registration form. Creates new user and send confirmation email.
	
	@return: page render
	"""
	username = ''
	if request.method == 'POST':
		try:
			with transaction.atomic():
				form = RegistrationForm(request.POST)
				#check if all information in fregistration form are valid:
				if form.is_valid():
					#get data from the form:
					form.save()
					username = form.cleaned_data['username']
					email = form.cleaned_data['email']
					affiliation = form.cleaned_data['affiliation']
					comment = form.cleaned_data['comment']
					#create activation link:
					salt = hashlib.sha1(str(random.random())).hexdigest()[:5]            
					activation_key = hashlib.sha1(salt+email).hexdigest()            
					key_expires = datetime.datetime.today() + datetime.timedelta(2)
					#create new user:
					user=User.objects.get(username=username)
					new_profile = UserProfile(user=user, activation_key=activation_key, key_expires=key_expires, affiliation=affiliation, comment=comment)
					new_profile.save()
					#get domain name:
					domain = request.META['HTTP_HOST']
					#send email:
					email_subject = 'Account confirmation'
					email_body = "Welcome %s Thank you for signing up. To activate your account, click this link within next 48hours (or copy it into your browser): \n%s/amyload/confirm/%s" % (username, domain, activation_key)
					send_mail(email_subject, email_body, 'comprec.pwr@gmail.com', [email], fail_silently=False)
					return render(request, 'registration/register_success.html', {'title': 'AmyLoad - Registration success'})
		except Exception as ex:
			traceback.print_exc()
			error = 'There was a registration error.'
			admin_emails = [v for k,v in settings.ADMINS]
			return render(request, 'error.html', {'title': 'AmyLoad - Registration error', 'error':  error, 'emails': admin_emails})
	else:
		form = RegistrationForm()
	return render(request, 'registration/registration.html', {'form': form, 'title': 'AmyLoad - Registration'})

def register_confirm(request, activation_key):
	"""
	Validate activation key clicked by user in confirmation email.
	
	@param activation_key: received activation key sent to the user by email during the registration process
	@type activation_key: string
	@return: page render
	"""
	#if user is already activated then just go home:
	if request.user.is_authenticated():
		return render(request, 'amyload/home.html', {'title': "AmyLoad - Home"})
	#compare activation key with the one created during the registration and show 404 error page if it doesn't match:
	user_profile = get_object_or_404(UserProfile, activation_key=activation_key)
	#check if the activation_key did not expired (it should be clicked within 2 days):
	if user_profile.key_expires < timezone.now().date():
		#if it expired then delete the previously created user and ask to register again:
		user = user_profile.user
		user_profile.delete()
		user.delete()
		return render(request, 'registration/confirm_expired.html', {'title': 'AmyLoad - Confirmation expired'})
	#if it is not expired then activate user and show confirmation:
	user = user_profile.user
	user.is_active = True
	user.save()
	return render(request, 'registration/confirm.html', {'title': 'AmyLoad - Registration confirmation'})

def user_login(request):
	"""
	Validates login data.
	
	@return: page render
	"""
	error = ''
	if request.method == 'POST':
		#get login and password:
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username=username, password=password)
		if user:
			#check if user is activated:
			if user.is_active:
				login(request, user)
				#create new temporary session:
				Session.objects.create(user=request.user, date=datetime.date.today().isoformat(), saved=False)
				return HttpResponseRedirect('/amyload/')
			else:
				error = 'Your account is disabled'
				return render(request, 'amyload/home.html', {'title': 'AmyLoad - Home', 'error': error})
		else:
			print "Invalid login details: {0}, {1}".format(username, password)
			error = 'Invalid login or password'
			return render(request, 'amyload/home.html', {'title': 'AmyLoad - Home', 'error': error})
	else:
		return render(request, 'amyload/home.html', {'title': "AmyLoad - Home", 'error': error})

@login_required
def user_logout(request):
	"""
	Log user out.
	
	@return: page render
	"""
	logout(request)
	return render(request, 'amyload/home.html', {'title': "AmyLoad - Home"})

@login_required
def insertForm(request):
	"""
	Gather data from sequence insertion form.
	
	@return: page render
	"""
	ifExist = ""
	info = ""
	xmlError = ""
	xmlInfo = ""
	upload_form = UploadFileForm()
	if request.method == 'POST':
		try:
			with transaction.atomic():
				#get data from insertion form:
				insert_form = InsertForm(data=request.POST)
				#check if insertion form is valid:
				if(insert_form.is_valid()):
					#initialize variables:
					name = 'Untitled'
					sequence = insert_form.data['sequence'].strip().upper()
					aggregation = insert_form.data['ifaggregate'].strip()
					if(aggregation == "0"):
						aggregation = "No"
					elif(aggregation == "1"):
						aggregation = "Yes"
					solution = 'No data'
					methods = ''
					diseases = ''
					source = 'NULL'
					method = 'NULL'
					disease = 'NULL'
					citation = 'NULL'
					note = ''
					#get fragment protein name:
					if (insert_form.data['proteinExist'] != ''):
						#if protein exists in a database (user found it on the list):
						protein_id = insert_form.data['proteinExist'].strip()
						protein = Protein.objects.filter(id=protein_id)
						protein = protein[0]
					else:
						#if user gave new protein:
						protein = checkProtein(request.user, insert_form.data['proteinName'].strip())
					#check if there is already the same sequence for the same protein in a database:
					ifExist = Fragment.objects.filter(sequence=sequence, protein=protein)
					if(not ifExist):
						#get fragment name:
						if (insert_form.data['name'] != ''):
							name = insert_form.data['name'].strip()
						#get fragment solution info:
						if (insert_form.data['solution'] != ''):
							solution = insert_form.data['solution'].strip()
						#get fragment methods info:
						if (insert_form.data['methodExist'] != ''):
							#if method exists in a database (user found it on the list):
							method_id = insert_form.data['methodExist'].strip()
							method = Method.objects.filter(id=method_id)
							method = method[0]
						else:
							#if user gave new methods names:
							if(insert_form.data['methodNames'] != ''):
								metTable = insert_form.data['methodNames'].strip()
								metTable = metTable.split(';')
								methods = checkMethods(request.user, metTable)
						#get fragment diseases info:
						if (insert_form.data['diseaseExist'] != ''):
							#if disease exists in a database (user found it on the list):
							disease_id = insert_form.data['diseaseExist'].strip()
							disease = Disease.objects.filter(id=disease_id)
							disease = disease[0]
						else:
							#if user gave new diseases names:
							if(insert_form.data['diseaseNames'] != ''):
								disTable = insert_form.data['diseaseNames'].strip()
								disTable = disTable.split(';')
								diseases = checkDiseases(request.user, disTable)
						#parse given source citation link:
						tmpUrl = urlparse(insert_form.data['sourceCitUrl'])
						#check the format of the link:
						if (tmpUrl.scheme):
							citationSourceLink = tmpUrl.netloc + tmpUrl.path
						else:
							citationSourceLink = insert_form.data['sourceCitUrl']
						#get fragment source data:
						if (insert_form.data['sourceExist'] != ''):
							#if source exists in a database (user found it on the list):
							source_id = insert_form.data['sourceExist'].strip()
							source = Source.objects.filter(id=source_id)
							source = source[0]
						else:
							#if user gave new source:
							references = []
							refTmp = {'name': insert_form.data['sourceCitName'].strip(), 'pmid': insert_form.data['sourceCitPmid'].strip(), 'url': citationSourceLink.strip()}
							references.append(refTmp)
							source = checkSource(request.user, insert_form.data['sourceName'].strip(), references)
						#parse given citation link:
						tmpUrl = urlparse(insert_form.data['citationUrl'])
						#check the format of the link:
						if (tmpUrl.scheme):
							citationLink = tmpUrl.netloc + tmpUrl.path
						else:
							citationLink = insert_form.data['citationUrl']
						#get fragment citation data:
						if (insert_form.data['citationExist'] != ''):
							#if citation exists in a database (user found it on the list):
							citation_id = insert_form.data['citationExist'].strip()
							citation = Citation.objects.filter(id=citation_id)
							citation = citation[0]
						else:
							#if user gave new citation:
							citation = checkCitation(request.user, insert_form.data['citationName'].strip(), insert_form.data['citationPmid'].strip(), citationLink.strip())
						#get fragment note:
						if (insert_form.data['note'] != ''):
							note = insert_form.data['note'].strip()
						#put sources and citations into table:
						sources = []
						if (source != 'NULL'):
							sources.append(source)
						citations = []
						if (citation != 'NULL'):
							citations.append(citation)
						#create fragment on the basis of gathered data:
						createFragment(request.user, name, sequence, aggregation, protein, solution, method, methods, disease, diseases, sources, citations, note)
						#empty the insertion form:
						insert_form = InsertForm()
						info = "Sequence added into the database. Status = unrevised. After the revision it will be visible in the database."
		except Exception as e:
			xmlError = "An error occurred. Please try again or contact wit our admin."
			info = ""
			traceback.print_exc()
	else:
		insert_form = InsertForm()
	return render(request, 'amyload/insertSeq.html', {'upload_form': upload_form, 'insert_form': insert_form, 'title': "AmyLoad - Insert Sequence", 'info': info, 'xmlError': xmlError, 'xmlInfo': xmlInfo, 'ifExist': ifExist})

@login_required
def databaseAllXml(request, info="", iferror=""):
	"""
	Gather data about all fragments in latest session to create their XML file.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render or HttpResponse
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get the latest session:
		latestSession = Session.objects.filter(user_id=request.user)
		if(latestSession):
			latestSession = latestSession.latest('id')
		else:
			print "XML session error occurred"
			info = "An error occurred. Chceck if everything is ok with your browsing session."
			iferror = "1"
			latestSession = Session.objects.create(user=request.user, date=datetime.date.today().isoformat(), saved=False)
		#get fragments form the latest session:
		session_list = latestSession.fragments.all()
		if(session_list):
			#if there are any fragments then create XML:
			return createXml(session_list, str(latestSession.id), request.user.username)
		else:
			#if there are aren't ant fragments then just reopen the Database page:
			fragment_list = afterHowMany(data, '')
			return setDatabasePageParams(request, fragment_list, int(data['h_fromWhich']), int(data['h_toWhich']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), int(data['h_fromWhichMoved']), int(data['h_toWhichMoved']), int(data['selectedMoved']), info, iferror)
	else:
		return database(request)

def createXml(session_list, session_id, username):
	"""
	Create XML file for fragments in temporary session.
	
	@param session_list: list of dictionaries representing objects of Fragment model
	@type session_list: list of dictionaries
	@param session_id: id of the latest session
	@type session_id: integer
	@param username: name of logged user
	@type username: string
	@return: HttpResponse
	"""
	selected = etree.Element("selected")
	for s in session_list:
		#create <fragment>:
		fragment = etree.SubElement(selected, "fragment")
		#create <id>:
		amyId = etree.SubElement(fragment, "id")
		amyId.text = "AMY" + str(s.amyindex)
		#create <name>:
		name = etree.SubElement(fragment, "name")
		name.text = s.name
		#create <sequence>:
		seq = etree.SubElement(fragment, "sequence")
		seq.text = s.sequence
		#create <fibrilation>:
		ifaggreg = etree.SubElement(fragment, "fibrilation")
		if (s.ifaggregate == True):
			ifaggreg.text = "Yes"
		else:
			ifaggreg.text = "No"
		#create <protein> and its <name>:
		protein = etree.SubElement(fragment, "protein")
		proteinName = etree.SubElement(protein, "name")
		proteinName.text = s.protein.name
		#create <solution>:
		solution = etree.SubElement(fragment, "solution")
		solution.text = s.solution
		#create all <method>:
		for m in s.methods.all():
			if (m.reviewed == 1):
				method = etree.SubElement(fragment, "method")
				methodName = etree.SubElement(method, "name")
				methodName.text = m.name
		#create all <disease>:
		for d in s.diseases.all():
			if (d.reviewed == 1):
				disease = etree.SubElement(fragment, "disease")
				diseaseName = etree.SubElement(disease, "name")
				diseaseName.text = d.name
		#create all <source>:
		for p in s.sources.all():
			if (p.reviewed == 1):
				source = etree.SubElement(fragment, "source")
				sourceName = etree.SubElement(source, "name")
				sourceName.text = p.name
				#create all <reference> in <source>:
				for c in p.citations.all():
					if (c.reviewed == 1):
						citation = etree.SubElement(source, "reference")
						citationName = etree.SubElement(citation, "name")
						citationName.text = c.name
						pmid = etree.SubElement(citation, "pmid")
						pmid.text = str(c.pmid)
						link = etree.SubElement(citation, "url")
						link.text = c.link
		#create all <reference>:
		for c in s.citations.all():
			if (c.reviewed == 1):
				citation = etree.SubElement(fragment, "reference")
				citationName = etree.SubElement(citation, "name")
				citationName.text = c.name
				pmid = etree.SubElement(citation, "pmid")
				pmid.text = str(c.pmid)
				link = etree.SubElement(citation, "url")
				link.text = c.link
	#create file to download:
	response = HttpResponse(content_type='text/plain')
	response['Content-Disposition'] = 'attachment; filename="%s"' % ("AmyLoad_session_" + session_id + "_" + username + ".xml")
	textContent = etree.tostring(selected, encoding='UTF-8', pretty_print=True)
	textContent = textContent.replace("\n","\r\n");
	response.write(textContent)
	return response

@login_required
def databaseAllSsv(request, info="", iferror=""):
	"""
	Gather data about all fragments in latest session to create their SSV file.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render or HttpResponse
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get the latest session:
		latestSession = Session.objects.filter(user_id=request.user)
		if(latestSession):
			latestSession = latestSession.latest('id')
		else:
			print "SSV session error occurred"
			info = "An error occurred. Chceck if everything is ok with your browsing session."
			iferror = "1"
			latestSession = Session.objects.create(user=request.user, date=datetime.date.today().isoformat(), saved=False)
		#get fragments form the latest session:
		session_list = latestSession.fragments.all()
		if(session_list):
			return createSsv(session_list, str(latestSession.id), request.user.username)
		else:
			#if there are aren't ant fragments then just reopen the Database page:
			fragment_list = afterHowMany(data, '')
			return setDatabasePageParams(request, fragment_list, int(data['h_fromWhich']), int(data['h_toWhich']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), int(data['h_fromWhichMoved']), int(data['h_toWhichMoved']), int(data['selectedMoved']), info, iferror)
	else:
		return database(request)

@login_required
def databaseAllCsv(request, info="", iferror=""):
	"""
	Gather data about all fragments in latest session to create their CSV file.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render or HttpResponse
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get the latest session:
		latestSession = Session.objects.filter(user_id=request.user)
		if(latestSession):
			latestSession = latestSession.latest('id')
		else:
			print "CSV session error occurred"
			info = "An error occurred. Chceck if everything is ok with your browsing session."
			iferror = "1"
			latestSession = Session.objects.create(user=request.user, date=datetime.date.today().isoformat(), saved=False)
		#get fragments form the latest session:
		session_list = latestSession.fragments.all()
		if(session_list):
			return createCsv(session_list, str(latestSession.id), request.user.username)
		else:
			#if there are aren't ant fragments then just reopen the Database page:
			fragment_list = afterHowMany(data, '')
			return setDatabasePageParams(request, fragment_list, int(data['h_fromWhich']), int(data['h_toWhich']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), int(data['h_fromWhichMoved']), int(data['h_toWhichMoved']), int(data['selectedMoved']), info, iferror)
	else:
		return database(request)

def createSsv(session_list, session_id, username):
	"""
	Create SSV file for fragments.
	
	@param session_list: list of dictionaries representing objects of Fragment model
	@type session_list: list of dictionaries
	@param session_id: id of the latest session
	@type session_id: integer
	@param username: name of logged user
	@type username: string
	@return: HttpResponse
	"""
	fileContent = ""
	for s in session_list:
		#create id:
		fileContent = fileContent + "AMY" + str(s.amyindex) + ";"
		#create name:
		fileContent = fileContent + s.name + ";"
		#create sequence:
		fileContent = fileContent + s.sequence + ";"
		#create fibrilation data:
		if (s.ifaggregate == True):
			ifaggreg = "Yes"
		else:
			ifaggreg = "No"
		fileContent = fileContent + ifaggreg + ";"
		#create protein name:
		fileContent = fileContent + s.protein.name + ";"
		#create solution data:
		fileContent = fileContent + s.solution + ";"
		fileContent = fileContent + "METHODS;"
		for m in s.methods.all():
			if (m.reviewed == 1):
				fileContent = fileContent + m.name + ";"
		fileContent = fileContent + "DISEASES;"
		for d in s.diseases.all():
			if (d.reviewed == 1):
				fileContent = fileContent + d.name + ";"
		fileContent = fileContent + "SOURCES;"
		#create sources data:
		for p in s.sources.all():
			if (p.reviewed == 1):
				fileContent = fileContent + "SOURCE;"
				fileContent = fileContent + p.name + ";"
				fileContent = fileContent + "SOURCE_REFERENCES;"
				#create references of source:
				for c in p.citations.all():
					if (c.reviewed == 1):
						fileContent = fileContent + c.name + ";"
						fileContent = fileContent + str(c.pmid) + ";"
						fileContent = fileContent + c.link + ";"
		#create references data:
		fileContent = fileContent + "REFERENCES;"
		for c in s.citations.all():
			if (c.reviewed == 1):
				fileContent = fileContent + c.name + ";"
				fileContent = fileContent + str(c.pmid) + ";"
				fileContent = fileContent + c.link + ";"
		fileContent = fileContent + "\r\n"
	#create file to download:
	response = HttpResponse(content_type='text/plain')
	response['Content-Disposition'] = 'attachment; filename="%s"' % ("AmyLoad_session_" + session_id + "_" + username + ".ssv")
	response.write(fileContent)
	return response

def createCsv(session_list, session_id, username):
	"""
	Create CSV file for fragments.
	
	@param session_list: list of dictionaries representing objects of Fragment model
	@type session_list: list of dictionaries
	@param session_id: id of the latest session
	@type session_id: integer
	@param username: name of logged user
	@type username: string
	@return: HttpResponse
	"""
	fileContent = ""
	for s in session_list:
		#create id:
		fileContent = fileContent + "AMY" + str(s.amyindex) + ","
		#create protein name:
		fileContent = fileContent + s.protein.name + ","
		#create name:
		fileContent = fileContent + s.name + ","
		#create sequence:
		fileContent = fileContent + s.sequence + ","
		#create fibrilation data:
		if (s.ifaggregate == True):
			ifaggreg = "Yes"
		else:
			ifaggreg = "No"
		fileContent = fileContent + ifaggreg + "\r\n"
	#create file to download:
	response = HttpResponse(content_type='text/plain')
	response['Content-Disposition'] = 'attachment; filename="%s"' % ("AmyLoad_session_" + session_id + "_" + username + ".csv")
	response.write(fileContent)
	return response

@login_required
def databaseSaveSession(request, info="", iferror=""):
	"""
	Save fragments in temporary session into saved session.
	
	@param info: information text to display on Database page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	existing = ""
	if (request.method == 'POST'):
		try:
			with transaction.atomic():
				data = request.POST
				latestSession = Session.objects.filter(user_id=request.user)
				if(latestSession):
					#get fragments of the latest session:
					latestSession = latestSession.latest('id')
					session_list = latestSession.fragments.all()
					if(session_list):
						allSessions = Session.objects.filter(user_id=request.user, saved=True)
						#check if the user has already session with the same fragments (if session is unique):
						for s in allSessions:
							tmp = s.fragments.all()
							length = len(set(tmp).intersection(session_list))
							if((length == len(session_list)) and (length == len(tmp))):
								existing = s.name
								break
						if(existing == ""):
							#if the session is unique then save it:
							form = SaveSessionForm(request.POST)
							latestSession.name = form.data['saveName']
							latestSession.saved = True
							latestSession.save()
							latestSession.pk = None
							latestSession.save()
							latestSession.name = None
							latestSession.saved = False
							latestSession.fragments.add(*session_list)
							latestSession.save()
							info = "Session saved as \"" + form.data['saveName'] + "\". It is visible in \"Your data\" section."
							iferror = "0"
						else:
							info = "Session with the same sequences already exists. It is called \"" + existing + "\". Find it in \"Your data\" section."
							iferror = "1"
					else:
						info = "You cannot save empty session."
						iferror = "1"
				else:
					latestSession = Session.objects.create(user=request.user, date=datetime.date.today().isoformat(), saved=False)
					print "Save session error occurred"
					info = "An error occurred. Chceck if everything is ok with your browsing session."
					iferror = "1"
				fragment_list = afterHowMany(data, '')
				return setDatabasePageParams(request, fragment_list, int(data['h_fromWhich']), int(data['h_toWhich']), "AmyLoad - Database", SortingForm(request.POST), SaveSessionForm(), int(data['selected']), int(data['h_fromWhichMoved']), int(data['h_toWhichMoved']), int(data['selectedMoved']), info, iferror)
		except Exception as e:
				info = "An error occurred. Chceck if everything is ok with your browsing session and try again or contact our admin."
				iferror = "1"
				traceback.print_exc()
	else:
		return database(request)
	resultsMoved = getSessionData(request, fromWhichMoved, toWhichMoved)
	return render(request, 'amyload/database.html', {'fragments': results['data_list'], 'sessionList': resultsMoved['session_list'], 'title': "AmyLoad - Database", 'form': form, 'saveForm': saveForm, 'howMany': results['howMany'], 'howManyMoved': resultsMoved['howManyMoved'], 'selected': selected, 'selectedMoved': selectedMoved, 'fromWhich': fromWhich, 'fromWhichMoved': fromWhichMoved, 'toWhich': toWhich, 'toWhichMoved': toWhichMoved, 'info': info, 'iferror': iferror})

def checkProtein(user, proteinName):
	"""
	Check if protein gave by the user in submit form is already in the database and create it if not.
	
	@param user: user who submit XML file
	@type user: Django model object
	@param proteinName: name of fragment's protein
	@type proteinName: string
	@return: Protein model object
	"""
	#search for this proteins in the database:
	matches = Protein.objects.filter(name__iexact=proteinName.strip())
	if (matches):
		protein = matches[0]
	else:
		#if protein doesn't exist in the database then add it (if user is an admin then it is instantly reviewed):
		if(user.is_superuser):
			protein = Protein.objects.create(name=proteinName.strip(), date=datetime.date.today(), reviewed=1, user=user)
		else:
			protein = Protein.objects.create(name=proteinName.strip(), date=datetime.date.today(), reviewed=0, user=user)
	return protein

def checkMethods(user, metTable):
	"""
	Create list of methods IDs added by the user in submit form and add new into the database.
	
	@param user: user who submit XML file
	@type user: Django model object
	@param metTable: names of methods
	@type metTable: string[]
	@return: string with methods' IDs separated with semicolons
	"""
	methods = ''
	#for each method:
	for m in metTable:
		if(m.strip() != ''):
			#check if method exists in database:
			matches = Method.objects.filter(name__iexact=m.strip())
			if (matches):
				#IDs are separated with semicolon:
				methods = methods + str(matches[0].id) + ';'
			else:
				#if method doesn't exist in the database then add it (if user is an admin then it is instantly reviewed):
				if(user.is_superuser):
					tmpMeth = Method.objects.create(name=m.strip(), date=datetime.date.today(), reviewed=1, user=user)
				else:
					tmpMeth = Method.objects.create(name=m.strip(), date=datetime.date.today(), reviewed=0, user=user)
				#IDs are separated with semicolon:
				methods = methods + str(tmpMeth.id) + ';'
	return methods

def checkDiseases(user, disTable):
	"""
	Create list of diseases IDs added by the user in submit form and add new into the database.
	
	@param user: user who submit XML file
	@type user: Django model object
	@param disTable: names of diseases
	@type disTable: string[]
	@return: string with diseases' IDs separated with semicolons
	"""
	diseases = ''
	#for each disease:
	for d in disTable:
		if(d.strip() != ''):
			#check if disease exists in database:
			matches = Disease.objects.filter(name__iexact=d.strip())
			if (matches):
				#IDs are separated with semicolon:
				diseases = diseases + str(matches[0].id) + ';'
			else:
				#if disease doesn't exist in the database then add it (if user is an admin then it is instantly reviewed):
				if(user.is_superuser):
					tmpDis = Disease.objects.create(name=d.strip(), date=datetime.date.today(), reviewed=1, user=user)
				else:
					tmpDis = Disease.objects.create(name=d.strip(), date=datetime.date.today(), reviewed=0, user=user)
				#IDs are separated with semicolon:
				diseases = diseases + str(tmpDis.id) + ';'
	return diseases

def checkSource(user, sourceName, references):
	"""
	Check if sources and references gave by the user in submit form are already in the database and create them if not.
	
	@param user: user who submit XML file
	@type user: Django model object
	@param sourceName: name of the fragment's source
	@type sourceName: string
	@param references: references of the source
	@type references: list of dictionaries
	@return: Source model object
	"""
	if (sourceName != ''):
		#if user is an admin then it is instantly reviewed when added into the database:
		if(user.is_superuser):
			superuser = 1
		else:
			superuser = 0
		#check if source exists in the database:
		matches = Source.objects.filter(name__iexact=sourceName.strip())
		if (matches):
			source = matches[0]
			#for each source's reference:
			for r in references:
				#if user gave PMID:
				if (r['pmid'] != ''):
					#check if PMID exists in the database:
					matches = Citation.objects.filter(pmid=r['pmid'].strip())
					if (matches):
						#if it exists then relate its citation with the source object:
						citationSource = matches[0]
						if (not citationSource.source_set.filter(name__iexact=source.name).exists()):
							source.citations.add(citationSource)
					else:
						#if not then create url and a new citation object:
						if(r['url'].strip() == ''):
							r['url'] = "www.ncbi.nlm.nih.gov/pubmed/" + r['pmid'].strip()
						citationSource = Citation.objects.create(name=r['name'].strip(), pmid=r['pmid'].strip(), link=r['url'].strip(), date=datetime.date.today(), reviewed=superuser, user=user)
						source.citations.add(citationSource)
				elif (r['url'] != ''):
					#check if url exists in the database:
					matches = Citation.objects.filter(link=r['url'].strip())
					if (matches):
						#it it exists then relate its citation with the source object:
						citationSource = matches[0]
						if (not citationSource.source_set.filter(name__iexact=source.name).exists()):
							source.citations.add(citationSource)
					else:
						#if not then create new citation object:
						citationSource = Citation.objects.create(name=r['name'].strip(), link=r['url'].strip(), date=datetime.date.today(), reviewed=superuser, user=user)
						source.citations.add(citationSource)
		else:
			#create new source:
			source = Source.objects.create(name=sourceName.strip(), date=datetime.date.today(), reviewed=superuser, user=user)
			for r in references:
				#if user gave PMID:
				if (r['pmid'] != ''):
					#check if PMID exists in the database:
					matches = Citation.objects.filter(pmid=r['pmid'].strip())
					if (matches):
						#if it exists then relate its citation with the source object:
						citationSource = matches[0]
						source.citations.add(citationSource)
					else:
						#if not then create url and a new citation object:
						if(r['url'].strip() == ''):
							r['url'] = "www.ncbi.nlm.nih.gov/pubmed/" + r['pmid'].strip()
						citationSource = Citation.objects.create(name=r['name'].strip(), pmid=r['pmid'].strip(), link=r['url'].strip(), date=datetime.date.today(), reviewed=superuser, user=user)
						source.citations.add(citationSource)
				elif (r['url'] != ''):
					#check if url exists in the database:
					matches = Citation.objects.filter(link=r['url'].strip())
					if (matches):
						#it it exists then relate its citation with the source object:
						citationSource = matches[0]
						source.citations.add(citationSource)
					else:
						#if not then create url and a new citation object:
						citationSource = Citation.objects.create(name=r['name'].strip(), link=r['url'].strip(), date=datetime.date.today(), reviewed=superuser, user=user)
						source.citations.add(citationSource)
	else:
		return "NULL"
	return source

def checkCitation(user, name, pmid, link):
	"""
	Check if reference gave by the user in submit form is already in the database and create them if not.
	
	@param user: user who submit XML file
	@type user: Django model object
	@param name: name of the reference
	@type name: string
	@param pmid: pmid of the reference
	@type pmid: string
	@param link: link of the reference
	@type link: string
	@return: Citation model object
	"""
	if (name != ''):
		#if user gave PMID:
		if (pmid != ''):
			#check if PMID exists in the database:
			matches = Citation.objects.filter(pmid=pmid.strip())
			if (matches):
				citation = matches[0]
			else:
				#if not then create url and a new citation object (if user is an admin then it is instantly reviewed):
				if(link.strip() == ''):
					link = "www.ncbi.nlm.nih.gov/pubmed/" + pmid.strip()
				if(user.is_superuser):
					citation = Citation.objects.create(name=name.strip(), pmid=pmid.strip(), link=link.strip(), date=datetime.date.today(), reviewed=1, user=user)
				else:
					citation = Citation.objects.create(name=name.strip(), pmid=pmid.strip(), link=link.strip(), date=datetime.date.today(), reviewed=0, user=user)
		else:
			#check if url exists in the database:
			matches = Citation.objects.filter(link=link.strip())
			if (matches):
				citation = matches[0]
			else:
				#if not then create url and a new citation object (if user is an admin then it is instantly reviewed):
				if(user.is_superuser):
					citation = Citation.objects.create(name=name.strip(), link=link.strip(), date=datetime.date.today(), reviewed=1, user=user)
				else:
					citation = Citation.objects.create(name=name.strip(), link=link.strip(), date=datetime.date.today(), reviewed=0, user=user)
	else:
		return "NULL"
	return citation

def createFragment(user, name, sequence, aggregation, protein, solution, method, methods, disease, diseases, sources, citations, note):
	"""
	Create new fragment.
	
	@param user: user who submit XML file
	@type user: Django model object
	@param name: name of the fragment
	@type name: string
	@param sequence: sequence of the fragment
	@type sequence: string
	@param aggregation: information about fibrilation
	@type aggregation: string
	@param protein: protein database object
	@type protein: Django model object
	@param solution: information abour solution
	@type solution: string
	@param method: method database object
	@type method: Django model object
	@param methods: method's objects IDs separated with semicolons
	@type methods: string
	@param sources: list of source database object
	@type sources: list of Django model objects
	@param citations: list of citation database object
	@type citations: list of Django model objects
	@param note: note to reviewer
	@type note: string
	"""
	#check aggregation:
	if(aggregation == "No"):
		aggregation = 0
	elif(aggregation == "Yes"):
		aggregation = 1
	amyId = Fragment.objects.all().aggregate(Max('amyindex'))['amyindex__max'] + 1
	#if user is an admin then fragment is instantly reviewed:
	if(user.is_superuser):
		fragment = Fragment.objects.create(amyindex=amyId, name=name, sequence=sequence, ifaggregate=aggregation, protein=protein, solution=solution, reviewed=1, date=datetime.date.today(), user=user, note=note)
	else:
		fragment = Fragment.objects.create(amyindex=amyId, name=name, sequence=sequence, ifaggregate=aggregation, protein=protein, solution=solution, reviewed=0, date=datetime.date.today(), user=user, note=note)
	#check if methods are in the form of model objects or a string with semicolons and add them to fragment:
	if (methods != '' or method != 'NULL'):
		if (method != 'NULL'):
			fragment.methods.add(method)
		else:
			metTable = methods.split(';')
			for m in metTable:
				if (m != ''):
					matches = Method.objects.filter(id=int(m.strip()))
					if (matches):
						fragment.methods.add(matches[0])
	#check if diseases are in the form of model objects or a string with semicolons and add them to fragment:
	if (diseases != '' or disease != 'NULL'):
		if (disease != 'NULL'):
			fragment.diseases.add(disease)
		else:
			disTable = diseases.split(';')
			for d in disTable:
				if (d != ''):
					matches = Disease.objects.filter(id=int(d.strip()))
					if (matches):
						fragment.diseases.add(matches[0])
	#add sources to fragment:
	for s in sources:
		fragment.sources.add(s)
	#add citations to fragment:
	for c in citations:
		fragment.citations.add(c)

@login_required
def insertXml(request):
	"""
	Start the process of fragments submission with XML file.
	
	@return: page render
	"""
	errors = ""
	info = ""
	xmlInfo = ""
	xmlError = ""
	if request.method == 'POST':
		uploadform = UploadFileForm(request.POST, request.FILES)
		if uploadform.is_valid():
			#create temporary file with uploaded file's data:
			fp = tempfile.TemporaryFile()
			fp.write(request.FILES["file"].read())
			fp.seek(0)
			#run parallel process to validate XML file and create new fragments:
			th = Thread(target=backgroundInsertXml, args=(fp, request.user, request.FILES["file"].name,))
			th.setDaemon(True)
			th.start()
			xmlInfo = "E-mail with the submission report will be sent to " + request.user.email + " after the analysis of your XML file."
		else:
			xmlError = "You must submit a file."
	insert_form = InsertForm()
	upload_form = UploadFileForm()
	return render(request, 'amyload/insertSeq.html', {'upload_form': upload_form, 'insert_form': insert_form, 'title': "AmyLoad - Insert Sequence", 'errors': errors, 'info': info, 'xmlInfo': xmlInfo, 'xmlError': xmlError})

def backgroundInsertXml(file, user, fileName):
	"""
	Validates data given by user in XML file and create new fragments.
	"""
	emailContent = ""
	try:
		with transaction.atomic():
			tree = etree.parse(file)
			selected = tree.getroot()
			#for each <fragment> in XML:
			for index, fragment in enumerate(selected.iter('fragment')):
				noErrors = True
				#validate <name>:
				if(len(fragment.findall('name')) > 1):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has more than 1 <name> tag.\n"
					noErrors = False
				elif(fragment.find('name') != None and fragment.find('name').text != None and not len(fragment.find('name').text.strip()) <= Fragment._meta.get_field('name').max_length):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has too long value in <name> tag. Maximum size of the name is " + str(Fragment._meta.get_field('name').max_length) + ".\n"
					noErrors = False
				#validate <sequence>:
				if(fragment.find('sequence') == None):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " doesn't have <sequence> tag.\n"
					noErrors = False
				elif(len(fragment.findall('sequence')) > 1):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has more than 1 <sequence> tag.\n"
					noErrors = False
				elif(fragment.find('sequence').text == None or fragment.find('sequence').text.strip() == ''):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has empty <sequence> tag.\n"
					noErrors = False
				#elif(not fragment.find('sequence').text.isalpha()):
				#	emailContent = emailContent + "<fragment> no. " + str(index+1) + " has non letter characters in <sequence> tag.\n"
				#	noErrors = False
				elif(not len(fragment.find('sequence').text.strip()) <= Fragment._meta.get_field('sequence').max_length):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has too long value in <sequence> tag. Maximum size of the sequence is " + str(Fragment._meta.get_field('sequence').max_length) + ".\n"
					noErrors = False
				#validate <fibrilation>:
				if(fragment.find('fibrilation') == None):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " doesn't have <fibrilation> tag.\n"
					noErrors = False
				elif(len(fragment.findall('fibrilation')) > 1):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has more than 1 <fibrilation> tag.\n"
					noErrors = False
				elif(not (fragment.find('fibrilation').text.strip() == "Yes" or fragment.find('fibrilation').text.strip() == "No")):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has no 'Yes'/'No' in <fibrilation> tag.\n"
					noErrors = False
				#validate <protein>:
				if(fragment.find('protein') == None):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " doesn't have <protein> tag.\n"
					noErrors = False
				elif(len(fragment.findall('protein')) > 1):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has more than 1 <protein> tag.\n"
					noErrors = False
				elif(fragment.find('protein').find('name') == None):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has no <name> tag in <protein> tag.\n"
					noErrors = False
				elif(len(fragment.find('protein').find('name')) > 1):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has more than 1 <name> tag in <protein> tag.\n"
					noErrors = False
				elif(fragment.find('protein').find('name').text == None or fragment.find('protein').find('name').text.strip() == ''):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has <protein> tag with empty <name> tag.\n"
					noErrors = False
				elif(not len(fragment.find('protein').find('name').text.strip()) <= Protein._meta.get_field('name').max_length):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has too long value in <name> tag in <protein> tag. Maximum size of the protein name is " + str(Protein._meta.get_field('name').max_length) + ".\n"
					noErrors = False
				#validate <solution>:
				if(len(fragment.findall('solution')) > 1):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has more than 1 <solution> tag.\n"
					noErrors = False
				elif (fragment.find('solution') != None and fragment.find('solution').text != None and not len(fragment.find('solution').text.strip()) <= Fragment._meta.get_field('solution').max_length):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has too long value in <solution> tag. Maximum size of the solution is " + str(Fragment._meta.get_field('solution').max_length) + ".\n"
					noErrors = False
				#validate <method>:
				for m in fragment.findall('method'):
					if(m.find('name') == None):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <method> tag without <name> tag inside.\n"
						noErrors = False
						break;
					elif(len(m.findall('name')) > 1):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <method> tag with more than 1 <name> tag.\n"
						noErrors = False
						break;
					elif(m.find('name').text != None and not len(m.find('name').text.strip()) <= Method._meta.get_field('name').max_length):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <method> tag with too long value in <name> tag. Maximum size of the method name is " + str(Method._meta.get_field('name').max_length) + ".\n"
						noErrors = False
						break;
				#validate <disease>:
				for d in fragment.findall('disease'):
					if(d.find('name') == None):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <disease> tag without <name> tag inside.\n"
						noErrors = False
						break;
					elif(len(d.findall('name')) > 1):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <disease> tag with more than 1 <name> tag.\n"
						noErrors = False
						break;
					elif(d.find('name').text != None and not len(d.find('name').text.strip()) <= Disease._meta.get_field('name').max_length):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <disease> tag with too long value in <name> tag. Maximum size of the method name is " + str(Disease._meta.get_field('name').max_length) + ".\n"
						noErrors = False
						break;
				#validate <source>:
				for s in fragment.findall('source'):
					if(s.find('name') == None):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <source> tag without <name> tag inside.\n"
						noErrors = False
						break;
					elif(len(s.findall('name')) > 1):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <source> tag with more than 1 <name> tag.\n"
						noErrors = False
						break;
					elif(s.find('name').text == None or s.find('name').text.strip() == ''):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <source> tag with empty <name> tag.\n"
						noErrors = False
						break;
					elif(not len(s.find('name').text.strip()) <= Source._meta.get_field('name').max_length):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <source> tag with too long value in <name> tag. Maximum size of the source name is " + str(Source._meta.get_field('name').max_length) + ".\n"
						noErrors = False
						break;
					#validate <reference> in <source>:
					for r in s.findall('reference'):
						if(r.find('name') == None):
							emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <source> tag with at least one <reference> tag without <name> tag.\n"
							noErrors = False
							break;
						elif(len(r.findall('name')) > 1):
							emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <source> tag with at least one <reference> tag with more than 1 <name> tag.\n"
							noErrors = False
							break;
						elif(r.find('name').text == None or r.find('name').text.strip() == ''):
							emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <source> tag with at least one <reference> tag with empty <name> tag.\n"
							noErrors = False
							break;
						elif(not len(r.find('name').text.strip()) <= Citation._meta.get_field('name').max_length):
							emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <source> tag with at least one <reference> tag with too long value in <name> tag. Maximum size of the reference name is " + str(Citation._meta.get_field('name').max_length) + ".\n"
							noErrors = False
							break;
						elif(r.find('pmid') == None and r.find('url') == None):
							emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <source> tag with at least one <reference> tag without both <pmid> and <url> tags.\n"
							noErrors = False
							break;
						elif(len(r.findall('pmid')) > 1 or len(r.findall('url')) > 1):
							emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <source> tag with at least one <reference> tag with more than 1 <pmid> or <url> tag.\n"
							noErrors = False
							break;
						elif((r.find('pmid') != None and (r.find('pmid').text == None or r.find('pmid').text.strip() == '')) and (r.find('url') != None and (r.find('url').text == None or r.find('url').text.strip() == ''))):
							emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <source> tag with at least one <reference> tag with both <pmid> and <url> tags empty.\n"
							noErrors = False
							break;
						elif(r.find('pmid') != None and r.find('pmid').text != None and not r.find('pmid').text.isdigit()):
							emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <source> tag with at least one <reference> tag with <pmid> which contains not only numbers.\n"
							noErrors = False
							break;
						elif(r.find('url') != None and r.find('url').text != None and r.find('url').text.strip() != '' and not urlparse(r.find('url').text.strip()).scheme):
							emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <source> tag with at least one <reference> tag with <url> which is not valid. Perhaps you have forgotten about 'http'.\n"
							noErrors = False
							break;
						elif(r.find('url') != None and r.find('url').text != None and not len(r.find('url').text.strip()) <= Citation._meta.get_field('link').max_length):
							emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <source> tag with at least one <reference> tag with too long value in <url> tag. Maximum size of the reference url is " + str(Citation._meta.get_field('link').max_length) + ".\n"
							noErrors = False
							break;
				#validate <reference>:
				for r in fragment.findall('reference'):
					if(r.find('name') == None):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <reference> tag without <name> tag.\n"
						noErrors = False
						break;
					elif(len(r.findall('name')) > 1):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <reference> tag with more than 1 <name> tag.\n"
						noErrors = False
						break;
					elif(r.find('name').text == None or r.find('name').text.strip() == ''):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <reference> tag with <name> tag empty.\n"
						noErrors = False
						break;
					elif(not len(r.find('name').text.strip()) <= Citation._meta.get_field('name').max_length):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <reference> tag with too long value in <name> tag. Maximum size of the reference name is " + str(Citation._meta.get_field('name').max_length) + ".\n"
						noErrors = False
						break;
					elif(r.find('pmid') == None and r.find('url') == None):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <reference> tag without both <pmid> and <url> tags.\n"
						noErrors = False
						break;
					elif(len(r.findall('pmid')) > 1 or len(r.findall('url')) > 1):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <reference> tag with more than 1 <pmid> or <url> tag.\n"
						noErrors = False
						break;
					elif((r.find('pmid') != None and (r.find('pmid').text == None or r.find('pmid').text.strip() == '')) and (r.find('url') != None and (r.find('url').text == None or r.find('url').text.strip() == ''))):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <reference> tag with both <pmid> and <url> tags empty.\n"
						noErrors = False
						break;
					elif(r.find('pmid') != None and r.find('pmid').text != None and not r.find('pmid').text.isdigit()):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <reference> tag with <pmid> which contains not only numbers.\n"
						noErrors = False
						break;
					elif(r.find('url') != None and r.find('url').text != None and r.find('url').text.strip() != '' and not urlparse(r.find('url').text.strip()).scheme):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <reference> tag with <url> which is not valid. Perhaps you have forgotten about 'http'.\n"
						noErrors = False
						break;
					elif(r.find('url') != None and r.find('url').text != None and not len(r.find('url').text.strip()) <= Citation._meta.get_field('link').max_length):
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " has at least one <reference> tag with too long value in <url> tag. Maximum size of the reference url is " + str(Citation._meta.get_field('link').max_length) + ".\n"
						noErrors = False
						break;
				#validate <note>:
				if(len(fragment.findall('note')) > 1):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has more than 1 <note> tag.\n"
					noErrors = False
				elif (fragment.find('note') != None and fragment.find('note').text != None and not len(fragment.find('note').text.strip()) <= Fragment._meta.get_field('note').max_length):
					emailContent = emailContent + "<fragment> no. " + str(index+1) + " has too long value in <note> tag. Maximum size of the note is " + str(Fragment._meta.get_field('note').max_length) + ".\n"
					noErrors = False
				#if there were no errors for analyzed fragment:
				if(noErrors):
					methods = ''
					diseases = ''
					sources = []
					citations = []
					#check if protein exists and if not then create it:
					protein = checkProtein(user, fragment.find('protein').find('name').text)
					sequence = fragment.find('sequence').text.strip().upper()
					aggregation = fragment.find('fibrilation').text.strip()
					#check (AGAIN) if fragment exists already in the database:
					ifExist = Fragment.objects.filter(sequence=sequence, protein=protein)
					if(not ifExist):
						#create fragment's name:
						if(fragment.find('name') != None and fragment.find('name').text != None and fragment.find('name').text.strip() != ''):
							name = fragment.find('name').text.strip()
						else:
							name = "Untitled"
						#create fragment's solution data:
						if(fragment.find('solution') != None and fragment.find('solution').text != None and fragment.find('solution').text.strip() != ''):
							solution = fragment.find('solution').text.strip()
						else:
							solution = "No data"
						#create fragment's methods data:
						if(fragment.findall('method')):
							metTable = fragment.xpath('method/name/text()')
							methods = checkMethods(user, metTable)
						#create fragment's diseases data:
						if(fragment.findall('disease')):
							disTable = fragment.xpath('disease/name/text()')
							diseases = checkDiseases(user, disTable)
						#create fragment's source data:
						for s in fragment.findall('source'):
							references = []
							for r in s.findall('reference'):
								if(r.find('pmid') == None or r.find('pmid').text == None or r.find('pmid').text.strip() == ''):
									pmidTmp = ''
								else:
									pmidTmp = r.find('pmid').text
								if(r.find('url') == None or r.find('url').text == None or r.find('url').text.strip() == ''):
									urlTmp = ''
								else:
									tmpUrl = urlparse(r.find('url').text)
									if (tmpUrl.scheme):
										urlTmp = tmpUrl.netloc + tmpUrl.path
									else:
										urlTmp = r.find('url').text
								refTmp = {'name': r.find('name').text, 'pmid': pmidTmp, 'url': urlTmp}
								references.append(refTmp)
							sources.append(checkSource(user, s.find('name').text, references))
						#create fragment's reference data:
						for r in fragment.findall('reference'):
							if(r.find('pmid') == None or r.find('pmid').text == None or r.find('pmid').text.strip() == ''):
								pmidTmp = ''
							else:
								pmidTmp = r.find('pmid').text
							if(r.find('url') == None or r.find('url').text == None or r.find('url').text.strip() == ''):
								urlTmp = ''
							else:
								tmpUrl = urlparse(r.find('url').text)
								if (tmpUrl.scheme):
									urlTmp = tmpUrl.netloc + tmpUrl.path
								else:
									urlTmp = r.find('url').text
							citations.append(checkCitation(user, r.find('name').text, pmidTmp, urlTmp))
						#create fragment's note to reviewer:
						if(fragment.find('note') != None and fragment.find('note').text != None and fragment.find('note').text.strip() != ''):
							note = fragment.find('note').text.strip()
						else:
							note = ''
						#create fragment:
						createFragment(user, name, sequence, aggregation, protein, solution, "NULL", methods, "NULL", diseases, sources, citations, note)
						emailContent = emailContent + "<fragment> no. " + str(index+1) + " added properly into the database. Status = unrevised. After the revision it will be visible in the database.\n"
					else:
						emailContent = emailContent + "Fragment with sequence from <fragment> no. " + str(index+1) + " already exists in a database for chosen protein. If you don't see it, perhaps it is waiting for the review.\n"
						noErrors = False
	except Exception as e:
		emailContent = "An error occurred. Perhaps your xml file is invalid. Check its structure if it matches the guidelines or contact our admin.\n"
		traceback.print_exc()
	#create and send email:
	emailContent = "Submission report for file: \"" + str(fileName) + "\", user: \"" + user.username + "\".\n\n" + emailContent
	emailContent = "This e-mail is sent automatically. Do not respond.\n\n" + emailContent
	send_mail('AmyLoad - xml submission report', emailContent, 'comprec.pwr@gmail.com', [user.email], fail_silently=False)
	file.close()

@login_required
def aboutYou(request, info="", emailForm="", passForm=""):
	"""
	Display forms on the 'About You' page.
	
	@param info: information text to display on 'About You' page
	@type info: string
	@param emailForm: email change form
	@type emailForm: Django form object
	@param passForm: password change form
	@type passForm: Django form object
	@return: page render
	"""
	if(not emailForm):
		emailForm = ChangeEmailForm()
	if(not passForm):
		passForm = ChangePassForm()
	#get user personal data:
	name = request.user.username
	email = request.user.email
	date = request.user.date_joined
	return render(request, 'amyload/aboutYou.html', {'title': "AmyLoad - About You", 'name': name, 'email': email, 'date': date, 'emailForm': emailForm, 'passForm': passForm, 'info': info})

@login_required
def changeEmail(request):
	"""
	Change user's email.
	
	@return: page render
	"""
	info = ""
	if request.method == 'POST':
		#check if email form was properly filled:
		emailForm = ChangeEmailForm(data=request.POST)
		if(emailForm.is_valid()):
			user = request.user
			user.email = emailForm['newEmail'].data
			user.save()
			info = "Email changed."
	else:
		emailForm = ChangeEmailForm()
	return aboutYou(request, info, emailForm, "")

@login_required
def changePassword(request):
	"""
	Change user's password.
	
	@return: page render
	"""
	info = ""
	if request.method == 'POST':
		#check if password form was properly filled:
		passForm = ChangePassForm(data=request.POST)
		if(passForm.is_valid()):
			request.user.set_password(passForm['newPass1'].data)
			request.user.save()
			info = "Password changed."
	return aboutYou(request, info, "", passForm)

def setAddedFragsPageParams(request, fragment_list, fromWhich, toWhich, title, form, selected):
	"""
	Pass parameters to the 'Added fragments' page.
	
	@param fragment_list: list of dictionaries representing objects of Fragment model
	@type fragment_list: list of dictionaries
	@param fromWhich: number of first fragment to display in table
	@type fromWhich: integer
	@param toWhich: number of last fragment to display in table
	@type toWhich: integer
	@param title: title of the web page
	@type title: string
	@param form: filtering form on the 'Added fragments' page
	@type form: Django form object
	@param selected: number of fragments displayed in table
	@type selected: integer
	@return: page render
	"""
	#get specific subset of fragments to present in table:
	results = getDatabaseData(fragment_list, fromWhich, toWhich)
	return render(request, 'amyload/addedFrags.html', {'fragments': results['data_list'], 'title': title, 'form': form,
	'howMany': results['howMany'], 'selected': selected, 'fromWhich': fromWhich, 'toWhich': toWhich})

@login_required
def addedFrags(request):
	"""
	Open 'Added fragments' page.
	
	@return: page render
	"""
	#get fragments to present in table:
	fragment_list = Fragment.objects.filter(user__id=request.user.id).order_by('protein__name')
	return setAddedFragsPageParams(request, fragment_list, 1, 10, "AmyLoad - Added fragments", SortingAddedForm(user=request.user), 10)

@login_required
def addedFilter(request):
	"""
	Gather data for table in 'Added fragments' page after clicking the 'Filter' button.
	
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in table:
		fragment_list = afterFilter(data, request.user)
		#check if the fragment list is empty:
		if (len(fragment_list) == 0):
			fromWhich = 0
		else:
			fromWhich = 1
		return setAddedFragsPageParams(request, fragment_list, fromWhich, int(data['selected']), "AmyLoad - Added fragments", SortingAddedForm(data=request.POST, user=request.user), int(data['selected']))
	else:
		return addedFrags(request)

@login_required
def addedHowMany(request):
	"""
	Gather data for table after changing number of fragments displayed in table on the 'Added fragments' page.
	
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in table:
		fragment_list = afterHowMany(data, request.user)
		return setAddedFragsPageParams(request, fragment_list, 1, int(data['selected']), "AmyLoad - Added fragments", SortingAddedForm(data=request.POST, user=request.user), int(data['selected']))
	else:
		return addedFrags(request)

@login_required
def addedPrevious(request):
	"""
	Gather data for table after clicking the 'Previous' link above table on the 'Added fragments' page.
	
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in table:
		fragment_list = afterHowMany(data, request.user)
		return setAddedFragsPageParams(request, fragment_list, int(data['h_fromWhich']) - int(data['selected']), int(data['h_toWhich']) - int(data['selected']), "AmyLoad - Added fragments", SortingAddedForm(data=request.POST, user=request.user), int(data['selected']))
	else:
		return addedFrags(request)

@login_required
def addedNext(request):
	"""
	Gather data for table after clicking the 'Next' link above table on the 'Added fragments' page.
	
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in table:
		fragment_list = afterHowMany(data, request.user)
		return setAddedFragsPageParams(request, fragment_list, int(data['h_fromWhich']) + int(data['selected']), int(data['h_toWhich']) + int(data['selected']), "AmyLoad - Added fragments", SortingAddedForm(data=request.POST, user=request.user), int(data['selected']))
	else:
		return addedFrags(request)

@login_required
def addedFragmentInfo(request, fragId="-1"):
	"""
	Shows information about chosen added by user fragment.
	
	@param fragId: fragment database id
	@type fragId: integer
	@return: page render
	"""
	fragment_list = Fragment.objects.filter(id=fragId, user=request.user)
	if(fragment_list):
		#get all fragment's data:
		fragment_list = fragment_list[0]
		amyId = fragment_list.amyindex
		name = fragment_list.name
		protein = fragment_list.protein.name
		ifaggregate = fragment_list.ifaggregate
		sequence = fragment_list.sequence
		solution = fragment_list.solution
		methods = fragment_list.methods.all()
		sources = fragment_list.sources.all()
		citations = fragment_list.citations.all()
		diseases = fragment_list.diseases.all()
		added = "Yes"
		date = fragment_list.date
		note = fragment_list.note
		
		data = {'amyId': amyId, 'name': name, 'protein': protein, 'ifaggregate': ifaggregate, 'sequence': sequence, 'solution': solution, 
			'methods': methods, 'sources': sources, 'citations': citations, 'diseases': diseases, 'title': "AmyLoad - Fragment Info", 'added': added, 'date': date,
			'note': note}
		return render(request, 'amyload/fragmentInfo.html', data)
	else:
		return render(request, 'amyload/home.html', {'title': "AmyLoad - Home"})

def setSavedSessionsPageParams(request, session_list, fromWhich, toWhich, title, form, selected, info="", iferror=""):
	"""
	Pass parameters to the 'Saved sessions' page.
	
	@param session_list: list of dictionaries representing objects of Fragment model
	@type session_list: list of dictionaries
	@param fromWhich: number of first fragment to display in table
	@type fromWhich: integer
	@param toWhich: number of last fragment to display in table
	@type toWhich: integer
	@param title: title of the web page
	@type title: string
	@param form: filtering form on the 'Saved sessions' page
	@type form: Django form object
	@param selected: number of fragments displayed in table
	@type selected: integer
	@param info: information text to display on 'Saved sessions' page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	#get specific subset of sessions to present in table:
	results = getDatabaseData(session_list, fromWhich, toWhich)
	return render(request, 'amyload/savedSessions.html', {'dataset': results['data_list'], 'title': title, 'form': form,
	'howMany': results['howMany'], 'selected': selected, 'fromWhich': fromWhich, 'toWhich': toWhich, 'info': info, 'iferror': iferror})

@login_required
def savedSessions(request, info="", iferror=""):
	"""
	Open 'Saved sessions' page.
	
	@param info: information text to display on 'Saved sessions' page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	#get sessions to present in table:
	session_list = Session.objects.filter(user__id=request.user.id, saved=1).order_by('-date')
	return setSavedSessionsPageParams(request, session_list, 1, 25, "AmyLoad - Saved sessions", SortingSavedSessionsForm(), 25, info="", iferror="")

@login_required
def savedFilter(request, info="", iferror=""):
	"""
	Gather data for table in 'Saved sessions' page after clicking the 'Filter' button.
	
	@param info: information text to display on 'Saved sessions' page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get sessions to present in table:
		session_list = sessionMainFilter(data['bywhatsort'], data['ascordesc'], data['date1_day'], data['date1_month'], 
			data['date1_year'], data['date2_day'], data['date2_month'], data['date2_year'], data['ifcontainSession'], request.user)
		#check if the session list is empty:
		if (len(session_list) == 0):
			fromWhich = 0
		else:
			fromWhich = 1
		return setSavedSessionsPageParams(request, session_list, fromWhich, int(data['selected']), "AmyLoad - Saved sessions", SortingSavedSessionsForm(data=request.POST), int(data['selected']), info="", iferror="")
	else:
		return savedSessions(request)

def sessionMainFilter(sByWhatSort, sAscOrDesc, sDay1, sMonth1, sYear1, sDay2, sMonth2, sYear2, sIfContainSession, user):
	"""
	Perform main filtration of saved sessions according to parameters submitted in filtration form on the web page.
	
	@param sByWhatSort: parameter to sort by
	@type sByWhatSort: integer (0 - protein name, 1 - name, 2 - sequence length, 3 - aggregation, 4 - revision, 5 - addition date)
	@param sAscOrDesc: order of sorting data
	@type sAscOrDesc: boolean (1 - descendingly, 0 - ascendingly)
	@param sDay1: day of the start date
	@type sDay1: integer
	@param sMonth1: month of the start date
	@type sMonth1: integer
	@param sYear1: year of the start date
	@type sYear1: integer
	@param sDay2: day of the end date
	@type sDay2: integer
	@param sMonth2: month of the end date
	@type sMonth2: integer
	@param sYear2: year of the end date
	@type sYear2: integer
	@param sIfContainSession: session name substring
	@type sIfContainSession: string
	@param user: logged user
	@type user: Django user object
	@return: list of dictionaries representing objects of Session model
	"""
	#initial parameters:
	params = {'sort': '.order_by(\'-date\')', 'sfiltr': '.filter(', 'efiltr': ')', 'ascdesc': '', 'ifcontainSess': '', 'user': '', 'date': ''}
	#create start date properly formated:
	try:
		if(sDay1 != '0' and sMonth1 != '0' and sYear1 != '0'):
			date1 = datetime.date(int(sYear1), int(sMonth1), int(sDay1)).isoformat()
		else:
			date1 = '2012-01-01'
	except ValueError:
		sDay1 = 1
		sMonth1 = (int(sMonth1)+1)
		sYear1 = int(sYear1)
		if(sMonth1>12):
			sMonth1 = 1
			sYear1 = sYear1 + 1
		date1 = datetime.date(sYear1, sMonth1, sDay1).isoformat()
	#create end date properly formated:
	try:
		if(sDay2 != '0' and sMonth2 != '0' and sYear2 != '0'):
			date2 = datetime.date(int(sYear2), int(sMonth2), int(sDay2)).isoformat()
		else:
			date2 = datetime.date.today().isoformat()
	except ValueError:
		sDay2 = 1
		sMonth2 = (int(sMonth2)+1)
		sYear2 = int(sYear2)
		if(sMonth2>12):
			sMonth2 = 1
			sYear2 = sYear2 + 1
		date2 = datetime.date(sYear2, sMonth2, sDay2).isoformat()
	#create range filtration question:
	params['date'] = 'date__range=[\"' + date1 + '\", \"' + date2 + '\"],'
	#get order of sorting data:
	if (sAscOrDesc == '1'):
		params['sort'] = '.order_by(\'-date\')'
		params['ascdesc'] = '-'
	#get parameter to sort by:
	if (sByWhatSort == '0'):
		params['sort'] = '.order_by(\'' + params['ascdesc'] + 'name\')'
	elif (sByWhatSort == '1'):
		params['sort'] = '.order_by(\'' + params['ascdesc'] + 'date\')'
	
	#get session name substring (Django bug - make new table in MySQL with lowercased protein names):
	if (sIfContainSession != ''):
		sIfContainSession = eliminRegexDestroyers(sIfContainSession)
		params['ifcontainSess'] = 'name__icontains=\'' + sIfContainSession + '\','
	#get user:
	if(user):
		params['user'] = 'user__id=' + str(user.id) + ','
	#get filtered fragments:
	final = 'data_list = Session.objects' + params['sfiltr'] + params['ifcontainSess'] + params['user'] + params['date'] + 'saved=1,' + params['efiltr'] + params['sort']
	exec(final)
	return data_list

@login_required
def savedHowMany(request, info="", iferror=""):
	"""
	Gather data for table after changing number of fragments displayed in table on the 'Saved sessions' page.
	
	@param info: information text to display on 'Saved sessions' page
	@type info: string
	@param iferror: 1 - info is an error (then display it in red in template)
	@type iferror: no specific type
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get sessions to present in table:
		session_list = savedAfterHowMany(data, request.user)
		return setSavedSessionsPageParams(request, session_list, 1, int(data['selected']), "AmyLoad - Saved sessions", SortingSavedSessionsForm(data=request.POST), int(data['selected']), info="", iferror="")
	else:
		return savedSessions(request)

def savedAfterHowMany(data, user):
	"""
	Gather sessions to present in table.
	
	@param data: data gathered after POST from page form
	@type data: dictionary
	@param user: logged user
	@type user: Django user object
	@return: list of dictionaries representing objects of Session model
	"""
	#if h_bywhatsort on page was set then server is asking for sessions after clicking 'Filter' button:
	if(data['h_bywhatsort'] == 'None'):
		#if h_bywhatsort wasn't set then server is asking for sessions on 'Saved sessions' page after nonPOST web enter:
		session_list = savedAfterNonPost(user)
		return session_list
	else:
		#gather data from date fields:
		if(data['h_date1'] != 'None'):
			tmpDate1 = data['h_date1'].split('-')
			year1 = tmpDate1[0]
			month1 = tmpDate1[1]
			day1 = tmpDate1[2]
		else:
			year1 = '0'
			month1 = '0'
			day1 = '0'
		
		if(data['h_date2'] != 'None'):
			tmpDate2 = data['h_date2'].split('-')
			year2 = tmpDate2[0]
			month2 = tmpDate2[1]
			day2 = tmpDate2[2]
		else:
			year2 = '0'
			month2 = '0'
			day2 = '0'
		#filtrate sessions:
		session_list = sessionMainFilter(data['h_bywhatsort'], data['h_ascordesc'], day1, month1, year1, day2, month2, year2, data['h_ifcontainSession'], user)
		return session_list

def savedAfterNonPost(user):
	"""
	Gather sessions to present in tables on 'Saved sessions' page after nonPOST web enter.
	
	@return: list of dictionaries representing objects of Session model
	"""
	session_list = Session.objects.filter(user__id=user.id, saved=1).order_by('-date')
	return session_list

@login_required
def savedPrevious(request):
	"""
	Gather data for table after clicking the 'Previous' link above table on the 'Saved sessions' page.
	
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in table:
		session_list = savedAfterHowMany(data, request.user)
		return setSavedSessionsPageParams(request, session_list, int(data['h_fromWhich']) - int(data['selected']), int(data['h_toWhich']) - int(data['selected']), "AmyLoad - Saved sessions", SortingSavedSessionsForm(data=request.POST), int(data['selected']), info="", iferror="")
	else:
		return savedSessions(request)

@login_required
def savedNext(request):
	"""
	Gather data for table after clicking the 'Next' link above table on the 'Saved sessions' page.
	
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get fragments to present in table:
		session_list = savedAfterHowMany(data, request.user)
		return setSavedSessionsPageParams(request, session_list, int(data['h_fromWhich']) + int(data['selected']), int(data['h_toWhich']) + int(data['selected']), "AmyLoad - Saved sessions", SortingSavedSessionsForm(data=request.POST), int(data['selected']), info="", iferror="")
	else:
		return savedSessions(request)
	return render(request, 'amyload/savedSessions.html', {'title': "AmyLoad - Saved sessions", 'dataset': results['data_list'], 'form': form, 'howMany': results['howMany'], 'selected': selected, 'fromWhich': fromWhich, 'toWhich': toWhich})

@login_required
def loadSaved(request):
	"""
	Load saved sessions after clicking 'Load session' on the 'Saved sessions' page.
	
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get information about clicked radio button (button ID is equal to session ID):
		chosenFrags = data.getlist('radio')
		if(chosenFrags):
			#get chosen session:
			session = Session.objects.filter(id=chosenFrags[0]).last()
			fragments_list = session.fragments.all()
			#load saved session into latest temporary session:
			latestSession = Session.objects.create(user=request.user, date=datetime.date.today().isoformat(), saved=False)
			latestSession.fragments.add(*fragments_list)
			latestSession.save()
			return database(request, ('Session ' + session.name + ' loaded.'), '0')
		else:
			return savedHowMany(request, 'You must choose a session to load.', '1')
	else:
		return savedSessions(request)

@login_required
def removeSaved(request):
	"""
	Remove saved sessions after clicking 'Remove session' on the 'Saved sessions' page.
	
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get information about clicked radio button (button ID is equal to session ID):
		chosenFrags = data.getlist('radio')
		if(chosenFrags):
			#remove session:
			session = Session.objects.filter(id=chosenFrags[0]).last()
			session.delete()
			return savedHowMany(request, ('Session ' + session.name + ' removed.'), '0')
		else:
			return savedHowMany(request, 'You must choose a session to remove.', '1')
	else:
		return savedSessions(request)

@login_required
def newSession(request):
	"""
	Clear temporary session after clicking 'Clear session' on the Database page.
	
	@return: page render
	"""
	if (request.method == 'POST'):
		#clear fragments in the latest not saved user session:
		session = Session.objects.filter(user__id=request.user.id, saved=0).last()
		session.fragments.clear()
		return databaseHowMany(request)
	else:
		return database(request)

def databaseChosenFasta(request):
	"""
	Gather checked fragments in filtering table to create FASTA file after clicking 'Download all in FASTA file' on the unlogged Database page.
	
	@return: HttpResponse
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get all checked fragments:
		chosenFrags = data.getlist('checkbox')
		chosen_list = Fragment.objects.filter(id__in=chosenFrags).all()
		return createFasta(chosen_list, 'unlogged', 'unlogged')
	else:
		return database(request)

def databaseChosenXml(request):
	"""
	Gather checked fragments in filtering table to create XML file after clicking 'Download all in XML file' on the unlogged Database page.
	
	@return: HttpResponse
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get all checked fragments:
		chosenFrags = data.getlist('checkbox')
		chosen_list = Fragment.objects.filter(id__in=chosenFrags).all()
		return createXml(chosen_list, 'unlogged', 'unlogged')
	else:
		return database(request)

def databaseChosenSsv(request):
	"""
	Gather checked fragments in filtering table to create SSV file after clicking 'Download all in SSV file' on the unlogged Database page.
	
	@return: HttpResponse
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get all checked fragments:
		chosenFrags = data.getlist('checkbox')
		chosen_list = Fragment.objects.filter(id__in=chosenFrags).all()
		return createSsv(chosen_list, 'unlogged', 'unlogged')
	else:
		return database(request)

def databaseChosenCsv(request):
	"""
	Gather checked fragments in filtering table to create CSV file after clicking 'Download in CSV file' on the unlogged Database page.
	
	@return: HttpResponse
	"""
	if (request.method == 'POST'):
		data = request.POST
		#get all checked fragments:
		chosenFrags = data.getlist('checkbox')
		chosen_list = Fragment.objects.filter(id__in=chosenFrags).all()
		return createCsv(chosen_list, 'unlogged', 'unlogged')
	else:
		return database(request)

def analysisPage(request):
	"""
	Start the Analysis page.
	
	@return: page render
	"""
	form = AnalysisForm()
	return render(request, 'amyload/analysis.html', {'title': "AmyLoad - Analysis", 'form': form})

def performAnalysis(request):
	"""
	Start the analysis of the given FASTA sequences in the parallel process.
	
	@return: page render
	"""
	info = ""
	iferror = ""
	if (request.method == 'POST'):
		data=request.POST
		predictors = {}
		#get user's email:
		if(request.user.is_authenticated()):
			email_addr = request.user.email
		else:
			if(data['email']):
				email_addr = data['email']
			else:
				email_addr = ""
				info = "You must input your email address."
				iferror = "1"
		#check which methods were used to run:
		if(email_addr):
			if(data.getlist('foldamyloid') and data.getlist('foldamyloid_options')):
				predictors['FoldAmyloid'] = True
			else:
				predictors['FoldAmyloid'] = False
			
			if(data.getlist('aggrescan')):
				predictors['Aggrescan'] = True
			else:
				predictors['Aggrescan'] = False
			
			if(data.getlist('inclusion')):
				predictors['Inclusion'] = True
			else:
				predictors['Inclusion'] = False
			
			if(data.getlist('fish')):
				predictors['Fish'] = True
			else:
				predictors['Fish'] = False
			
			if(request.FILES["file"] and (predictors['FoldAmyloid'] or predictors['Aggrescan'] or predictors['Inclusion'] or predictors['Fish'])):
				#create temporary file with uploaded file's data:
				fp = tempfile.TemporaryFile()
				fp.write(request.FILES["file"].read())
				fp.seek(0)
				#run the analysis in parallel process:
				th = Thread(target=backgroundAnalysis, args=(data, fp, predictors, email_addr,))
				th.setDaemon(True)
				th.start()
				info = "Email with the results will be sent to " + email_addr + " after the analysis."
				iferror = "0"
			else:
				info = "You must upload a FASTA file and choose at least one analysis method with at least one option if available."
				iferror = "1"
		form = AnalysisForm(request.POST)
	else:
		form = AnalysisForm()
	return render(request, 'amyload/analysis.html', {'title': "AmyLoad - Analysis", 'form': form, 'info': info, 'iferror': iferror})

def backgroundAnalysis(data, fastaFile, predictors, email_addr):
	"""
	Start the parallel analysis process.
	
	@param data: data from the POST of Analysis page's form
	@type data: dictionary
	@param fastaFile: FASTA file name
	@type fastaFile: string
	@param predictors: information about methods to run
	@type predictors: dictionary
	@param email_addr: user email address
	@type email_addr: string
	"""
	#gather all options checked with FoldAmyloid:
	foldAmyloidOptions = getFoldAmyloidOptions(data)
	#gather all options checked with FiSH:
	fishOptions = getFishOptions(data)
	#run main calulations:
	calculateResults(fastaFile, predictors, foldAmyloidOptions, fishOptions, email_addr)
	sys.exit(1)

def calculateResults(fastaFile, predictors, foldAmyloidOptions, fishOptions, email_addr):
	"""
	Run all checked methods one by one.
	
	@param fastaFile: FASTA file name
	@type fastaFile: string
	@param predictors: information about methods to run
	@type predictors: dictionary
	@param foldAmyloidOptions: options checked with FoldAmyloid
	@type foldAmyloidOptions: list of dictionaries
	@param fishOptions: options checked with FiSH
	@type fishOptions: list of dictionaries
	@param email_addr: user email address
	@type email_addr: string
	"""
	foldAmyRes = {}
	#parse FASTA file:
	sequences = SeqIO.parse(fastaFile, "fasta")
	email = "(" + datetime.datetime.now().isoformat() + ") AmyLoad - analysis results\n"
	email = email + "\n"
	for s in sequences:
		email = email + "\n\n=======================================================================\n"
		email = email + "RESULTS FOR:\n"
		email = email + ">" + str(s.id) + "\n"
		email = email + str(s.seq) + "\n"
		email = email + "\n"
		#run FoldAmyloid if checked:
		if(predictors['FoldAmyloid']):
			email = email + "FoldAmyloid:" + "\n"
			for scale in foldAmyloidOptions.keys():
				email = email + "Options: Scale = " + getScaleName(scale) + ", Averaging frame = " + str(getFrameFoldAmyloid(foldAmyloidOptions[scale])) + ", Threshold = " + str(getThresholdFoldAmyloid(foldAmyloidOptions[scale])) + "\n"
				email = email + calcFoldAmyloid(scale, foldAmyloidOptions[scale], s.seq) + "\n"
				email = email + "\n"
		#run AGGRESCAN if checked:
		if(predictors['Aggrescan']):
			email = email + "Aggrescan:" + "\n"
			email = email + "Options: Averaging frame = " + str(getFrameAggrescan(s.seq)) + ", Threshold = -0.02\n"
			email = email + calcAggrescan(s.seq) + "\n"
			email = email + "\n"
		#run FiSH if checked:
		if(predictors['Fish']):
			email = email + "FiSH:" + "\n"
			for pred in fishOptions.keys():
				email = email + "Options: Version = " + str(getPredNameFish(fishOptions[pred])) + ", Threshold = " + str(getThresholdFish(fishOptions[pred])) + "\n"
				email = email + calcFish(fishOptions[pred], s.seq) + "\n"
				email = email + "\n"
		#run comparison with database if checked:
		if(predictors['Inclusion']):
			email = email + "AmyLoad database:" + "\n"
			email = email + "Options: 0 - fragment nonamyloidogenic; 1 - fragment amyloidogenic; # - no fragment from AmyLoad\n"
			email = email + calcInclusion(s.seq)
			email = email + "\n"
	email = email + "\n\n=======================================================================\n"
	email = email + "\n\n\n(If there is no results for any of your chosen options, then probably your FASTA file is not proper. If it happens please see the example FASTA file on the Analysis page.)\n"
	send_mail('AmyLoad - analysis results', email, 'comprec.pwr@gmail.com', [email_addr], fail_silently=False)
	fastaFile.close()
	return

def calcFish(options, sequence):
	"""
	Run the FiSH method according to http://www.ncbi.nlm.nih.gov/pubmed/24564523/.
	
	@param options: parameters for the one of the options checked with FiSH
	@type options: dictionary
	@param seq: FASTA sequence
	@type seq: string
	@return: string with results of FiSH
	"""
	
	randCode = uuid.uuid4()
	if(len(sequence) < 10):
		lower = int(math.ceil((len(sequence)/3)))
		upper = int(math.ceil((len(sequence)/3)*2))
	else:
		lower = 5
		upper = len(sequence)-5
	outputName = sequence[1:lower] + "-" + str(randCode) + "-" + sequence[upper:]
	sourceDir = finders.find("comprec/FISH_src/")
	outputDir = finders.find("comprec/FISH_tmp/")
	outputName = str(outputDir + "/" + outputName)
	os.chdir(sourceDir)
	command = ["octave","fish.m",str(sequence),options['threshold'],options['pred'],outputName]
	process = subprocess.Popen(command).wait()
	result = ""
	with open(outputName) as fp:
		for line in fp:
			tmp = line.split()
			result = result + tmp[2]
	os.remove(outputName)
	return result

def calcInclusion(seq):
	"""
	Run the comparison of input FASTA sequence with fragments in database.
	
	@param seq: FASTA sequence
	@type seq: string
	@return: string with results of fragments-sequence comparison
	"""
	#get all fragments in the database:
	seqDB = Fragment.objects.filter(reviewed=1).values_list('protein__name', 'name', 'sequence', 'ifaggregate')
	result = ""
	#for each fragment:
	for sDB in seqDB:
		#check if fragment occurs in the sequence at least once:
		if(sDB[2] in seq):
			#create a headline:
			headline = "Protein: " + sDB[0] + "; Name: " + sDB[1] + "; Sequence: " + sDB[2] + "; Fibrilation: " + str(sDB[3]) + "\n"
			#get all fragments occurrences:
			res = [m.start() for m in re.finditer(str(sDB[2]), str(seq))]
			resultTmp = ""
			#for each occurrence:
			for start in res:
				#where there is no fragment write '#':
				resultTmp = resultTmp + ("#" * (start-len(resultTmp)))
				if(sDB[3] == True):
					#where there is a fibrilating fragment write '1':
					resultTmp = resultTmp + ("1" * len(sDB[2]))
				else:
					#where there is a non-fibrilating fragment write '0':
					resultTmp = resultTmp + ("0" * len(sDB[2]))
			#from last fragment to the end write '#':
			resultTmp = resultTmp + ("#" * (len(seq)-len(resultTmp))) + "\n"
			result = result + headline + resultTmp
	if(not result):
		result = result + "No fragments from AmyLoad in any query sequence."
	return result

def calcAggrescan(seq):
	"""
	Run the AGGRESCAN method according to http://www.ncbi.nlm.nih.gov/pubmed/17324296/.
	
	@param seq: FASTA sequence
	@type seq: string
	@return: string with results of AGGRESCAN
	"""
	#all sequence symbols (# - first additional aminoacid, % - last additional amino acid):
	amino = ['#', 'I', 'F', 'V', 'L', 'Y', 'W', 'M', 'C', 'A', 'T', 'S', 'P', 'G', 'K', 'H', 'Q', 'R', 'N', 'E', 'D', '%']
	#check if sequence consists of only proper symbols:
	iferror = aggrescanSeqCheck(amino, str(seq))
	if(not iferror):
		#values for each sequence symbol:
		values = [-1.0855, 1.822, 1.754, 1.594, 1.38, 1.159, 1.037, 0.91, 0.604, -0.036, -0.159, -0.294, -0.334, -0.535, -0.931, -1.033, -1.231, -1.24, -1.302, -1.412, -1.836, -1.624]
		#get frame length:
		frame = getFrameAggrescan(seq)
		hotspot = 5
		threshold = -0.02
		#add additional sequence symbols:
		seq = "#" + seq + "%"
		
		###results = []
		amyloFrags = ""
		frameHalf = frame/2
		length = len(seq)
		amylCount = 0
		#calculate first average value of sequence symbols:
		first = calcAverage(seq[0:frame], amino, values)
		#analyze sequence symbols from the beginning to the half of frame:
		for r in range(0, frameHalf):
			###results.append(first)
			if(first > threshold and seq[frameHalf] != 'P'):
				#if average higher than threshold and sequence symbol is not proline then it is pro-aggregation:
				amylCount = amylCount + 1
			else:
				#if not then check if there was already more '1' than the hotspot length and write it properly:
				if(amylCount >= hotspot):
					for r in range(0,amylCount):
						amyloFrags = amyloFrags + "1"
					amyloFrags = amyloFrags + "0"
				else:
					for r in range(0,amylCount+1):
						amyloFrags = amyloFrags + "0"
				amylCount = 0
		#analyze sequence symbols from the half of frame to the 'sequence_length - half_of_the_frame':
		for i, a in enumerate(seq):
			if(i>frameHalf and frameHalf<(length-i-1)):
				#calculate average value of sequence symbols in the frame and assign it to central one:
				tmp = calcAverage(seq[(i-frameHalf):(i+frameHalf+1)], amino, values)
				###results.append(tmp)
				if(tmp > threshold and seq[i] != 'P'):
					#if average higher than threshold and sequence symbol is not proline then it is pro-aggregation:
					amylCount = amylCount + 1
				else:
					#if not then check if there was already more '1' than the hotspot length and write it properly:
					if(amylCount >= hotspot):
						for r in range(0,amylCount):
							amyloFrags = amyloFrags + "1"
						amyloFrags = amyloFrags + "0"
					else:
						for r in range(0,amylCount+1):
							amyloFrags = amyloFrags + "0"
					amylCount = 0
		#calculate last average value of sequence symbols:
		last = calcAverage(seq[(length-frame+1):length], amino, values)
		#analyze sequence symbols from the 'sequence_length - half_of_the_frame' to the end of sequence:
		for r in range(0, frameHalf):
			###results.append(last)
			if(last > threshold and seq[length-frameHalf] != 'P'):
				#if average higher than threshold and sequence symbol is not proline then it is pro-aggregation:
				amylCount = amylCount + 1
			else:
				#if not then check if there was already more '1' than the hotspot length and write it properly:
				if(amylCount >= hotspot):
					for r in range(0,amylCount):
						amyloFrags = amyloFrags + "1"
					amyloFrags = amyloFrags + "0"
				else:
					for r in range(0,amylCount+1):
						amyloFrags = amyloFrags + "0"
				amylCount = 0
		#deal with the last not assigned symbols when there is the last symbol:
		if(amylCount >= hotspot):
			for r in range(0,amylCount):
				amyloFrags = amyloFrags + "1"
		else:
			for r in range(0,amylCount):
				amyloFrags = amyloFrags + "0"
		
		return amyloFrags
	else:
		return "Error - some characters in your sequence are not allowed: " + iferror

def aggrescanSeqCheck(amino, seq):
	"""
	Check if sequence consists of only proper symbols.
	
	@param amino: possible sequence symbols
	@type amino: char[]
	@param seq: FASTA sequence
	@type seq: string
	@return: sequence string
	"""
	length = len(amino)
	#delete all proper symbols in FASTA sequence - if the sequence is empty at the end, then it is ok:
	for i, a in enumerate(amino):
		if(i>0 or i<length-1):
			seq = seq.replace(a,'')
			seq = seq.replace(a.lower(),'')
	return seq

def getFrameAggrescan(seq):
	"""
	Find what is a proper frame for the sequence according to AGGRESCAN algorithm.
	
	@param seq: FASTA sequence
	@type seq: string
	@return: AGGRESCAN integer frame
	"""
	if(len(seq) <= 75):
		frame = 5
	elif(len(seq) <= 175):
		frame = 7
	elif(len(seq) <= 300):
		frame = 9
	else:
		frame = 11
	return frame

def getFrameFoldAmyloid(options):
	"""
	Find what is a proper frame for the sequence according to option chosen in FoldAmyloid algorithm.
	
	@param options: parameters for the one of the options checked with FoldAmyloid
	@type options: dictionary
	@return: FoldAmyloid integer frame
	"""
	if(options['frame']):
		frame = int(options['frame'])
		#frame cannot be negative or even:
		if(frame<0):
			frame = frame * -1
		if(not frame%2):
			frame = frame + 1
	else:
		frame = 1
	return frame

def getPredNameFish(options):
	"""
	Find what is a proper name of the prediction version for the sequence according to option chosen in FiSH algorithm.
	
	@param options: parameters for the one of the options checked with FiSH
	@type options: dictionary
	@return: FiSH string name of the prediction version
	"""
	if(options['pred'] == '0'):
		name = 'Standard'
	elif(options['pred'] == '1'):
		name = 'Full'
	return name

def getThresholdFish(options):
	"""
	Find what is a proper threshold for the sequence according to option chosen in FiSH algorithm.
	
	@param options: parameters for the one of the options checked with FiSH
	@type options: dictionary
	@return: FiSH float threshold
	"""
	if(options['threshold']):
		threshold = float(options['threshold'])
	else:
		threshold = 0
	return threshold

def getThresholdFoldAmyloid(options):
	"""
	Find what is a proper threshold for the sequence according to option chosen in FoldAmyloid algorithm.
	
	@param options: parameters for the one of the options checked with FoldAmyloid
	@type options: dictionary
	@return: FoldAmyloid integer threshold
	"""
	if(options['threshold']):
		threshold = float(options['threshold'])
	else:
		threshold = 0
	return threshold

def calcFoldAmyloid(scale, options, seq):
	"""
	Run the FoldAmyloid method according to http://www.ncbi.nlm.nih.gov/pubmed/20019059/.
	
	@param scale: name of the scale (option) chosen with FoldAmyloid
	@type scale: string
	@param options: parameters for the one of the options checked with FoldAmyloid
	@type options: dictionary
	@param seq: FASTA sequence
	@type seq: string
	@return: string with results of FoldAmyloid
	"""
	#all sequence symbols (amino acid symbols):
	amino = ['A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y', 'X']
	#get values for each sequence symbol according to the chosen FoldAmyloid option:
	values = getFoldAmyloidValues(scale)
	frame = getFrameFoldAmyloid(options)
	threshold = getThresholdFoldAmyloid(options)
	
	amyloFrags = ""
	frameHalf = frame/2
	length = len(seq)
	amylCount = 0
	for i, a in enumerate(seq):
		if(i<frameHalf):
			#analyze sequence symbols from the beginning to the half of frame:
			tmp = calcAverage(seq[0:(i+frameHalf+1)], amino, values)
		elif(length-i-1<frameHalf):
			#analyze sequence symbols from the 'sequence_length - half_of_the_frame' to the end of sequence:
			tmp = calcAverage(seq[(i-frameHalf):length], amino, values)
		else:
			#analyze sequence symbols from the half of frame to the 'sequence_length - half_of_the_frame':
			tmp = calcAverage(seq[(i-frameHalf):(i+frameHalf+1)], amino, values)
		
		if(tmp > threshold):
			#if average higher than threshold then it is pro-aggregation:
			amylCount = amylCount + 1
		else:
			#if not then check if there was already more '1' than the frame length and write it properly:
			if(amylCount >= frame):
				for r in range(0,amylCount):
					amyloFrags = amyloFrags + "1"
				amyloFrags = amyloFrags + "0"
			else:
				for r in range(0,amylCount+1):
					amyloFrags = amyloFrags + "0"
			amylCount = 0
	#deal with the last not assigned symbols when there is the last symbol:
	if(amylCount >= frame):
		for r in range(0,amylCount):
			amyloFrags = amyloFrags + "1"
	else:
		for r in range(0,amylCount):
			amyloFrags = amyloFrags + "0"
	
	return amyloFrags

def calcAverage(seqFrag, amino, values):
	"""
	Calculates average value of amino acids in a frame.
	
	@param seqFrag: amino acids' frame
	@type seqFrag: string
	@param amino: sequence symbols
	@type amino: char[]
	@param values: values for sequence symbols
	@type values: double[]
	@return: average value of amino acids in a frame (double)
	"""
	length = len(seqFrag)
	suma = 0
	for s in seqFrag:
		suma = suma + values[getValForAA(amino, s)]
	return suma/length

def getValForAA(amino, AA):
	"""
	Get symbol position in the list of sequence symbols.
	
	@param amino: sequence symbols
	@type amino: char[]
	@param AA: one symbol
	@type AA: char
	@return: symbol position in the list of sequence symbols (integer)
	"""
	for i, a in enumerate(amino):
		#comparison is case insensitive:
		if(a == AA.lower() or a == AA.upper()):
			return i
	return 20

def getFoldAmyloidValues(scale):
	"""
	Get values for sequence symbols for chosen FoldAmyloid scale.
	
	@param scale: name of the scale (option) chosen with FoldAmyloid
	@type scale: string
	@return: list of values for FolAmyloid sequence symbols
	"""
	values = []
	if(scale == '1'):
		values = [19.89, 23.52, 17.41, 17.46, 27.18, 17.11, 21.72, 25.71, 17.67, 25.36, 24.82, 18.49, 17.43, 19.23, 21.03, 18.19, 19.81, 23.93, 28.48, 25.93, 20.73]
	elif(scale == '2'):
		values = [0.726, 0.769, 0.511, 0.574, 0.764, 0.619, 0.662, 0.834, 0.662, 0.836, 0.77, 0.587, 0, 0.675, 0.698, 0.56, 0.597, 0.798, 0.719, 0.749, 0.656]
	elif(scale == '3'):
		values = [-0.021, 0.581, -0.948, -0.764, 1.053, -0.687, 0.044, 1.052, -0.493, 1.011, 0.756, -0.592, -2.36, -0.25, 0.052, -0.708, -0.389, 0.715, 1.102, 0.846, 0]
	elif(scale == '4'):
		values = [0.684, 0.705, 0.623, 0.629, 0.724, 0.502, 0.658, 0.788, 0.6, 0.745, 0.715, 0.58, 0.477, 0.633, 0.659, 0.595, 0.637, 0.77, 0.733, 0.731, 0.659]
	elif(scale == '5'):
		values = [0.086, 0.568, -0.776, -0.632, 0.958, -1.088, 0.025, 1.217, -0.565, 1.015, 0.725, -0.713, -2.303, -0.271, 0.032, -0.73, -0.349, 0.92, 1.027, 0.851, 0]
	return values

def getFishOptions(data):
	"""
	Get pred and threshold parameters for each chosen FiSH option.
	
	@param data: data from the POST of Analysis page's form
	@type data: dictionary
	@return: dictionary of parameters for the options checked with FiSH
	"""
	options = data.getlist('fish_options')
	options_dict = {}
	for p in options:
		options_dict[p] = getPredVersionParams(data, p)
	return options_dict

def getPredVersionParams(data, pred):
	"""
	Get pred and threshold parameters for one FiSH option.
	
	@param data: data from the POST of Analysis page's form
	@type data: dictionary
	@param pred: name of the pred (option) chosen with FiSH
	@type pred: string
	@return: dictionary of parameters for the one of the options checked with FiSH
	"""
	params = {}
	if(pred == '0'):
		params = {'pred': pred, 'threshold': data['fish_threshold_1']}
	elif(pred == '1'):
		params = {'pred': pred, 'threshold': data['fish_threshold_2']}
	return params

def getFoldAmyloidOptions(data):
	"""
	Get frame and threshold parameters for each chosen FoldAmyloid option.
	
	@param data: data from the POST of Analysis page's form
	@type data: dictionary
	@return: dictionary of parameters for the options checked with FoldAmyloid
	"""
	options = data.getlist('foldamyloid_options')
	options_dict = {}
	for p in options:
		options_dict[p] = getScaleParams(data, p)
	return options_dict

def getScaleParams(data, scale):
	"""
	Get frame and threshold parameters for one FoldAmyloid option.
	
	@param data: data from the POST of Analysis page's form
	@type data: dictionary
	@param scale: name of the scale (option) chosen with FoldAmyloid
	@type scale: string
	@return: dictionary of parameters for the one of the options checked with FoldAmyloid
	"""
	params = {}
	if(scale == '1'):
		params = {'frame': data['foldamyloid_frame_1'], 'threshold': data['foldamyloid_threshold_1']}
	elif(scale == '2'):
		params = {'frame': data['foldamyloid_frame_2'], 'threshold': data['foldamyloid_threshold_2']}
	elif(scale == '3'):
		params = {'frame': data['foldamyloid_frame_3'], 'threshold': data['foldamyloid_threshold_3']}
	elif(scale == '4'):
		params = {'frame': data['foldamyloid_frame_4'], 'threshold': data['foldamyloid_threshold_4']}
	elif(scale == '5'):
		params = {'frame': data['foldamyloid_frame_5'], 'threshold': data['foldamyloid_threshold_5']}
	return params

def getScaleName(option):
	"""
	Get scale name for one FoldAmyloid option.
	
	@param option: option chosen by user for FoldAmyloid
	@type option: string
	@return: scale for the one of the options checked with FoldAmyloid
	"""
	if(option == '1'):
		return "Expected number of contacts 8A"
	elif(option == '2'):
		return "Bone-bone donors"
	elif(option == '3'):
		return "Hybrid (contacts + donors)"
	elif(option == '4'):
		return "Bone-bone acceptors"
	elif(option == '5'):
		return "Triple hybrid (contacts + donors + acceptors)"
	return ""

def help(request):
	"""
	Open Help page.
	
	@return: page render
	"""
	return render(request, 'amyload/help.html', {'title': "AmyLoad - Help"})

def downloadXML1(request):
	"""
	Set downloadable XML file at Help page to 'input.xml' file.
	
	@return: HttpResponse
	"""
	download_name ="amyload/input.xml"
	return downloadFile(download_name)

def downloadXML2(request):
	"""
	Set downloadable XML file at Help page to 'download.xml' file.
	
	@return: HttpResponse
	"""
	download_name ="amyload/download.xml"
	return downloadFile(download_name)

def downloadFasta(request):
        """
	Set downloadable FASTA file at Analysis page to 'test.fasta' file.

        @return: HttpResponse
        """
	download_name ="amyload/test.fasta"
	return downloadFile(download_name)

def downloadFile(download_name):
	"""
	Get proper static XML file to download on Help page.
	
	@param download_name: name of the file to download
	@type download_name: string
	@return: HttpResponse
	"""
	
	filename = finders.find(download_name)
	wrapper = FileWrapper(open(filename))
	content_type = mimetypes.guess_type(filename)[0]
	response = HttpResponse(wrapper,content_type=content_type)
	response['Content-Length'] = os.path.getsize(filename)    
	response['Content-Disposition'] = "attachment; filename=%s"%download_name
	return response

@login_required
def sendComment(request, fragId="-1"):
	"""
	Sent comment from user to admin about a chosen fragment.
	
	@param fragId: fragment database id
	@type fragId: integer
	@return: page render
	"""
	if (request.method == 'POST'):
		data = request.POST
		fragment = Fragment.objects.filter(id=fragId).all()[0]
		email_subject = 'Comment from user: ' + request.user.username + ' about fragment no. ' + str(fragment.amyindex)
		email_body = data['comment']
		if (email_body):
			email_body = email_body + "\n"
			email_body = email_body + "User email: " + request.user.email
			send_mail(email_subject, email_body, 'comprec.pwr@gmail.com', ['comprec.pwr@gmail.com'], fail_silently=False)
			info = "Comments have been sent. Thank you."
			iferror = "0"
		else:
			info = "You cannot send empty comment."
			iferror = "1"
		return fragmentInfo(request, fragId, info, iferror)	
	else:
		return render(request, 'amyload/home.html', {'title': "AmyLoad - Home"})

#mozliwosc wykonywania przez uzytkownika jednego procesu na raz
#relacja admin-user: usuniecie fragmentu i mail do uzytkownika z XMLem i informacjami co zmienic

#amypdb?
#kasowanie uzystkownika - kasowanie wszystkiego?
