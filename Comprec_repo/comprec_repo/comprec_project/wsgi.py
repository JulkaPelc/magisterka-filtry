import os
import sys
import site

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('/var/www/comprec/comprec-env/lib/python2.7/site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append('/var/www/comprec/comprec_project')
sys.path.append('/var/www/comprec/comprec_project/comprec_project')

#os.environ['DJANGO_SETTINGS_MODULE'] = 'settings.py'
os.environ['DJANGO_SETTINGS_MODULE'] = 'comprec_project.settings'

# Activate your virtual env
#activate_env=os.path.expanduser("~/.virtualenvs/myprojectenv/bin/activate_this.py")
#execfile(activate_env, dict(__file__=activate_env))

#import django.core.handlers.wsgi
#application = django.core.handlers.wsgi.WSGIHandler()
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
