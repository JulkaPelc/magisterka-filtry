function b=baza_zmiana(baza)
n=size(baza,1);

b=cell(1);
for k=1:n
    f=char(baza(k,1));
    b1=size(f);
    a=[];
    for t=1:b1(2)
        if f(t)=='A'
           a(t)=1;
        elseif f(t)=='C' 
           a(t)=2;
        elseif f(t)=='D'
            a(t)=3;
        elseif f(t)=='E' 
           a(t)=4;
        elseif f(t)=='F'
            a(t)=5;
        elseif f(t)=='G' 
           a(t)=6;
        elseif f(t)=='H'
            a(t)=7;
        elseif f(t)=='I' 
           a(t)=8;
        elseif f(t)=='K'
            a(t)=9;
        elseif f(t)=='L' 
           a(t)=10;
        elseif f(t)=='M'
            a(t)=11;
        elseif f(t)=='N' 
           a(t)=12;
        elseif f(t)=='P'
            a(t)=13;
        elseif f(t)=='Q' 
           a(t)=14;
        elseif f(t)=='R'
            a(t)=15;
        elseif f(t)=='S' 
           a(t)=16;
        elseif f(t)=='T'
            a(t)=17;
        elseif f(t)=='V' 
           a(t)=18;
        elseif f(t)=='W'
            a(t)=19;
        elseif f(t)=='Y' 
           a(t)=20;
        end
    end
    b(k,1)={a};
   % b(k,2)=baza(k,2);
end