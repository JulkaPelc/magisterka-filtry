function j = fishfull(name,ciag,ciag2,tr)

okno=5;
load macierzfull;
wsp=macierz;

%format compact

fprintf('results of ');
fprintf('%s\n',name);

s=size(ciag,2);
for t=1:s-okno+1
	CC{t,1}=[ciag(t:(t+okno-1))];
end
BB={CC};

		
[j,m]=baza_sprawdzanie(wsp,okno,BB,s-okno+1);
d=size(CC,1);
k=0;
n=1;
for n=1:d
	if j(n,1,1,1)>=tr
		k=k+1;
		L(n,1)=1;
	else
	L(n,1)=0; 
	end
end

lll=zeros(d+okno,1);
for n=1:d
    if L(n,1)==1
        lll([n:(n+okno-1)],1)=[1;1;1;1;1];
    end
end

fprintf('%s\n','<pre><tabele>nr	Seq.	prog.	val.');

for n=1:d
	if lll(n)==1
	fprintf('%s%i \t %s \t %s%i%s \t %6.3f%s\n','<b><font color=red>',n,ciag2(n),'',lll(n),'',j(n,1,1,1),'</b></font>');
	else	
	fprintf('%s%i \t %s \t %s%i%s \t %6.3f%s\n','',n,ciag2(n),'',lll(n),'',j(n,1,1,1),'');;
	end
end
for n=d+1:s
	if lll(n)==1
	fprintf('%s%i \t %s \t %s%i%s \t %s\n',' <b><font color=red>',n,ciag2(n),'',lll(n),'','</b></font>');
	else	
	fprintf('%s%i \t %s \t %s%i%s \t %s\n','',n,ciag2(n),'',lll(n),'','');;
	end
end




fprintf('</pre></tabele>\n<tr><td><H2>Thanks</H2></td></tr>\n');
