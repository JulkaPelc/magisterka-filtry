function [wy,m]=baza_sprawdzanie2(wsp,baza,okno,nn)

b=zeros(20*(okno-1),(okno-1)*20);
x=size(baza);
wy=zeros(nn,1);
m=zeros(nn,1);
for t1=1:x(1)
    y=size(baza{t1,1});
    wy(t1,1)=-10;
    for t2=1:(y(2)-okno+1)
        [wys,n]=baza_jedna(baza{t1,1}(t2:(t2+okno-1)),b,0);
        wys=sum(sum((wys.*wsp)));
        if wys>wy(t1,1)
            wy(t1,1)=wys;
            m(t1,1)=t2;
        end
    end
end

