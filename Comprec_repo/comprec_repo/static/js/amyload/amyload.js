function CheckAlert(text) {
	if(document.getElementById("analyze").disabled == true)
	{
		alert(text);
	}
	else
	{
		document.getElementById("analyze").click();
	}
}

function All(source) {
	checkboxes = document.getElementsByName('checkbox');
	for(var i=0, n=checkboxes.length;i<n;i++) {
		checkboxes[i].checked = true;
	}
	
	if(document.getElementById("move"))
	{
		document.getElementById("move").disabled = false;
	}
	if(document.getElementById("toFasta"))
	{
		document.getElementById("toFasta").disabled = false;
	}
	if(document.getElementById("toXml"))
	{
		document.getElementById("toXml").disabled = false;
	}
	if(document.getElementById("toSsv"))
	{
		document.getElementById("toSsv").disabled = false;
	}
	if(document.getElementById("toCsv"))
	{
		document.getElementById("toCsv").disabled = false;
	}
}

function None(source) {
	checkboxes = document.getElementsByName('checkbox');
	for(var i=0, n=checkboxes.length;i<n;i++) {
		checkboxes[i].checked = false;
	}
	if(document.getElementById("move"))
	{
		document.getElementById("move").disabled = true;
	}
	if(document.getElementById("toFasta"))
	{
		document.getElementById("toFasta").disabled = true;
	}
	if(document.getElementById("toXml"))
	{
		document.getElementById("toXml").disabled = true;
	}
	if(document.getElementById("toSsv"))
	{
		document.getElementById("toSsv").disabled = true;
	}
	if(document.getElementById("toCsv"))
	{
		document.getElementById("toCsv").disabled = true;
	}
}

function AllSecond(source) {
	checkboxes = document.getElementsByName('checkbox2');
	for(var i=0, n=checkboxes.length;i<n;i++) {
		checkboxes[i].checked = true;
	}
	document.getElementById("remove").disabled = false;
}

function NoneSecond(source) {
	checkboxes = document.getElementsByName('checkbox2');
	for(var i=0, n=checkboxes.length;i<n;i++) {
		checkboxes[i].checked = false;
	}
	document.getElementById("remove").disabled = true;
}

function SetURL(form, nmb){
  document[form].action = nmb;
  return true;
}

function checkEnableRemove(){
	var inputs = document.getElementsByName('checkbox2');
	var is_checked = false;
	for(var x = 0; x < inputs.length; x++)
	{
		if(inputs[x].type == 'checkbox' && inputs[x].name == 'checkbox2')
		{
			is_checked = inputs[x].checked;
			if(is_checked)
			{
				document.getElementById("remove").disabled = false;
				return;
			}
		}
	}
	document.getElementById("remove").disabled = true;
}

function checkEnableMove(){
	var inputs = document.getElementsByName('checkbox');
	var is_checked = false;
	for(var x = 0; x < inputs.length; x++)
	{
		if(inputs[x].type == 'checkbox' && inputs[x].name == 'checkbox')
		{
			is_checked = inputs[x].checked;
			if(is_checked)
			{
				document.getElementById("move").disabled = false;
				return;
			}
		}
	}
	document.getElementById("move").disabled = true;
}

function checkEnableFile(){
	var inputs = document.getElementsByName('checkbox');
	var is_checked = false;
	for(var x = 0; x < inputs.length; x++)
	{
		if(inputs[x].type == 'checkbox' && inputs[x].name == 'checkbox')
		{
			is_checked = inputs[x].checked;
			if(is_checked)
			{
				document.getElementById("toFasta").disabled = false;
				document.getElementById("toXml").disabled = false;
				document.getElementById("toSsv").disabled = false;
				document.getElementById("toCsv").disabled = false;
				return;
			}
		}
	}
	document.getElementById("toFasta").disabled = true;
	document.getElementById("toXml").disabled = true;
	document.getElementById("toSsv").disabled = true;
	document.getElementById("toCsv").disabled = true;
}

function checkEnableSave() {
	if (sorting_form.saveName.value !== "") 
		{document.getElementById("save_session").disabled = false;}
	else
		{document.getElementById("save_session").disabled = true;}
}

function checkEnableLoad(){
	var inputs = document.getElementsByName('radio');
	var is_checked = false;
	for(var x = 0; x < inputs.length; x++)
	{
		if(inputs[x].type == 'radio' && inputs[x].name == 'radio')
		{
			is_checked = inputs[x].checked;
			if(is_checked)
			{
				document.getElementById("load").disabled = false;
				document.getElementById("remove").disabled = false;
				return;
			}
		}
	}
	document.getElementById("load").disabled = true;
	document.getElementById("remove").disabled = true;
}

function enableTd(className,idName){
	var inputs=document.getElementsByClassName(className);
	var id;
	var tmp;
	for(var i=0; i<inputs.length; ++i)
	{
		id = inputs[i].id;
		tmp = document.getElementById(id);
		if(document.getElementById(idName).checked)
		{
			tmp.style.display = 'table-row';
		}
		else
		{
			tmp.style.display = 'none';
		}
	}
}

function checkEnableAnalyze() {
	if(document.getElementById("id_file").value !== "" && (!document.getElementById("id_email") || document.getElementById("id_email").value !== ""))
	{
		var inclusion;
		if(document.getElementById('id_inclusion').checked)
		{
			inclusion = true;
		}
		else
		{
			inclusion = false;
		}
		
		var aggrescan;
		if(document.getElementById('id_aggrescan').checked)
		{
			aggrescan = true;
		}
		else
		{
			aggrescan = false;
		}
		
		var foldamyloid = false;
		var foldamyloidTD = false;
		if(document.getElementById('id_foldamyloid').checked)
		{
			foldamyloid = true;
			var inputs = document.getElementsByName('foldamyloid_options');
			var is_checked = false;
			for(var x = 0; x < inputs.length; x++)
			{
				if(inputs[x].type == 'checkbox')
				{
					is_checked = inputs[x].checked;
					if(is_checked)
					{
						foldamyloidTD = true;
						break;
					}
				}
			}
		}
		
		var fish = false;
		var fishTD = false;
		if(document.getElementById('id_fish').checked)
		{
			fish = true;
			var inputs = document.getElementsByName('fish_options');
			var is_checked = false;
			for(var x = 0; x < inputs.length; x++)
			{
				if(inputs[x].type == 'checkbox')
				{
					is_checked = inputs[x].checked;
					if(is_checked)
					{
						fishTD = true;
						break;
					}
				}
			}
		}
		
		if((aggrescan || (foldamyloidTD && foldamyloid) || inclusion || (fishTD && fish)) && !(!foldamyloidTD && foldamyloid) && !(!fishTD && fish))
		{
			document.getElementById("analyze").disabled = false;
		}
		else
		{
			document.getElementById("analyze").disabled = true;
		}
	}
	else
	{
		document.getElementById("analyze").disabled = true;
	}
}
