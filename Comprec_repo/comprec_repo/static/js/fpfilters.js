// js for displaying or hiding file fields depending on checked filters
// Author: Julia Pelc

var LowEntropyCheck = document.getElementById('id_filters_0');
var FrequentGapCheck = document.getElementById('id_filters_1');
var IntraSSCheck = document.getElementById('id_filters_2');
var InterTerminusCheck = document.getElementById('id_filters_3');
var TerminusCheck = document.getElementById('id_filters_4');
var BuriedExposedCheck = document.getElementById('id_filters_5');

//MSA FILE REQUIRED
LowEntropyCheck.onchange = function() {
    if (LowEntropyCheck.checked) {
        document.getElementById('msa-file-row').style.display = '';
    }
    else {
        if (FrequentGapCheck.checked == false && InterTerminusCheck.checked == false){
            document.getElementById('msa-file-row').style.display = 'none';
        }
    }
};
FrequentGapCheck.onchange = function() {
    if (FrequentGapCheck.checked) {
        document.getElementById('msa-file-row').style.display = '';
    }
    else {
        if (LowEntropyCheck.checked == false && InterTerminusCheck.checked == false){
            document.getElementById('msa-file-row').style.display = 'none';
        }
    }
};
InterTerminusCheck.onchange = function() {
    if (InterTerminusCheck.checked) {
        document.getElementById('msa-file-row').style.display = '';
    }
    else {
        if (LowEntropyCheck.checked == false && FrequentGapCheck.checked == false){
            document.getElementById('msa-file-row').style.display = 'none';
        }
    }
};

//SS FILE REQUIRED
IntraSSCheck.onchange = function() {
    if (IntraSSCheck.checked) {
        document.getElementById('ss-file-row').style.display = '';
    } else {
        document.getElementById('ss-file-row').style.display = 'none';
    }
};

//RSA FILE REQUIRED
TerminusCheck.onchange = function() {
    if (TerminusCheck.checked) {
        document.getElementById('rsa-file-row').style.display = '';
    }
    else {
        if (BuriedExposedCheck.checked == false){
            document.getElementById('rsa-file-row').style.display = 'none';
        }
    }
};
BuriedExposedCheck.onchange = function() {
    if (BuriedExposedCheck.checked) {
        document.getElementById('rsa-file-row').style.display = '';
    }
    else {
        if (TerminusCheck.checked == false){
            document.getElementById('rsa-file-row').style.display = 'none';
        }
    }
};